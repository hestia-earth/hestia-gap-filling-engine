# usage:
# 1. Install python-dotenv
# 2. Set your env variables in a `.env` file
# 3. Put your test cycle into the `samples` folder
# 3. Run `python run.py samples/cycle.jsonld`
from dotenv import load_dotenv
load_dotenv()


import sys
import json
from hestia_earth.gap_filling import gap_fill


def main(args):
    filepath = args[0]
    with open(filepath) as f:
        node = json.load(f)

    print(f"processing {filepath}")
    data = gap_fill(node)

    with open(f"{filepath}-gap-filled", 'w') as f:
        f.write(json.dumps(data, indent=2, ensure_ascii=False))


if __name__ == "__main__":
    main(sys.argv[1:])
