default:
  image: python:3.7
  before_script:
    - pip install -r requirements.txt
    - pip install twine
    - python setup.py sdist bdist_wheel

stages:
  - test
  - build
  - deploy
  # needs to happen after deploy version
  - build-layer
  - deploy-layer

lint:
  stage: test
  before_script:
    - pip install flake8
  script:
    - flake8

validate-jsonld:
  image: node:latest
  stage: test
  before_script:
    - source envs/.develop.env
    - npm config set @hestia-earth:registry https://$GITLAB_NPM_URL
    - npm install
  script:
    - npm test
    # make sure terms exist
    - npm run test:terms

test:
  stage: test
  before_script:
    - source envs/.dev.env
    - pip install -r requirements.txt
    - pip install pytest-cov
    - chmod +x scripts/download_data.sh && ./scripts/download_data.sh $API_URL
  script:
    - pytest --cov-config=.coveragerc --cov-report term --cov-report html --cov=./ --cov-fail-under=90
  artifacts:
    paths:
      - coverage/

deploy-dev:
  stage: deploy
  script:
    - twine upload -u $PYPI_USERNAME -p $PYPI_PASSWORD --repository-url https://test.pypi.org/legacy/ dist/* || true
    - twine upload -u npm -p $GITLAB_NPM_TOKEN --repository-url https://$GITLAB_PYPI_URL dist/* || true
  only:
    - develop

deploy:
  stage: deploy
  script:
    - twine upload -u $PYPI_USERNAME -p $PYPI_PASSWORD --skip-existing dist/*
  only:
    - tags

docs-python:
  stage: deploy
  artifacts:
    paths:
      - public/
  before_script:
    - cd docs
    - pip install -r requirements.txt
  script:
    - make html
    - cd ../
    - mv docs/_build/html public
  only:
    - develop
    - tags

build-layer:
  image: docker:stable
  stage: build-layer
  services:
    - docker:dind
  artifacts:
    paths:
      - layer/python
  before_script:
    - mkdir -p ./layer/python
  script:
    - docker run --rm -v $(pwd):/var/task lambci/lambda:build-python3.7 pip install -r layer/requirements.txt -t layer/python/lib/python3.7/site-packages/
  after_script:
    # make sure we use the latest version of the files
    - rm -rf ./layer/python/lib/python3.7/site-packages/hestia_earth/gap_filling/*models
    - cp -R ./hestia_earth ./layer/python/lib/python3.7/site-packages/.
    # remove numpy as already included in another layer
    - rm -rf ./layer/python/lib/python3.7/site-packages/numpy*
  only:
    - develop
    - master

deploy-layer:
  stage: deploy-layer
  before_script:
    - source envs/.${CI_COMMIT_REF_NAME}.env
    - pip install awscli
    - apt-get update && apt-get install -y zip
  script:
    - chmod +x scripts/download_data.sh && ./scripts/download_data.sh $API_URL ./layer/python/lib/python3.7/site-packages/
    - chmod +x layer/deploy.sh && ./layer/deploy.sh $STAGE
  only:
    - develop
    - master
