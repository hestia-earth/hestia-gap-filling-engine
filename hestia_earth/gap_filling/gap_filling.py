from concurrent.futures import ThreadPoolExecutor
from functools import reduce
import pydash
from hestia_earth.utils.tools import non_empty_value

from .log import logger
from .utils import _merge_lists_term


def _non_empty_gap_fills(gap_fills): return list(filter(lambda value: non_empty_value(value[1]), gap_fills))


def _merge_gap_fills(source: dict, gap_fills: list):
    for key, values in gap_fills:
        current = pydash.objects.get(source, key)
        new_value = _merge_lists_term(current if current else [], values) if isinstance(values, list) else values
        pydash.objects.set_(source, key, new_value)
    return source


def _run_in_series(data, model):
    gap_fills = model(data)
    return _merge_gap_fills(data, _non_empty_gap_fills(gap_fills))


def run_serie(source: dict, models=[]):
    # make sure we don't override the source
    clone = pydash.objects.clone_deep(source)
    clone = reduce(lambda prev, curr: _run_in_series(prev, curr), models, clone)
    return clone


def run_parallel(source: dict, models=[]):
    # make sure we don't override the source
    clone = pydash.objects.clone_deep(source)

    with ThreadPoolExecutor() as executor:
        gap_fills = executor.map(lambda model: model(clone), models)

    gap_fills = _non_empty_gap_fills(pydash.flatten(gap_fills))
    logger.info('total=%s, gap_filled=%s', len(models), len(gap_fills))
    clone = _merge_gap_fills(clone, gap_fills)
    return clone
