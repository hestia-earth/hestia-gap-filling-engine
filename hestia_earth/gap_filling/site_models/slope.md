# Slope

This model gap-fills the terrain slope (%) of the 'Site'. Based on data from [Scherer & Pfister, (2015) Int. J. Life Cycle Assess. 20, 785–795](https://link.springer.com/article/10.1007%2Fs11367-015-0880-0), and [Danielson & Gesch (2008) Int. Arch. Photogramm. Remote Sens. Spat. Inf. Sci. XXXVII, 1857–1864 (2008)](https://www.researchgate.net/profile/Dean-Gesch/publication/254213144_AN_ENHANCED_GLOBAL_ELEVATION_MODEL_GENERALIZED_FROM_MULTIPLE_HIGHER_RESOLUTION_SOURCE_DATASETS/links/53d03bf20cf25dc05cfe392f/AN-ENHANCED-GLOBAL-ELEVATION-MODEL-GENERALIZED-FROM-MULTIPLE-HIGHER-RESOLUTION-SOURCE-DATASETS.pdf). If `Site` location is a spatial point (`latitude` and `longitude`), this model returns the percent slope at the given point location. If `Site` location is a spatial boundary (either a custom [boundary](https://www.hestia.earth/schema/Site#boundary), or a boundary automatically derived from the GADM [region](https://www.hestia.earth/schema/Site#region)), the mean slope percent within the bounary is returned.

## Gap-fills

- [measurement.term](https://hestia.earth/schema/Measurement#term) with [slope](https://hestia.earth/term/slope)
- [measurement.value](https://hestia.earth/schema/Measurement#value)
- [measurement.statsDefinition](https://hestia.earth/schema/Measurement#statsDefinition) with `spatial`

## Requirements

* Must have spatial information in one of the following formats:
  - Spatial point: [latitude](https://www.hestia.earth/schema/Site#latitude) and [longitude](https://www.hestia.earth/schema/Site#longitude)
  - Spatial boundary: [boundary](https://www.hestia.earth/schema/Site#boundary)
  - If no spatial data are provided, the boundary for the GADM [region](https://www.hestia.earth/schema/Site#region) is used

## Implementation Status

- `Implemented`
- [Tested](../../../tests/site_models/test_slope.py)
