from hestia_earth.schema import MeasurementStatsDefinition, TermTermType

from hestia_earth.gap_filling.log import logger
from hestia_earth.gap_filling.utils import should_gap_fill, _gap_filled_version
from hestia_earth.gap_filling.site_utils import _get_or_create_measurement
from hestia_earth.gap_filling.gee import download, should_gap_fill as gee_should_gap_fill, _site_gadm_id

GAP_FILLING_KEY = 'measurements'
TERM_TYPE = 'soilType'
TERM_ID = 'histosol'
MODEL_KEYS = 'statsDefinition,source'
BIBLIO_TITLE = 'The harmonized world soil database. verson 1.0'


def _gap_fill_measurement(measurements: list):
    logger.info('key=%s, term=%s', GAP_FILLING_KEY, TERM_ID)
    measurement = _get_or_create_measurement(measurements, TERM_ID, BIBLIO_TITLE)
    measurement['statsDefinition'] = MeasurementStatsDefinition.SPATIAL.value
    return _gap_filled_version(measurement, MODEL_KEYS)


def _gap_fill_site(site: dict):
    reducer = 'mode'
    value = download(collection='users/hestiaplatform/histosols_0_1',
                     ee_type='raster',
                     reducer=reducer,
                     latitude=site.get('latitude'),
                     longitude=site.get('longitude'),
                     gadm_id=_site_gadm_id(site),
                     boundary=site.get('boundary'),
                     fields=reducer
                     ).get(reducer, None)

    return [_gap_fill_measurement(site.get(GAP_FILLING_KEY, []))] if value == 1 else []


def _need_gap_fill(site: dict):
    measurements = site.get('measurements', [])
    has_soil_type = any([m for m in measurements if m.get('term').get('termType') == TermTermType.SOILTYPE.value])
    gap_fill = gee_should_gap_fill(site) and \
        not has_soil_type and \
        should_gap_fill(site.get('measurements', []), TERM_ID)
    logger.info('key=%s, term=%s, gap_fill=%s', GAP_FILLING_KEY, TERM_ID, gap_fill)
    return gap_fill


def gap_fill(site: dict): return [(GAP_FILLING_KEY, _gap_fill_site(site) if _need_gap_fill(site) else [])]
