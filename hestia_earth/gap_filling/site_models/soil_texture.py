from functools import reduce
from typing import List
from hestia_earth.schema import SchemaType, MeasurementJSONLD, MeasurementStatsDefinition
from hestia_earth.utils.api import download_hestia, find_node_exact
from hestia_earth.utils.model import find_term_match, linked_node

from hestia_earth.gap_filling.log import logger
from hestia_earth.gap_filling.utils import should_gap_fill, _gap_filled_version
from hestia_earth.gap_filling.site_utils import measurement_value_average
from hestia_earth.gap_filling.gee import download, should_gap_fill as gee_should_gap_fill, _site_gadm_id

GAP_FILLING_KEY = 'measurements'
TERM_ID = 'clayContent,sandContent,siltContent'
TERM_IDS = {
    'clayContent': 'users/hestiaplatform/T_CLAY',
    'sandContent': 'users/hestiaplatform/T_SAND',
    'siltContent': None
}
MODEL_KEYS = 'value,statsDefinition,depthUpper,depthLower,source'
BIBLIO_TITLE = 'The harmonized world soil database. verson 1.0'


def _get_source():
    source = find_node_exact(SchemaType.SOURCE, {'bibliography.title': BIBLIO_TITLE})
    if source is not None:
        logger.debug('fetching source: %s', source.get('@id'))
        source = download_hestia(source.get('@id'), SchemaType.SOURCE)
        return linked_node({'@type': SchemaType.SOURCE.value, **source})
    return None


def _new_measurement(measurements: list, model: str, value: int, source=None):
    measurement = find_term_match(measurements, model, None)
    if measurement is None:
        measurement = MeasurementJSONLD().to_dict()
        measurement['term'] = linked_node(download_hestia(model))
        if source:
            measurement['source'] = source
            measurement = _gap_filled_version(measurement, 'source')

    logger.info('key=%s, term=%s, value=%s', GAP_FILLING_KEY, measurement.get('term', {}).get('@id'), value)
    measurement['value'] = [value]
    measurement['depthUpper'] = 0
    measurement['depthLower'] = 30
    measurement['statsDefinition'] = MeasurementStatsDefinition.SPATIAL.value
    return _gap_filled_version(measurement, MODEL_KEYS)


def _gap_fill_content(source: dict, site: dict, model: str, collection: str):
    field = 'first'
    value = download(collection=collection,
                     ee_type='raster',
                     latitude=site.get('latitude'),
                     longitude=site.get('longitude'),
                     gadm_id=_site_gadm_id(site),
                     boundary=site.get('boundary'),
                     fields=field
                     ).get(field, None)

    return [_new_measurement(site.get('measurements', []), model, round(value), source)] if value is not None else []


def _gap_fill_all(site: dict, models: List[str]):
    source = _get_source()
    other_models = list(filter(lambda model: TERM_IDS[model] is not None, models))
    measurements = reduce(
        lambda prev, curr: prev + _gap_fill_content(source, site, curr, TERM_IDS[curr]),
        other_models,
        []
    )
    # if we gap-filled all but 1 model, it can be gap-filled without querying GEE
    model_keys = _need_gap_fill_models(measurements)
    return measurements + (_gap_fill_single(measurements, model_keys[0]) if len(model_keys) == 1 else [])


def _gap_fill_single(measurements: list, model: str):
    source = _get_source()
    other_models = list(TERM_IDS.keys())
    other_models.remove(model)
    value = reduce(
        lambda prev, curr: prev - float(measurement_value_average(find_term_match(measurements, curr, {}))),
        other_models,
        100
    )
    return [_new_measurement(measurements, model, value, source)]


def _need_gap_fill_models(measurements: list):
    return list(filter(
        lambda model: should_gap_fill(measurements, model),
        list(TERM_IDS.keys())
    ))


def _gap_fill_site(site: dict):
    measurements = site.get('measurements', [])
    model_keys = _need_gap_fill_models(measurements)
    return _gap_fill_single(measurements, model_keys[0]) if len(model_keys) == 1 else _gap_fill_all(site, model_keys)


def _need_gap_fill(site: dict):
    has_coordinates = gee_should_gap_fill(site)
    model_keys = _need_gap_fill_models(site.get('measurements', []))
    gap_fill = has_coordinates and len(model_keys) > 0
    logger.info('key=%s, term=%s, gap_fill=%s', GAP_FILLING_KEY, model_keys, gap_fill)
    return gap_fill


def gap_fill(site: dict): return [(GAP_FILLING_KEY, _gap_fill_site(site) if _need_gap_fill(site) else [])]
