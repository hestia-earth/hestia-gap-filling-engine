# Region

This model gap-fills the geogrpahic region of the `Site`. The model gap-fills the finest scale GADM region possible, moving from gadm level 5 (for example, a village) to GADM level 0 (Country).

## Gap-fills

- [site.region](https://hestia.earth/schema/Site#region)

## Requirements

* Must have spatial point data, [latitude](https://www.hestia.earth/schema/Site#latitude) and [longitude](https://www.hestia.earth/schema/Site#longitude).

## Implementation Status

- `Implemented`
- [Tested](../../../tests/site_models/test_region.py)
