from hestia_earth.schema import MeasurementStatsDefinition

from hestia_earth.gap_filling.log import logger
from hestia_earth.gap_filling.utils import should_gap_fill, _gap_filled_version
from hestia_earth.gap_filling.site_utils import _get_or_create_measurement
from hestia_earth.gap_filling.gee import download, should_gap_fill as gee_should_gap_fill, _site_gadm_id

GAP_FILLING_KEY = 'measurements'
TERM_ID = 'soilPh'
MODEL_KEYS = 'value,statsDefinition,depthUpper,depthLower,source'
BIBLIO_TITLE = 'The harmonized world soil database. verson 1.0'


def _gap_fill_measurement(measurements: list, value: float):
    logger.info('key=%s, term=%s, value=%s', GAP_FILLING_KEY, TERM_ID, value)
    measurement = _get_or_create_measurement(measurements, TERM_ID, BIBLIO_TITLE)
    measurement['value'] = [value]
    measurement['depthUpper'] = 0
    measurement['depthLower'] = 30
    measurement['statsDefinition'] = MeasurementStatsDefinition.SPATIAL.value
    return _gap_filled_version(measurement, MODEL_KEYS)


def _gap_fill_site(site: dict):
    field = 'first'
    value = download(collection='users/hestiaplatform/T_PH_H2O',
                     ee_type='raster',
                     latitude=site.get('latitude'),
                     longitude=site.get('longitude'),
                     gadm_id=_site_gadm_id(site),
                     boundary=site.get('boundary'),
                     fields=field
                     ).get(field, None)

    return [_gap_fill_measurement(site.get(GAP_FILLING_KEY, []), round(value, 1))] if value is not None else []


def _need_gap_fill(site: dict):
    gap_fill = gee_should_gap_fill(site) and \
        should_gap_fill(site.get('measurements', []), TERM_ID)
    logger.info('key=%s, term=%s, gap_fill=%s', GAP_FILLING_KEY, TERM_ID, gap_fill)
    return gap_fill


def gap_fill(site: dict): return [(GAP_FILLING_KEY, _gap_fill_site(site) if _need_gap_fill(site) else [])]
