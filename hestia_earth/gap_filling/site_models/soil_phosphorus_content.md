# Soil Phosphorus Content

This model gap-fills the Phosphorus concentration of the soil at the `Site`. The data are derived from Phosphorus area density to a depth of 50cm, see [Scherer & Pfister, (2015) Int. J. Life Cycle Assess. 20, 785–795](https://link.springer.com/article/10.1007%2Fs11367-015-0880-0).  If `Site` location is a spatial point (`latitude` and `longitude`), this model returns the Phosphorus concentration of the soil at the point location. If `Site` location is a spatial boundary (either a custom [boundary](https://www.hestia.earth/schema/Site#boundary), or a boundary automatically derived from the GADM [region](https://www.hestia.earth/schema/Site#region)), this model returns the mean Phosphorus concentration of the soil within the boundary.

## Gap-fills

- [measurement.term](https://hestia.earth/schema/Measurement#term) with [soilPhosphorusContent](https://hestia.earth/term/soilPhosphorusContent)
- [measurement.value](https://hestia.earth/schema/Measurement#value)
- [measurement.statsDefinition](https://hestia.earth/schema/Measurement#statsDefinition) with `spatial`
- [measurement.depthUpper](https://hestia.earth/schema/Measurement#depthUpper)
- [measurement.depthLower](https://hestia.earth/schema/Measurement#depthLower)

## Requirements

* Must have spatial information in one of the following formats:
  - Spatial point: [latitude](https://www.hestia.earth/schema/Site#latitude) and [longitude](https://www.hestia.earth/schema/Site#longitude)
  - Spatial boundary: [boundary](https://www.hestia.earth/schema/Site#boundary)
  - If no spatial data are provided, the boundary for the GADM [region](https://www.hestia.earth/schema/Site#region) is used

## Implementation Status

- `Implemented`
- [Tested](../../../tests/site_models/test_soil_phosphorous_content.py)
