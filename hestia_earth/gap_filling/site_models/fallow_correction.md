# Fallow correction

This model gap-fills fallow correction, which is the time/area under fallow relative to the time/area under both cultivation and fallow per year. Based on the data and fallow period model of [Siebert et al. (2010) Remote Sens. 2, 1625-1643, doi:10.3390/rs2071625](https://www.mdpi.com/2072-4292/2/7/1625). If `Site` location is a spatial point (`latitude` and `longitude`), this model returns an estimate of fallow correction at the given point location.

## Gap-fills

- [measurement.term](https://hestia.earth/schema/Measurement#term) with [Fallow correction](https://hestia.earth/term/fallowCorrection)
- [measurement.value](https://hestia.earth/schema/Measurement#value)
- [measurement.statsDefinition](https://hestia.earth/schema/Measurement#statsDefinition) with `spatial`

## Requirements

* Must have spatial information in one of the following formats:
  - Spatial point: [latitude](https://www.hestia.earth/schema/Site#latitude) and [longitude](https://www.hestia.earth/schema/Site#longitude)
  - Spatial boundary: [boundary](https://www.hestia.earth/schema/Site#boundary)
  - If no spatial data are provided, the boundary for the GADM [region](https://www.hestia.earth/schema/Site#region) is used

## Implementation Status

- `Implemented`
- [Tested](../../../tests/site_models/test_fallow_correction.py)
