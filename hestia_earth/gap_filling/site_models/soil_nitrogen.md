# Soil Total Nitrogen Content

Gap-fills soil total Nitrogen concentration for the `Site`. Based on data from [Batjes (2015) ISRIC report 2015/01 World Soil Information](https://www.researchgate.net/publication/282185041_World_soil_property_estimates_for_broad-_scale_modelling_WISE30sec).  If `Site` location is a spatial point (`latitude` and `longitude`), this model returns the total Nitrogen concentration of the soil at the point location. If `Site` location is a spatial boundary (either a custom [boundary](https://www.hestia.earth/schema/Site#boundary), or a boundary automatically derived from the GADM [region](https://www.hestia.earth/schema/Site#region)), this model returns the mean total Nitrogen concentration of the soils within the boundary.
*NOTE* This gap-filling model is still in development. Currently, it is not advised to use the results of this model

## Gap-fills

- [measurement.term](https://hestia.earth/schema/Measurement#term) with [soilTotalNitrogenContent](https://hestia.earth/term/soilTotalNitrogenContent)
- [measurement.value](https://hestia.earth/schema/Measurement#value)
- [measurement.statsDefinition](https://hestia.earth/schema/Measurement#statsDefinition) with `spatial`
- [measurement.depthUpper](https://hestia.earth/schema/Measurement#depthUpper)
- [measurement.depthLower](https://hestia.earth/schema/Measurement#depthLower)

## Requirements

* Must have spatial information in one of the following formats:
  - Spatial point: [latitude](https://www.hestia.earth/schema/Site#latitude) and [longitude](https://www.hestia.earth/schema/Site#longitude)
  - Spatial boundary: [boundary](https://www.hestia.earth/schema/Site#boundary)
  - If no spatial data are provided, the boundary for the GADM [region](https://www.hestia.earth/schema/Site#region) is used

## Implementation Status

- `Implemented`
- [Tested](../../../tests/site_models/test_soil_nitrogen.py)
