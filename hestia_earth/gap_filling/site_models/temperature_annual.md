# Temperature Annual

Gap-fills the mean annual temperature in the final year of the `Cycle` for the `Site`. Based on data from the [Copernicus programme, ERA5 produced by ECMWF/Copernicus Climate Change Service](https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-single-levels?tab=overview)
*NOTE* This model is in development, due to be updated during the week starting 22nd March.

## Gap-fills

- [measurement.term](https://hestia.earth/schema/Measurement#term) with [temperatureAnnual](https://hestia.earth/term/temperatureAnnual)
- [measurement.value](https://hestia.earth/schema/Measurement#value)
- [measurement.statsDefinition](https://hestia.earth/schema/Measurement#statsDefinition) with `spatial`
- [measurement.startDate](https://hestia.earth/schema/Measurement#startDate)
- [measurement.endDate](https://hestia.earth/schema/Measurement#endDate)

## Requirements

* Must have spatial information in one of the following formats:
  - Spatial point: [latitude](https://www.hestia.earth/schema/Site#latitude) and [longitude](https://www.hestia.earth/schema/Site#longitude)
  - Spatial boundary: [boundary](https://www.hestia.earth/schema/Site#boundary)
  - If no spatial data are provided, the boundary for the GADM [region](https://www.hestia.earth/schema/Site#region) is used
* Must be associated with at least 1 `Cycle` that has an [endDate](https://www.hestia.earth/schema/Cycle#endDate) after `1979`

## Implementation Status

- `Implemented`
- [Tested](../../../tests/site_models/test_temperature_annual.py)
