# Heavy Winter Precipitation

Heavy winter precipitation is a boolean variable set to `1` when precipitation exceeds 15 % of the annual average for at least one winter month during the year of the cycle, returns `0` if not. Based on [Scherer & Pfister (2015)](https://link.springer.com/article/10.1007%2Fs11367-015-0880-0). This model gap-fills the [heavy winter precipitation](https://www.hestia.earth/term/heavyWinterPrecipitation) for the `Site`. If `Site` location is a spatial point (`latitude` and `longitude`), this model returns the heavy winter precipitation at the given point location. If `Site` location is a spatial boundary (either a custom [boundary](https://www.hestia.earth/schema/Site#boundary), or a boundary automatically derived from the GADM [region](https://www.hestia.earth/schema/Site#region)), the dominant (modal) heavy winter precipitation (either `1` or `0`) within boundary is returned.

## Gap-fills

- [measurement.term](https://hestia.earth/schema/Measurement#term) with [heavyWinterPrecipitation](https://hestia.earth/term/heavyWinterPrecipitation)
- [measurement.value](https://hestia.earth/schema/Measurement#value)
- [measurement.statsDefinition](https://hestia.earth/schema/Measurement#statsDefinition) with `spatial`

## Requirements

* Must have spatial information in one of the following formats:
  - Spatial point: [latitude](https://www.hestia.earth/schema/Site#latitude) and [longitude](https://www.hestia.earth/schema/Site#longitude)
  - Spatial boundary: [boundary](https://www.hestia.earth/schema/Site#boundary)
  - If no spatial data are provided, the boundary for the GADM [region](https://www.hestia.earth/schema/Site#region) is used

## Implementation Status

- `Implemented`
- [Tested](../../../tests/site_models/test_heavy_winter_precipitation.py)
