from hestia_earth.utils.api import download_hestia
from hestia_earth.utils.model import linked_node

from hestia_earth.gap_filling.log import logger
from hestia_earth.gap_filling.gee import download

GAP_FILLING_KEY = 'region'


def _download_by_level(site: dict, level: int):
    field = f"GID_{level}"
    gadm_id = download(collection=f"users/hestiaplatform/gadm36_{level}",
                       ee_type='vector',
                       latitude=site.get('latitude'),
                       longitude=site.get('longitude'),
                       boundary=site.get('boundary'),
                       fields=field
                       ).get(field)
    try:
        return None if gadm_id is None else linked_node(download_hestia(f"GADM-{gadm_id}"))
    except Exception:
        # the Term might not exist in our glossary if it was marked as duplicate
        return None


def _gap_fill_site(site: dict):
    for level in [5, 4, 3, 2, 1]:
        value = _download_by_level(site, level)
        if value is not None:
            logger.info('key=%s, value=%s', GAP_FILLING_KEY, value.get('@id'))
            break

    return value


def _need_gap_fill(site: dict):
    gap_fill = 'latitude' in site and 'longitude' in site and GAP_FILLING_KEY not in site
    logger.info('key=%s, gap_fill=%s', GAP_FILLING_KEY, gap_fill)
    return gap_fill


def gap_fill(site: dict): return [(GAP_FILLING_KEY, _gap_fill_site(site) if _need_gap_fill(site) else None)]
