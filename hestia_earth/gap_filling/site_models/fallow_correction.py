from hestia_earth.schema import MeasurementStatsDefinition

from hestia_earth.gap_filling.log import logger
from hestia_earth.gap_filling.utils import should_gap_fill, _gap_filled_version
from hestia_earth.gap_filling.site_utils import _get_or_create_measurement
from hestia_earth.gap_filling.gee import download, should_gap_fill as gee_should_gap_fill, _site_gadm_id

GAP_FILLING_KEY = 'measurements'
TERM_ID = 'fallowCorrection'
MODEL_KEYS = 'value,statsDefinition,source'


def _gap_fill_measurement(measurements: list, value: float):
    logger.info('key=%s, term=%s, value=%s', GAP_FILLING_KEY, TERM_ID, value)
    measurement = _get_or_create_measurement(measurements, TERM_ID)
    measurement['value'] = [value]
    measurement['statsDefinition'] = MeasurementStatsDefinition.SPATIAL.value
    return _gap_filled_version(measurement, MODEL_KEYS)


def _gap_fill_site(site: dict):
    # 1) extract maximum monthly growing area (MMGA)
    reducer = 'sum'
    MMGA_value = download(collection='users/hestiaplatform/MMGA',
                          ee_type='raster',
                          reducer=reducer,
                          latitude=site.get('latitude'),
                          longitude=site.get('longitude'),
                          gadm_id=_site_gadm_id(site),
                          boundary=site.get('boundary'),
                          fields=reducer
                          )
    MMGA_value = MMGA_value.get('first', MMGA_value.get('sum', None))

    # 2) extract cropping extent (CE)
    CE_value = download(collection='users/hestiaplatform/CE',
                        ee_type='raster',
                        reducer=reducer,
                        latitude=site.get('latitude'),
                        longitude=site.get('longitude'),
                        gadm_id=_site_gadm_id(site),
                        boundary=site.get('boundary'),
                        fields=reducer
                        )
    CE_value = CE_value.get('first', CE_value.get('sum', None))

    # 3) estimate fallowCorrection from MMGA and CE.
    value = None if MMGA_value is None or CE_value is None else (CE_value / MMGA_value)

    return [_gap_fill_measurement(site.get(GAP_FILLING_KEY, []), value)] if value is not None else []


def _need_gap_fill(site: dict):
    gap_fill = gee_should_gap_fill(site) and \
        should_gap_fill(site.get(GAP_FILLING_KEY, []), TERM_ID)
    logger.info('key=%s, term=%s, gap_fill=%s', GAP_FILLING_KEY, TERM_ID, gap_fill)
    return gap_fill


def gap_fill(site: dict): return [(GAP_FILLING_KEY, _gap_fill_site(site) if _need_gap_fill(site) else [])]
