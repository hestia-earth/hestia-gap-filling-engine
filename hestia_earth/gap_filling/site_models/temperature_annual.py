from hestia_earth.schema import SchemaType, MeasurementStatsDefinition
from datetime import datetime
from dateutil.relativedelta import relativedelta
from hestia_earth.utils.tools import non_empty_list, safe_parse_date

from hestia_earth.gap_filling.log import logger
from hestia_earth.gap_filling.utils import should_gap_fill, _gap_filled_version
from hestia_earth.gap_filling.site_utils import _new_measurement, _related_cycles
from hestia_earth.gap_filling.gee import download, should_gap_fill as gee_should_gap_fill, _site_gadm_id

GAP_FILLING_KEY = 'measurements'
TERM_ID = 'temperatureAnnual'
MODEL_KEYS = 'value,statsDefinition,startDate,endDate,source'
BIBLIO_TITLE = 'ERA5: Fifth generation of ECMWF atmospheric reanalyses of the global climate'
KELVIN_0 = 273.15


def _cycle_year(cycle: dict):
    date = safe_parse_date(cycle.get('endDate'))
    return date.year if date else None


def _should_gap_fill_cycle(year: int):
    # NOTE: Currently uses the climate data for the final year of the study
    # see: https://developers.google.com/earth-engine/datasets/catalog/ECMWF_ERA5_MONTHLY
    # ERA5 data is available from 1979 to three months from real-time
    limit_upper = datetime.now() + relativedelta(months=-3)
    return 1979 <= year and year <= limit_upper.year


def _gap_fill_measurement(value: float, year: int):
    logger.info('key=%s, term=%s, value=%s, year=%s', GAP_FILLING_KEY, TERM_ID, value, year)
    measurement = _new_measurement(TERM_ID, BIBLIO_TITLE)
    measurement['value'] = [value]
    measurement['startDate'] = f"{year}-01-01"
    measurement['endDate'] = f"{year}-12-31"
    measurement['statsDefinition'] = MeasurementStatsDefinition.SPATIAL.value
    return _gap_filled_version(measurement, MODEL_KEYS)


def _gap_fill_site(site: dict, year: int):
    collection = 'ECMWF/ERA5/MONTHLY'
    reducer = 'mean'
    value = download(collection=collection,
                     ee_type='raster_by_period',
                     band_name='mean_2m_air_temperature',
                     reducer=reducer,
                     year=str(year),
                     latitude=site.get('latitude'),
                     longitude=site.get('longitude'),
                     gadm_id=_site_gadm_id(site),
                     boundary=site.get('boundary')
                     ).get(reducer, None)

    # Ensure the units of the data extracted from GEE match the units of the term (Kelvin to Celcius)
    return [_gap_fill_measurement(value - KELVIN_0, year)] if value is not None else []


def _need_gap_fill(site: dict, year: int):
    gap_fill = _should_gap_fill_cycle(year) and gee_should_gap_fill(site) and \
        should_gap_fill(site.get('measurements', []), TERM_ID)
    logger.info('key=%s, term=%s, gap_fill=%s', GAP_FILLING_KEY, TERM_ID, gap_fill)
    return gap_fill


def gap_fill(site: dict):
    cycles = _related_cycles(SchemaType.SITE, site.get('@id'))
    logger.info('term=%s, related_cycles=%s', TERM_ID, ','.join(map(lambda c: c.get('@id'), cycles)))
    years = non_empty_list(set(map(_cycle_year, cycles)))
    years = list(filter(lambda year: _need_gap_fill(site, year), years))
    logger.info('term=%s, years=%s', TERM_ID, years)
    return list(map(lambda year: (GAP_FILLING_KEY, _gap_fill_site(site, year)), years))
