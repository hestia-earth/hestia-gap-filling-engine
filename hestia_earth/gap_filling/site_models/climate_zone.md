# Eco-climate Zone

Eco-climate zone is a twelve class categorical variable that broadly groups areas based on their ecology and climate. They approximataely map to the [IPCC (2019) Climate Zones](https://www.ipcc-nggip.iges.or.jp/public/2019rf/pdf/4_Volume4/19R_V4_Ch03_Land%20Representation.pdf). Data are derived from [Hiederer et al. (2010) Biofuels: A new methodology to estimate GHG emissions from global land use change, European Commission Joint Research Centre](https://ec.europa.eu/jrc/en/publication/eur-scientific-and-technical-research-reports/biofuels-new-methodology-estimate-ghg-emissions-due-global-land-use-change-methodology). This model gap-fills the [eco-climate zone](https://hestia.earth/term/eco-ClimateZone) of the `Site`. If `Site` location is a spatial point (`latitude` and `longitude`), this model returns the category of eco-climate zone at the given point location. If `Site` location is a spatial boundary (either a custom [boundary](https://www.hestia.earth/schema/Site#boundary), or a boundary automatically derived from the GADM [region](https://www.hestia.earth/schema/Site#region)), the dominant (modal) eco-climate zone present within the boundary is returned.

| Value | Climate Zone         |
|-------|----------------------|
| 1     | Warm Temperate Moist |
| 2     | Warm Temperate Dry   |
| 3     | Cool Temperate Moist |
| 4     | Cool Temperate Dry   |
| 5     | Polar Moist          |
| 6     | Polar Dry            |
| 7     | Boreal Moist         |
| 8     | Boreal Dry           |
| 9     | Tropical Montane     |
| 10    | Tropical Wet         |
| 11    | Tropical Moist       |
| 12    | Tropical Dry         |


## Gap-fills

- [measurement.term](https://hestia.earth/schema/Measurement#term) with [eco-ClimateZone](https://hestia.earth/term/eco-ClimateZone)
- [measurement.value](https://hestia.earth/schema/Measurement#value)
- [measurement.statsDefinition](https://hestia.earth/schema/Measurement#statsDefinition) with `spatial`

## Requirements

* Must have spatial information in one of the following formats:
  - Spatial point: [latitude](https://www.hestia.earth/schema/Site#latitude) and [longitude](https://www.hestia.earth/schema/Site#longitude)
  - Spatial boundary: [boundary](https://www.hestia.earth/schema/Site#boundary)
  - If no spatial data are provided, the boundary for the GADM [region](https://www.hestia.earth/schema/Site#region) is used

## Implementation Status

- `Implemented`
- [Tested](../../../tests/site_models/test_climate_zone.py)
