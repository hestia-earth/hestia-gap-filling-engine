# Soil Ph

This model gap-fills the topsoil pH measurement of the `Site`. This approach is based on the soil-water solution method with data extracted from the [Harmonized World Soil Database (HWSD)](http://www.fao.org/3/aq361e/aq361e.pdf). If `Site` location is a spatial point (`latitude` and `longitude`), this model returns the soil pH at the point location. If `Site` location is a spatial boundary (either a custom [boundary](https://www.hestia.earth/schema/Site#boundary), or a boundary automatically derived from the GADM [region](https://www.hestia.earth/schema/Site#region)), this model returns the mean soil pH within the boundary.

## Gap-fills

- [measurement.term](https://hestia.earth/schema/Measurement#term) with [soilPh](https://hestia.earth/term/soilPh)
- [measurement.value](https://hestia.earth/schema/Measurement#value)
- [measurement.statsDefinition](https://hestia.earth/schema/Measurement#statsDefinition) with `spatial`
- [measurement.depthUpper](https://hestia.earth/schema/Measurement#depthUpper)
- [measurement.depthLower](https://hestia.earth/schema/Measurement#depthLower)

## Requirements

* Must have spatial information in one of the following formats:
  - Spatial point: [latitude](https://www.hestia.earth/schema/Site#latitude) and [longitude](https://www.hestia.earth/schema/Site#longitude)
  - Spatial boundary: [boundary](https://www.hestia.earth/schema/Site#boundary)
  - If no spatial data are provided, the boundary for the GADM [region](https://www.hestia.earth/schema/Site#region) is used

## Implementation Status

- `Implemented`
- [Tested](../../../tests/site_models/test_soil_ph.py)
