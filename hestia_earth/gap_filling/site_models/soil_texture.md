# Soil Texture

This model gap-fills the clay, sand and silt composition of the soil. The three terms can be gap-filled providing 0, 1 or 2 of the values. This model uses data from from the [Harmonized World Soil Database, version 1.2 (FAO, 2012)](http://www.fao.org/3/aq361e/aq361e.pdf). If `Site` location is a spatial point (`latitude` and `longitude`), this model returns the soil composition values at the point location. If `Site` location is a spatial boundary (either a custom [boundary](https://www.hestia.earth/schema/Site#boundary), or a boundary automatically derived from the GADM [region](https://www.hestia.earth/schema/Site#region)), this model returns the mean soil composition values within the boundary. Clay, sand and silt content percentages must sum, to 100%. If two of the terms have percentage values, the remaining term value is gap-filled as to ensure the three values sum to 100%. If only one term has values, the spatial queries discussed earlier are used to extract clay and sand content.

## Gap-fills

- [measurement.term](https://hestia.earth/schema/Measurement#term) with [clayContent](https://hestia.earth/term/clayContent), [sandContent](https://hestia.earth/term/sandContent) or [siltContent](https://hestia.earth/term/siltContent)
- [measurement.value](https://hestia.earth/schema/Measurement#value)
- [measurement.statsDefinition](https://hestia.earth/schema/Measurement#statsDefinition) with `spatial`
- [measurement.depthUpper](https://hestia.earth/schema/Measurement#depthUpper)
- [measurement.depthLower](https://hestia.earth/schema/Measurement#depthLower)

## Requirements

* Must have spatial information in one of the following formats:
  - Spatial point: [latitude](https://www.hestia.earth/schema/Site#latitude) and [longitude](https://www.hestia.earth/schema/Site#longitude)
  - Spatial boundary: [boundary](https://www.hestia.earth/schema/Site#boundary)
  - If no spatial data are provided, the boundary for the GADM [region](https://www.hestia.earth/schema/Site#region) is used

## Implementation Status

- `Implemented`
- [Tested](../../../tests/site_models/test_soil_texture.py)
