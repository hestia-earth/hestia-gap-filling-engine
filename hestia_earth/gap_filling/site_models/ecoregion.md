# Ecoregion

Ecoregions represent the original distribution of distinct assemblages of species and communities. There are 867 terrestrial ecoregions as [defined by WWF](https://www.worldwildlife.org/publications/terrestrial-ecoregions-of-the-world). This model gap-fills the [ecoregion](https://www.hestia.earth/schema/Site#ecoregion) present at the `Site`. If `Site` location is a spatial point (`latitude` and `longitude`), this model returns the ecoregion at the given point location. If `Site` location is a spatial boundary (either a custom [boundary](https://www.hestia.earth/schema/Site#boundary), or a boundary automatically derived from the GADM [region](https://www.hestia.earth/schema/Site#region)), the dominant (modal) ecoregion is returned. *NOTE* this model will be updated to return percent cover of each ecoregion within the boundary.

## Gap-fills

- [site.ecoregion](https://hestia.earth/schema/Site#ecoregion)

## Requirements

* Must have spatial information in one of the following formats:
  - Spatial point: [latitude](https://www.hestia.earth/schema/Site#latitude) and [longitude](https://www.hestia.earth/schema/Site#longitude)
  - Spatial boundary: [boundary](https://www.hestia.earth/schema/Site#boundary)
  - If no spatial data are provided, the boundary for the GADM [region](https://www.hestia.earth/schema/Site#region) is used

## Implementation Status

- `Implemented`
- [Tested](../../../tests/site_models/test_ecoregion.py)
