# Histosol

This model adds [histosol](https://hestia.earth/term/histosol) as a measurement of the site, if the soils located at `Site` are predominantly classed as histosols. Based on data adapted from the [Harmonized World Soil Database, version 1.2 (FAO, 2012)](http://www.fao.org/3/aq361e/aq361e.pdf). If `Site` location is a spatial point (`latitude` and `longitude`), this model returns histosol as a measurement if the given point location fall within a histosol location. If `Site` location is a spatial boundary (either a custom [boundary](https://www.hestia.earth/schema/Site#boundary), or a boundary automatically derived from the GADM [region](https://www.hestia.earth/schema/Site#region)), and the dominant (modal) soil type within that boundary is a histosol, then histosol is added as a measurement.

## Gap-fills

- [measurement.term](https://hestia.earth/schema/Measurement#term) with [histosol](https://hestia.earth/term/histosol)
- [measurement.statsDefinition](https://hestia.earth/schema/Measurement#statsDefinition) with `spatial`

## Requirements

* Must have spatial information in one of the following formats:
  - Spatial point: [latitude](https://www.hestia.earth/schema/Site#latitude) and [longitude](https://www.hestia.earth/schema/Site#longitude)
  - Spatial boundary: [boundary](https://www.hestia.earth/schema/Site#boundary)
  - If no spatial data are provided, the boundary for the GADM [region](https://www.hestia.earth/schema/Site#region) is used
  - If another measurement with `termType`=`soilType` exists, this model will not add 'histosol' as a site measurement.  

## Implementation Status

- `Implemented`
- [Tested](../../../tests/site_models/test_histosol.py)
