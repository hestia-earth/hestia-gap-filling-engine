from hestia_earth.gap_filling.log import logger
from hestia_earth.gap_filling.gee import download, should_gap_fill as gee_should_gap_fill, _site_gadm_id

GAP_FILLING_KEY = 'awareWaterBasinId'


def _gap_fill_site(site: dict):
    field = 'Name'
    awareId = download(collection='users/hestiaplatform/AWARE',
                       ee_type='vector',
                       latitude=site.get('latitude'),
                       longitude=site.get('longitude'),
                       gadm_id=_site_gadm_id(site),
                       boundary=site.get('boundary'),
                       fields=field
                       ).get(field, None)

    logger.info('key=%s, value=%s', GAP_FILLING_KEY, awareId)
    return awareId


def _need_gap_fill(site: dict):
    gap_fill = gee_should_gap_fill(site) and GAP_FILLING_KEY not in site
    logger.info('key=%s, gap_fill=%s', GAP_FILLING_KEY, gap_fill)
    return gap_fill


def gap_fill(site: dict): return [(GAP_FILLING_KEY, _gap_fill_site(site) if _need_gap_fill(site) else None)]
