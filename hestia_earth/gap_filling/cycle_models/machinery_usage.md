# Machinery Usage

Machinery gradually depreciates over multiple production Cycles until it reaches the [end of its life](https://hestia.earth/schema/Infrastructure#endDate).
As a rough rule, the more the machinery is used, the faster it depreciates.
Machinery use can be proxied for by the amount of fuel used.
From 139 processes in [AGRIBALYSE](https://agribalyse.ademe.fr/), the ratio of machinery depreciated per unit of fuel consumed (kg machinery kg diesel–1) was established.
Recognizing that farms in less developed countries have poorer access to capital and maintain farm machinery for longer, the machinery-to-diesel ratio was doubled in countries with a [Human Development Index](http://hdr.undp.org/en/data) of less than 0.8

## Gap-fills

- [input.term](https://hestia.earth/schema/Input#term) with [machineryInfrastructureDepreciatedAmountPerCycle](https://hestia.earth/term/machineryInfrastructureDepreciatedAmountPerCycle)
- [input.value](https://hestia.earth/schema/Input#value)
- [input.statsDefinition](https://hestia.earth/schema/Input#statsDefinition) with `simulated`

## Requirements

- [Input](https://hestia.earth/schema/Input) of [termType](https://hestia.earth/schema/Term#termType) = [fuel](https://hestia.earth/glossary?page=1&termType=fuel) which is a liquid fuel.
- Country: [site.country](https://hestia.earth/schema/Site#country).
- Site type: [site.siteType](https://www.hestia.earth/schema/Site#siteType) must be either `cropland` or `permanent pasture` as this term only refers to the machinery used for farming activity (for example, ploughing).
- Data completeness assessment for material: [completeness.material](https://www.hestia.earth/schema/Completeness#material) must be `False`.

## Implementation Status

- `Implemented`
- [Tested](../../../tests/cycle_models/test_machinery_usage.py)
