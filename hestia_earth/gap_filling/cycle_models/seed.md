# Seed

This model estimates the amount of [seed](https://hestia.earth/term/seed) applied as an [Input](https://hestia.earth/schema/Input).
It uses the global average seed application rate per unit of crop yield from the FAOSTAT food balance sheets.

Standard deviation is a production weighted standard deviation across countries.

## Gap-fills

- [input.term](https://hestia.earth/schema/Input#term) with [seed](https://hestia.earth/term/seed)
- [input.value](https://hestia.earth/schema/Input#value)
- [input.sd](https://hestia.earth/schema/Input#sd)
- [input.statsDefinition](https://hestia.earth/schema/Input#statsDefinition) with `regions`

## Requirements

- Crop yield: [product.value](https://hestia.earth/schema/Product#value)
- Data completeness assessment for other (which includes `seed`): [completeness.other](https://www.hestia.earth/schema/Completeness#other) must be `False`

## Implementation Status

- `Implemented`
- [Tested](../../../tests/cycle_models/test_seed.py)
