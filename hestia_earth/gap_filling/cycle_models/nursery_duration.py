from hestia_earth.utils.lookup import get_table_value, download_lookup
from hestia_earth.utils.tools import safe_parse_float

from hestia_earth.gap_filling.log import logger
from hestia_earth.gap_filling.utils import should_gap_fill, _gap_filled_version, _get_or_create_practice
from hestia_earth.gap_filling.cycle_utils import _should_gap_fill_term_type


GAP_FILLING_KEY = 'practices'
TERM_ID = 'nurseryDuration'


def _gap_fill_cycle(cycle: dict):
    lookup = download_lookup('crop.csv', True)
    practices = cycle.get(GAP_FILLING_KEY, [])

    for product in cycle.get('products', []):
        term_id = product.get('term', {}).get('@id', '')
        logger.debug('lookup data for Term: %s', term_id)
        if term_id in list(lookup.termid):
            value = safe_parse_float(get_table_value(lookup, 'termid', term_id, 'nursery_duration'), None)
            if value is not None:
                logger.info('key=%s, term=%s, value=%s', GAP_FILLING_KEY, TERM_ID, value)
                practice = _get_or_create_practice(practices, TERM_ID)
                practice['value'] = value
                return [_gap_filled_version(practice, 'value')]

    return []


def _need_gap_fill(cycle: dict):
    gap_fill = should_gap_fill(cycle.get(GAP_FILLING_KEY, []), TERM_ID) \
        and _should_gap_fill_term_type(cycle, TERM_ID)
    logger.info('key=%s, term=%s, gap_fill=%s', GAP_FILLING_KEY, TERM_ID, gap_fill)
    return gap_fill


def gap_fill(cycle: dict): return [(GAP_FILLING_KEY, _gap_fill_cycle(cycle) if _need_gap_fill(cycle) else [])]
