# Nursery Duration

This model gap-fills the length of the [nursery duration](https://hestia.earth/term/nurseryDuration), which is the length, in days, from the time from planting seedlings to the sale of marketable trees. This model adds this term and value as a [Practice](https://hestia.earth/schema/Practice).
It uses the crop specific average values derived from a variety sources, detailed in [Poore & Nemecek, 2018](https://science.sciencemag.org/content/360/6392/987).

## Gap-fills

- [practice.term](https://hestia.earth/schema/Practice#term) with [nurseryDuration](https://hestia.earth/term/nurseryDuration)
- [practice.value](https://hestia.earth/schema/Input#value)

## Requirements

- Crop term: [product.term](https://hestia.earth/schema/Product#term)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/cycle_models/test_nursery_duration.py)
