# Start Date

This model calculates the [startDate](https://hestia.earth/schema/Cycle#startDate) from the [endDate](https://hestia.earth/schema/Cycle#endDate) and [cycleDuration](https://hestia.earth/schema/Cycle#cycleDuration).

## Gap-fills

- [cycle.startDate](https://hestia.earth/schema/Cycle#startDate)

## Requirements

- [cycle.endDate](https://hestia.earth/schema/Cycle#endDate)
- [cycle.cycleDuration](https://hestia.earth/schema/Cycle#cycleDuration)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/cycle_models/test_start_date.py)
