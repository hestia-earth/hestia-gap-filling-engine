from hestia_earth.schema import TermTermType, CycleFunctionalUnitMeasure
from hestia_earth.utils.tools import list_average

from hestia_earth.gap_filling.log import logger
from hestia_earth.gap_filling.utils import should_gap_fill, _get_or_create_practice

GAP_FILLING_KEY = 'practices'
TERM_ID = 'irrigated'


def _gap_fill_practice(practices: list):
    logger.info('key=%s, term=%s', GAP_FILLING_KEY, TERM_ID)
    return _get_or_create_practice(practices, TERM_ID)


def _gap_fill_cycle(cycle: dict): return [_gap_fill_practice(cycle.get(GAP_FILLING_KEY))]


def _should_gap_fill_practices(practices: list):
    return not any([p for p in practices if p.get('term').get('termType') == TermTermType.WATERREGIME.value
                    and p.get('term').get('@id') != TERM_ID])


def _should_gap_fill_inputs(inputs: list, functionalUnitMeasure: str):
    irrigation_inputs = [i for i in inputs if i.get('term').get('termType') == TermTermType.WATER.value]
    value = sum([list_average(i.get('value')) for i in irrigation_inputs if len(i.get('value', [])) > 0])
    return len(irrigation_inputs) > 0 and (
        functionalUnitMeasure != CycleFunctionalUnitMeasure._1_HA.value or value > 250
    )


def _need_gap_fill(cycle: dict):
    gap_fill = _should_gap_fill_practices(cycle.get(GAP_FILLING_KEY, [])) \
        and _should_gap_fill_inputs(cycle.get('inputs', []), cycle.get('functionalUnitMeasure')) \
        and should_gap_fill(cycle.get(GAP_FILLING_KEY, []), TERM_ID, 'term')
    logger.info('key=%s, term=%s, gap_fill=%s', GAP_FILLING_KEY, TERM_ID, gap_fill)
    return gap_fill


def gap_fill(cycle: dict): return [(GAP_FILLING_KEY, _gap_fill_cycle(cycle) if _need_gap_fill(cycle) else [])]
