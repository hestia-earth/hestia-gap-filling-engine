from hestia_earth.schema import CycleFunctionalUnitMeasure

from hestia_earth.utils.api import download_hestia
from hestia_earth.utils.model import find_term_match
from hestia_earth.utils.lookup import get_table_value, download_lookup
from hestia_earth.utils.tools import safe_parse_float

from hestia_earth.gap_filling.log import logger
from hestia_earth.gap_filling.data.properties import _load_property
from hestia_earth.gap_filling.utils import should_gap_fill, _gap_filled_version, _get_or_create_property

GAP_FILLING_KEY = 'properties'
TERM_ID = 'rootingDepth'


def _should_gap_fill(cycle: dict):
    gap_fill = cycle.get('functionalUnitMeasure') == CycleFunctionalUnitMeasure._1_HA.value
    logger.info('key=%s, term=%s, gap_fill=%s', GAP_FILLING_KEY, TERM_ID, gap_fill)
    return gap_fill


def _gap_fill_property(properties: list, value: float, term: dict):
    logger.info('term=%s, value=%s', TERM_ID, value)
    prop = _get_or_create_property(properties, term)
    prop['value'] = value
    return _gap_filled_version(prop)


def _get_input_value_from_term(inputs: list, term_id: str):
    val = find_term_match(inputs, term_id, None)
    return val.get('value', [0])[0] if val is not None else 0


def _get_value(cycle: dict, term: dict, lookup_table, irrigation_names: list):
    term_id = term.get('@id', '')

    logger.debug('lookup data for Term: %s', term_id)
    if term_id in list(lookup_table.termid):
        # add an if statement to catch the irrigation level
        if cycle.get('dataCompleteness').get('water', False):
            value = sum([_get_input_value_from_term(cycle.get('inputs', []), term_id) for term_id in irrigation_names])

            # Assumes that if water data is complete and there are no data on irrigation then there was no irrigation.
            column = 'rooting_depth_irrigated_m' if value >= 250 else 'rooting_depth_rainfed_m'
        else:
            column = 'rooting_depth_average_m'

        value = safe_parse_float(get_table_value(lookup_table, 'termid', term_id, column), 0)
        return value if value > 0 else None
    return None


def _need_gap_fill_product(product: dict):
    gap_fill = should_gap_fill(product.get(GAP_FILLING_KEY, []), TERM_ID)
    logger.info('term=%s, gap_fill=%s', TERM_ID, gap_fill)
    return gap_fill


def _gap_fill_cycle(cycle: dict):
    term = download_hestia(TERM_ID)
    lookup_table = download_lookup('crop.csv', True)
    irrigation_names = _load_property('irrigations.txt').split('\n')

    def gap_fill_product(values):
        index = values[0]
        product = values[1]
        properties = product.get(GAP_FILLING_KEY, [])
        value = _get_value(cycle, product.get('term'), lookup_table, irrigation_names)
        property = _gap_fill_property(properties, value, term) if value is not None and term is not None else None
        return (f"products.{index}.{GAP_FILLING_KEY}" if property else None, [property])

    products = list(filter(lambda x: _need_gap_fill_product(x[1]), enumerate(cycle.get('products', []))))
    properties = list(map(gap_fill_product, products))
    return list(filter(lambda prop: prop[0] is not None, properties))


def gap_fill(cycle: dict): return _gap_fill_cycle(cycle) if _should_gap_fill(cycle) else []
