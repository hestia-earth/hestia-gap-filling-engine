# Orchard Density

This model gap-fills [orchard density](https://hestia.earth/term/orchardDensity), which is the number of trees required for 1 ha of mature orchard. This is added as a [Practice](https://hestia.earth/schema/Practice).
It uses the crop specific average values derived from a variety sources, detailed in [Poore & Nemecek, 2018](https://science.sciencemag.org/content/360/6392/987).

## Gap-fills

- [practice.term](https://hestia.earth/schema/Practice#term) with [orchardDuration](https://hestia.earth/term/orchardDensity)
- [practice.value](https://hestia.earth/schema/Input#value)

## Requirements

- Crop term: [product.term](https://hestia.earth/schema/Product#term)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/cycle_models/test_orchard_density.py)
