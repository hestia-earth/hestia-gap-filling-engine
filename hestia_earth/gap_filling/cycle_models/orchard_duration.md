# Orchard Duration

This model gap-fills the length of the [orchard duration](https://hestia.earth/term/orchardDuration), which is the length, in days, from the establishment of an orchard to its removal, and adds this as a [Practice](https://hestia.earth/schema/Practice).
It uses the crop specific average values derived from a variety sources, detailed in [Poore & Nemecek, 2018](https://science.sciencemag.org/content/360/6392/987).

## Gap-fills

- [practice.term](https://hestia.earth/schema/Practice#term) with [orchardDuration](https://hestia.earth/term/orchardDuration)
- [practice.value](https://hestia.earth/schema/Input#value)

## Requirements

- Crop term: [product.term](https://hestia.earth/schema/Product#term)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/cycle_models/test_orchard_duration.py)
