from typing import List
from functools import reduce
from hestia_earth.utils.model import find_term_match
from hestia_earth.utils.tools import safe_parse_float
from hestia_earth.utils.api import download_hestia, node_exists

from hestia_earth.gap_filling.log import logger
from hestia_earth.gap_filling.utils import should_gap_fill, _gap_filled_version
from hestia_earth.gap_filling.cycle_utils import _get_or_create_input

GAP_FILLING_KEY = 'inputs'


def _as_kg_mass(percent: int, value: int): return (1 / (percent/100)) * value


def _as_N(percent: int, value: int): return (percent/100) * value


SUFFIX_VALUE = 'KgMass'
SUFFIXES = ['KgMass', 'AsN']
SUFFIX_MAP = {
    'KgMass': {
        'opposite': 'AsN',
        'value_fn': _as_kg_mass
    },
    'AsN': {
        'opposite': 'KgMass',
        'value_fn': _as_N
    }
}


def _opposite_id(id: str, suffix: str): return f"{id}{SUFFIX_MAP[suffix]['opposite']}"


def _term_no_suffix_id(id: str): return reduce(lambda prev, curr: prev.replace(curr, ''), SUFFIXES, id)


def _term_no_suffix_exists(id: str): return all([node_exists(f"{id}{suffix}") for suffix in SUFFIXES])


def _term_no_suffix_should_gap_fill(inputs: List[dict]):
    def _should_gap_fill(id: str): return any([should_gap_fill(inputs, f"{id}{suffix}") for suffix in SUFFIXES])
    return _should_gap_fill


def _gap_fill_suffix(term_id: str, suffix: str):
    full_term_id = f"{term_id}{SUFFIX_VALUE}"
    full_term = download_hestia(full_term_id)
    property = find_term_match(full_term.get('defaultProperties', []), 'nitrogenContent', {'value': None})
    percent = safe_parse_float(property.get('value'))
    logger.debug('gap-fill Term: %s, value: %s', full_term_id, percent)
    return {'suffix': suffix, 'percent': percent} if percent else None


def _gap_fill_input(inputs: List[dict]):
    def _gap_fill(term_id: str):
        model = next((_gap_fill_suffix(term_id, suffix) for suffix in SUFFIXES
                      if should_gap_fill(inputs, f"{term_id}{suffix}")), None)
        if model is not None:
            suffix = model.get('suffix')
            suffix_term_id = f"{term_id}{suffix}"
            opposite_term_id = _opposite_id(term_id, suffix)
            opposite_value = find_term_match(inputs, opposite_term_id).get('value', [0])[0]
            value = SUFFIX_MAP[suffix]['value_fn'](model.get('percent'), opposite_value)

            logger.info('model=%s, value=%s', suffix_term_id, value)
            input = _get_or_create_input(inputs, suffix_term_id)
            input['value'] = [value]
            return _gap_filled_version(input)
    return _gap_fill


def _gap_fill_inputs(inputs: List[dict], term_ids: List[str]): return list(map(_gap_fill_input(inputs), term_ids))


def _need_gap_fill(inputs: List[dict]) -> List[str]:
    term_ids = [element.get('term', {}).get('@id') for element in inputs]
    term_ids = list(set(map(_term_no_suffix_id, term_ids)))
    term_ids = list(filter(_term_no_suffix_exists, term_ids))
    gap_fill = list(filter(_term_no_suffix_should_gap_fill(inputs), term_ids))
    logger.info('key=%s, gap_fill=%s', GAP_FILLING_KEY, gap_fill)
    return gap_fill


def gap_fill(cycle: dict):
    inputs = cycle.get(GAP_FILLING_KEY, [])
    term_ids = _need_gap_fill(inputs)
    return [(GAP_FILLING_KEY, _gap_fill_inputs(inputs, term_ids) if len(term_ids) > 0 else [])]
