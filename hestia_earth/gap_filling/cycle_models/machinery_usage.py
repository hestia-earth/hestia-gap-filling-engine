from hestia_earth.schema import SiteSiteType, InputStatsDefinition
from hestia_earth.utils.lookup import get_table_value, download_lookup
from hestia_earth.utils.model import find_term_match
from hestia_earth.utils.tools import safe_parse_float

from hestia_earth.gap_filling.log import logger
from hestia_earth.gap_filling.utils import should_gap_fill, _gap_filled_version
from hestia_earth.gap_filling.data.properties import _load_property
from hestia_earth.gap_filling.cycle_utils import _should_gap_fill_term_type, _get_or_create_input

GAP_FILLING_KEY = 'inputs'
TERM_ID = 'machineryInfrastructureDepreciatedAmountPerCycle'
MODEL_KEYS = 'value,statsDefinition'
VALID_SITE_TYPES = [
    SiteSiteType.CROPLAND.value,
    SiteSiteType.PERMANENT_PASTURE.value
]


def _get_input_value_from_term(inputs: list, term_id: str):
    val = find_term_match(inputs, term_id, None)
    return val.get('value', [0])[0] if val is not None else 0


def get_value(country_id: dict, cycle: dict):
    machinery_usage_country = download_lookup('region.csv', True)
    liquid_fuels = _load_property('liquid_fuel.txt').split('\n')

    logger.debug('lookup data for Term: %s', country_id)
    if country_id in list(machinery_usage_country.termid):
        hdi = safe_parse_float(get_table_value(machinery_usage_country, 'termid', country_id, 'hdi'), None)
        if hdi is not None:
            # if hdi >= 0.8 take use Diesel KG from row 1, if <0.8 use row 2
            machinery_usage = 11.5 if float(hdi) >= 0.8 else 23
            fuel_use = sum([_get_input_value_from_term(cycle.get('inputs', []), term_id) for term_id in liquid_fuels])
            return fuel_use/machinery_usage if fuel_use > 0 else None
    return None


def _gap_fill_input(inputs: list, value: float):
    logger.info('key=%s, term=%s, value=%s', GAP_FILLING_KEY, TERM_ID, value)
    input = _get_or_create_input(inputs, TERM_ID)
    input['value'] = [value]
    input['statsDefinition'] = InputStatsDefinition.SIMULATED.value
    return _gap_filled_version(input, MODEL_KEYS)


def _gap_fill_cycle(cycle: dict):
    country_id = cycle.get('site').get('country').get('@id')
    value = get_value(country_id, cycle)
    return [_gap_fill_input(cycle.get(GAP_FILLING_KEY, []), value)] if value is not None else []


def _need_gap_fill(cycle: dict):
    site_type = cycle.get('site', {}).get('siteType')
    gap_fill = site_type in VALID_SITE_TYPES \
        and should_gap_fill(cycle.get(GAP_FILLING_KEY, []), TERM_ID) \
        and _should_gap_fill_term_type(cycle, TERM_ID)
    logger.info('key=%s, term=%s, gap_fill=%s', GAP_FILLING_KEY, TERM_ID, gap_fill)
    # Add inputs liquid fuels into here.
    return gap_fill


def gap_fill(cycle: dict): return [(GAP_FILLING_KEY, _gap_fill_cycle(cycle) if _need_gap_fill(cycle) else [])]
