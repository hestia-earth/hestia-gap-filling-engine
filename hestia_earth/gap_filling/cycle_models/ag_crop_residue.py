from functools import reduce
from hestia_earth.utils.model import find_primary_product, find_term_match
from hestia_earth.utils.tools import safe_parse_float, list_average

from hestia_earth.gap_filling.log import logger
from hestia_earth.gap_filling.utils import _gap_filled_version, should_gap_fill
from hestia_earth.gap_filling.cycle_utils import _should_gap_fill_term_type, _get_or_create_product
from hestia_earth.gap_filling.cycle_pre_models.ag_crop_residue_total import TERM_ID as TOTAL_TERM_ID
from hestia_earth.gap_filling.ag_crop_residue_models import ag_crop_residue_burnt
from hestia_earth.gap_filling.ag_crop_residue_models import ag_crop_residue_incorporated
from hestia_earth.gap_filling.ag_crop_residue_models import ag_crop_residue_leftonfield
from hestia_earth.gap_filling.ag_crop_residue_models import ag_crop_residue_removed

GAP_FILLING_KEY = 'products'
TERM_ID = 'aboveGroundCropResidueIncorporated,aboveGroundCropResidueBurnt,aboveGroundCropResidueLeftOnField'
MODELS = [
    ag_crop_residue_removed,
    ag_crop_residue_incorporated,
    ag_crop_residue_burnt
]
REMAINING_TERM_ID = ag_crop_residue_leftonfield.TERM_ID


def _get_practice_value(model, cycle: dict) -> float:
    value = find_term_match(cycle.get('practices', []), model.PRACTICE_TERM_ID).get('value')
    return safe_parse_float(value) / 100


def _gap_fill_product(products: list, term_id: str, value: float):
    product = _get_or_create_product(products, term_id)
    logger.info('key=%s, term=%s, value=%s', GAP_FILLING_KEY, term_id, value)
    product['value'] = [value]
    return _gap_filled_version(product)


def _need_gap_fill_model(model, cycle: dict, primary_product: dict):
    practice_value = _get_practice_value(model, cycle)
    gap_fill = model.need_gap_fill(cycle, primary_product) \
        and practice_value is not None \
        and should_gap_fill(cycle.get(GAP_FILLING_KEY, []), model.TERM_ID)
    logger.info('key=%s, term=%s, gap_fill=%s', GAP_FILLING_KEY, model.TERM_ID, gap_fill)
    return gap_fill, practice_value


def _gap_fill_model(model, cycle: dict, primary_product: dict, total_value: float):
    need_gap_fill, practice_value = _need_gap_fill_model(model, cycle, primary_product)
    return total_value * practice_value if need_gap_fill else None


def _model_value(model, products: list):
    values = find_term_match(products, model.TERM_ID).get('value', [])
    return list_average(values) if len(values) > 0 else 0


def _gap_fill_cycle(cycle: dict, total_values: list):
    products = cycle.get(GAP_FILLING_KEY, [])
    primary_product = find_primary_product(cycle)
    total_value = list_average(total_values)
    # first, calculate the remaining value available after applying all user-uploaded data
    remaining_value = reduce(
        lambda prev, model: prev - _model_value(model, products),
        MODELS + [ag_crop_residue_leftonfield],
        total_value
    )

    values = []
    # then gap-fill every model in order up to the remaining value
    for model in MODELS:
        value = _gap_fill_model(model, cycle, primary_product, total_value)
        logger.debug('key=%s, term=%s, value=%s', GAP_FILLING_KEY, model.TERM_ID, value)
        if remaining_value > 0 and value is not None and value > 0:
            value = value if value < remaining_value else remaining_value
            values.extend([
                _gap_fill_product(products, model.TERM_ID, value)
            ])
            remaining_value = remaining_value - value
            if remaining_value == 0:
                logger.debug('no more residue, stopping')
                break

    return values + [
        # whatever remains is "left on field"
        _gap_fill_product(products, REMAINING_TERM_ID, remaining_value)
    ] if remaining_value > 0 else values


def _need_gap_fill(cycle: dict):
    total_values = find_term_match(cycle.get(GAP_FILLING_KEY, []), TOTAL_TERM_ID).get('value', [])
    gap_fill = len(total_values) > 0 and _should_gap_fill_term_type(cycle, TOTAL_TERM_ID)
    logger.info('key=%s, gap_fill=%s', GAP_FILLING_KEY, gap_fill)
    return gap_fill, total_values


def gap_fill(cycle: dict):
    need_gap_fill, total_values = _need_gap_fill(cycle)
    return [(GAP_FILLING_KEY, _gap_fill_cycle(cycle, total_values) if need_gap_fill else [])]
