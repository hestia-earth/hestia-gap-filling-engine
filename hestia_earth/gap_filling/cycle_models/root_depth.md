# Rooting Depth

This model estimates the [rootingDepth](https://hestia.earth/term/rootingDepth) of the crop, and adds it as a [Property](https://hestia.earth/schema/Property) to the crop.
It uses rooting depths from the [Global Crop Water Model](https://www.uni-frankfurt.de/45217788/FHP_07_Siebert_and_Doell_2008.pdf) that differentiate between irrigated and rainfed systems.
If no information is provided on whether the crop is irrigated or not, an average rooting depth estiamte is used. If total irrigation (sum of all irrigation applied) is >= 250m<sup>3</sup> per hectare then the rooting depth values for irrigated cycles are used, if < 250m<sup>3</sup> per hectare then rooting depth estimates for rainfed systems are returned. 

## Gap-fills

- [property.value](https://hestia.earth/schema/Property#value) with [rootingDepth](https://hestia.earth/term/rootingDepth)

## Requirements

- A [Product](https://hestia.earth/schema/Product) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?page=1&termType=crop)
- The [functionalUnitMeasure](https://hestia.earth/schema/Cycle#functionalUnitMeasure) for the cycle must be `1 ha`
- (`Optional`) The amount of irrigation water applied
- (`Optional`) Data completeness assessment for water: [dataCompleteness.water](https://hestia.earth/schema/Completeness#water)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/cycle_models/test_root_depth.py)
