from os.path import dirname, basename, isfile, join, abspath
from importlib import import_module
import sys
import glob


CURRENT_DIR = dirname(abspath(__file__)) + '/'
sys.path.append(CURRENT_DIR)
PKG = 'hestia_earth.gap_filling.cycle_models'
modules = glob.glob(join(dirname(__file__), '*.py'))
modules = [basename(f)[:-3] for f in modules if isfile(f) and not f.endswith('__init__.py')]
MODELS = list(map(lambda m: getattr(import_module(f".{m}", package=PKG), 'gap_fill'), modules))
