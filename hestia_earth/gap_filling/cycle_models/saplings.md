# Saplings

This model gap-fills [saplings](https://hestia.earth/term/saplings), which is the number of marketable saplings produced per hectare per year, and adds this as a [Input](https://hestia.earth/schema/Input).
It uses the crop specific average values derived from a variety sources, detailed in [Poore & Nemecek, 2018](https://science.sciencemag.org/content/360/6392/987).

## Gap-fills

- [input.term](https://hestia.earth/schema/Input#term) with [saplings](https://hestia.earth/term/saplings)
- [input.value](https://hestia.earth/schema/Input#value)

## Requirements

- Crop term: [product.term](https://hestia.earth/schema/Product#term)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/cycle_models/test_saplings.py)
