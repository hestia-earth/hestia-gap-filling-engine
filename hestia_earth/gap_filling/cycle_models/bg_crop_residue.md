# Below Ground Crop Residue

This model estimates the amount of below ground crop residue using the [IPCC (2019)](https://www.ipcc-nggip.iges.or.jp/public/2019rf/pdf/4_Volume4/19R_V4_Ch11_Soils_N2O_CO2.pdf) linear regression based methodology, based on crop type and crop yield.
For the following crop groups (Bananas, Berries, Nuts, Olives, Apples, Citrus, Orchard Fruit, Grapes, Coffee, Cocoa), below ground crop residue (roots) are assumed to be removed at orchard end of life, consistent with best practice pest control, therefore below ground crop residue is set to zero.
For oil palm, below ground crop residue is estimated at 180 kg (Corley & Tinker, 2008, The Oil Palm, 4th Edition).

## Gap-fills

- [product.term](https://hestia.earth/schema/Product#term) with [belowGroundCropResidue](https://hestia.earth/term/belowGroundCropResidue)
- [product.value](https://hestia.earth/schema/Product#value)

## Requirements

- [Product](https://hestia.earth/schema/Product) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?page=1&termType=crop) which is a [primary](https://hestia.earth/schema/Product#primary) product.
- Data completeness assessment for crop residue: [completeness.cropResidue](https://www.hestia.earth/schema/Completeness#cropResidue) must be `False`.

## Implementation Status

- `Implemented`
- [Tested](../../../tests/cycle_models/test_bg_crop_residue.py)
