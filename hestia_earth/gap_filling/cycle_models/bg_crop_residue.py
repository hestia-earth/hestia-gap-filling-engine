from hestia_earth.utils.model import find_primary_product
from hestia_earth.utils.lookup import get_table_value, download_lookup
from hestia_earth.utils.tools import safe_parse_float

from hestia_earth.gap_filling.log import logger
from hestia_earth.gap_filling.utils import should_gap_fill, _gap_filled_version, _get_property_value
from hestia_earth.gap_filling.cycle_utils import _should_gap_fill_term_type, _get_or_create_product

GAP_FILLING_KEY = 'products'
TERM_ID = 'belowGroundCropResidue'
PROPERTY_KEY = 'dryMatter'


def _get_value_dm(term_id: str, primary_product_yield: int, dm_percent: float):
    crop_res_dry_matter = download_lookup('crop.csv', True)

    logger.debug('lookup data for Term: %s', term_id)
    if term_id in list(crop_res_dry_matter.termid):
        # Multiply yield by dryMatter proportion
        yield_dm = primary_product_yield * (dm_percent / 100)
        # TODO with the spreadsheet there are a number of ways this value is calculated.
        #  Currently, the result of this gap-filling when applied to Sah et al does not match the gap-filled
        #  example due to hardcoded calc in the spreadsheet

        # estimate the BG DM calculation
        bg_slope = safe_parse_float(
            get_table_value(crop_res_dry_matter, 'termid', term_id, 'crop_residue_slope')
        )
        bg_intercept = safe_parse_float(
            get_table_value(crop_res_dry_matter, 'termid', term_id, 'crop_residue_intercept')
        )
        ab_bg_ratio = safe_parse_float(
            get_table_value(crop_res_dry_matter, 'termid', term_id, 'ratio_abv_to_below_grou_crop_residue')
        )

        above_ground_residue = yield_dm * bg_slope + bg_intercept * 1000

        # TODO: Update to include fraction renewed addition of
        #  https://www.ipcc-nggip.iges.or.jp/public/2019rf/pdf/4_Volume4/19R_V4_Ch11_Soils_N2O_CO2.pdf
        #  only if site.type = pasture
        # multiply by the ratio of above to below matter
        return None if bg_slope is None or bg_intercept is None or ab_bg_ratio is None \
            else (above_ground_residue + yield_dm) * ab_bg_ratio
    return None


def _get_value_default(primary_product: dict):
    crop_res = download_lookup('crop.csv', True)

    term_id = primary_product.get('term', {}).get('@id', '')

    logger.debug('lookup data for Term: %s', term_id)
    if term_id in list(crop_res.termid):
        # take the default ag crop residue for the crop in question
        value = safe_parse_float(
            get_table_value(crop_res, 'termid', term_id, 'default_bg_dm_crop_residue'), None
        )
        return value if value is not None else []

    return None


def _gap_fill_product(products: list, value: float):
    product = _get_or_create_product(products, TERM_ID)
    logger.info('key=%s, term=%s, value=%s', GAP_FILLING_KEY, TERM_ID, value)
    product['value'] = [value]
    return _gap_filled_version(product)


def _gap_fill_cycle(cycle: dict, primary_product: dict):
    dm_property = _get_property_value(primary_product, PROPERTY_KEY) if primary_product is not None else None
    term_id = primary_product.get('term', {}).get('@id', '')

    #  1) use dm regression, or 2) use default for orchard crops
    value = _get_value_default(primary_product) if dm_property is None \
        else _get_value_dm(term_id, primary_product.get('value', [0])[0], safe_parse_float(dm_property.get('value')))

    return [_gap_fill_product(cycle.get(GAP_FILLING_KEY, []), value)] if value is not None else []


def _need_gap_fill(cycle: dict):
    product = find_primary_product(cycle)
    gap_fill = product is not None \
        and should_gap_fill(cycle.get(GAP_FILLING_KEY, []), TERM_ID) \
        and _should_gap_fill_term_type(cycle, TERM_ID)
    logger.info('key=%s, term=%s, gap_fill=%s', GAP_FILLING_KEY, TERM_ID, gap_fill)
    return gap_fill, product


def gap_fill(cycle: dict):
    need_gap_fill, product = _need_gap_fill(cycle)
    return [(GAP_FILLING_KEY, _gap_fill_cycle(cycle, product) if need_gap_fill else [])]
