# Irrigated

This model gap-fills the Practice [irrigated](https://hestia.earth/term/irrigated).
Cycles are marked as irrigated if the sum of the [irrigation Inputs](https://hestia.earth/glossary?termType=water) is greater than 25mm per hectare (250m3 per hectare).

## Gap-fills

- [practice.term](https://hestia.earth/schema/Product#term) with [irrigated](https://hestia.earth/term/irrigated)

## Requirements

- [Practice](https://hestia.earth/schema/Practice) of [termType](https://hestia.earth/schema/Term#termType) = [waterRegime](https://hestia.earth/glossary?termType=waterRegime) and [Input](https://hestia.earth/schema/Input) of [termType](https://hestia.earth/schema/Term#termType) = [water](https://hestia.earth/glossary?termType=water)
- [cycle.functionalUnitMeasure](https://hestia.earth/schema/Cycle#functionalUnitMeasure) = `1 ha` and the sum of the [cycle.inputs](https://hestia.earth/schema/Cycle#inputs) of [termType](https://hestia.earth/schema/Term#termType) = [water](https://hestia.earth/glossary?termType=water) is above `250` (m3 per hectare).

## Implementation Status

- `Implemented`
- [Tested](../../../tests/cycle_models/test_irrigated.py)
