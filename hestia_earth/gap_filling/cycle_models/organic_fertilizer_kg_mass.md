# Organic Fertilizer to Kg or Mass

This model uses the default [nitrogenContent](https://hestia.earth/term/nitrogenContent) of [organic fertilizers](https://www.hestia.earth/glossary?page=1&termType=organicFertilizer) to convert a fertilzer amount from kilograms of mass to kilograms of nitrogen equivalents or vice versa.

## Gap-fills

- [input.term](https://hestia.earth/schema/Input#term) with an [organic fertilizer](https://www.hestia.earth/glossary?page=1&termType=organicFertilizer) with the suffix "(as N)" if the term was originally "(kg mass)" or "(kg mass)" if the term was originally "(kg N)".
- [input.value](https://hestia.earth/schema/Input#value)

## Requirements

- One or more [Inputs](https://hestia.earth/schema/Input) of [termType](https://hestia.earth/schema/Term#termType) = [organicFertilizer](https://hestia.earth/glossary?page=1&termType=organicFertilizer)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/cycle_models/test_organic_fertilizer_kg_mass.py)
