# Above Ground Crop Residue

This model gap-fills the amounts and destinations of above ground crop residue, working in the following order:
1. [Above ground crop residue, removed](https://hestia.earth/term/aboveGroundCropResidueRemoved);
2. [Above ground crop residue, incorporated](https://hestia.earth/term/aboveGroundCropResidueIncorporated);
3. [Above ground crop residue, burnt](https://hestia.earth/term/aboveGroundCropResidueBurnt);
4. [Above ground crop residue, left on field](https://hestia.earth/term/aboveGroundCropResidueLeftOnField).

For more details about each of the models, please see the models documentation [here](../ag_crop_residue_models).

## Gap-fills

- [product.term](https://hestia.earth/schema/Product#term) with any of: [aboveGroundCropResidueRemoved](https://hestia.earth/term/aboveGroundCropResidueRemoved), [aboveGroundCropResidueIncorporated](https://hestia.earth/term/aboveGroundCropResidueIncorporated), [aboveGroundCropResidueBurnt](https://hestia.earth/term/aboveGroundCropResidueBurnt) or [aboveGroundCropResidueLeftOnField](https://hestia.earth/term/aboveGroundCropResidueLeftOnField)
- [product.value](https://hestia.earth/schema/Product#value)

## Requirements

- Data completeness assessment for crop residue: [completeness.cropResidue](https://www.hestia.earth/schema/Completeness#cropResidue) must be `False`.

For more details about the requirements of each model, please see the documentation [here](../ag_crop_residue_models).

## Implementation Status

- `Implemented`
- [Tested](../../../tests/cycle_models/test_ag_crop_residue.py)
