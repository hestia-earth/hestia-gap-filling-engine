from dateutil.relativedelta import relativedelta
from hestia_earth.utils.tools import safe_parse_date

from hestia_earth.gap_filling.log import logger
from hestia_earth.gap_filling.utils import _is_in_days

GAP_FILLING_KEY = 'startDate'


def _gap_fill_cycle(cycle: dict):
    days = -float(cycle.get('cycleDuration'))
    start_date = (safe_parse_date(cycle.get('endDate')) + relativedelta(days=days)).strftime('%Y-%m-%d')
    logger.info('key=%s, value=%s', GAP_FILLING_KEY, start_date)
    return start_date


def _need_gap_fill(cycle: dict):
    gap_fill = _is_in_days(cycle.get('endDate')) and cycle.get('cycleDuration') is not None \
        and cycle.get(GAP_FILLING_KEY) is None
    logger.info('key=%s, gap_fill=%s', GAP_FILLING_KEY, gap_fill)
    return gap_fill


def gap_fill(cycle: dict): return [(GAP_FILLING_KEY, _gap_fill_cycle(cycle) if _need_gap_fill(cycle) else [])]
