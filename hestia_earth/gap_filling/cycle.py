from .gap_filling import run_parallel, run_serie
from .cycle_pre_models import site as site_pre
from .cycle_pre_models import ag_crop_residue_practices
from .cycle_pre_models import ag_crop_residue_removed
from .cycle_pre_models import ag_crop_residue_total
from .cycle_pre_models import primary_product
from .cycle_models import MODELS
from .cycle_post_models import data_completeness
from .cycle_post_models import nitrogen_content
from .cycle_post_models import site as site_post

PRE_MODELS = [
    site_pre.gap_fill,
    primary_product.gap_fill,
    ag_crop_residue_removed.gap_fill,
    ag_crop_residue_practices.gap_fill,
    ag_crop_residue_total.gap_fill
]
POST_MODELS = [
    nitrogen_content.gap_fill,
    data_completeness.gap_fill,
    site_post.gap_fill
]


def gap_fill(cycle: dict):
    """
    Gap-fill an Hestia `Cycle` with all the models in:
        - cycle_pre_models
        - cycle_models
        - cycle_post_models

    Parameters
    ----------
    cycle : dict
        The `Cycle` to gap-fill.

    Returns
    -------
    dict
        The `Cycle` with gap-filled content
    """
    data = run_serie(cycle, PRE_MODELS)
    data = run_parallel(data, MODELS)
    return run_serie(data, POST_MODELS)
