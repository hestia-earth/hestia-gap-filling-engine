# Cycle

This model is run only if the [pre model](../impact_assessment_pre_models/cycle.md) with the same name has been run before.
This model will restore the `impactAssessment.cycle` as a "linked node" (i.e. it will be set with only `@type`, `@id` and `name` keys).

## Gap-fills

- [impactAssessment.cycle](https://hestia.earth/schema/Cycle#cycle)

## Requirements

- [cycle.@id](https://hestia.earth/schema/Cycle#id) must be set (is linked to an existing Cycle)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/impact_assessment_post_models/test_cycle.py)
