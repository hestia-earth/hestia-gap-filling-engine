# this is not a gap-filling model
# we need to remove the complete cycle object after all have been gap-filled
from hestia_earth.utils.model import linked_node

from hestia_earth.gap_filling.log import logger

GAP_FILLING_KEY = 'cycle'


def _gap_fill_impact(impact: dict): return linked_node(impact.get('cycle'))


def _need_gap_fill(impact: dict):
    cycle_id = impact.get('cycle', {}).get('@id')
    gap_fill = cycle_id is not None
    logger.info('key=%s, gap_fill=%s', GAP_FILLING_KEY, gap_fill)
    return gap_fill


def gap_fill(impact: dict): return [(GAP_FILLING_KEY, _gap_fill_impact(impact) if _need_gap_fill(impact) else [])]
