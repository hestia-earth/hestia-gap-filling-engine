from hestia_earth.utils.tools import list_average

from hestia_earth.gap_filling.log import logger
from hestia_earth.gap_filling.utils import _gap_filled_version

GAP_FILLING_KEY = 'measurements'


def _need_gap_fill_measurement(measurement: dict):
    return ('value' not in measurement or len(measurement['value']) == 0) and \
        len(measurement.get('min', [])) > 0 and len(measurement.get('max', [])) > 0


def _gap_fill_measurement(measurement: dict):
    value = list_average(measurement.get('min') + measurement.get('max'))
    logger.info('key=%s, term=%s, value=%s', GAP_FILLING_KEY, measurement.get('term', {}).get('@id'), value)
    measurement['value'] = [value]
    return _gap_filled_version(measurement)


def _need_gap_fill(site: dict):
    measurements = list(filter(_need_gap_fill_measurement, site.get('measurements', [])))
    gap_fill = len(measurements) > 0
    logger.info('key=%s, gap_fill=%s', GAP_FILLING_KEY, gap_fill)
    return gap_fill, measurements


def gap_fill(site: dict):
    gap_fill, measurements = _need_gap_fill(site)
    return [(GAP_FILLING_KEY, list(map(_gap_fill_measurement, measurements)) if gap_fill else [])]
