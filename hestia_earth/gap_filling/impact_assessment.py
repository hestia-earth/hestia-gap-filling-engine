from .gap_filling import run_parallel, run_serie
from .impact_assessment_pre_models import cycle as cycle_pre
from .impact_assessment_models import MODELS
from .impact_assessment_post_models import cycle as cycle_post

PRE_MODELS = [
    cycle_pre.gap_fill
]
POST_MODELS = [
    cycle_post.gap_fill
]


def gap_fill(impact_assessment: dict):
    """
    Gap-fill an Hestia `ImpactAssessment` with all the models in:
        - impact_assessment_models

    Parameters
    ----------
    impact_assessment : dict
        The `ImpactAssessment` to gap-fill.

    Returns
    -------
    dict
        The `ImpactAssessment` with gap-filled content
    """
    data = run_serie(impact_assessment, PRE_MODELS)
    data = run_parallel(data, MODELS)
    return run_serie(data, POST_MODELS)
