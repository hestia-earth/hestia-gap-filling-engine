# Nitrogen Content

This model adds an average [nitrogenContent](https://hestia.earth/term/nitrogenContent) of the crop, and adds it as a [Property](https://hestia.earth/schema/Property) to the `Above Ground Crop Residue Total` and `Below Ground Crop Residue` products.
It uses values from [IPCC 2019 Volume 4 Chapter 11 Table 11.1A](https://www.ipcc-nggip.iges.or.jp/public/2019rf/pdf/4_Volume4/19R_V4_Ch11_Soils_N2O_CO2.pdf).

## Gap-fills

- [property.value](https://hestia.earth/schema/Property#value) with [nitrogenContent](https://hestia.earth/term/nitrogenContent)

## Requirements

- A [Product](https://hestia.earth/schema/Product) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/cycle_post_models/test_crop_residue_nitrogen_content.py)
