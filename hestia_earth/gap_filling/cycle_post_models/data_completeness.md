# Data Completeness

The [Data Completeness](https://hestia.earth/schema/Cycle#dataCompleteness) blank node in Cycle specifies how complete the [Inputs](https://hestia.earth/schema/Cycle#inputs) and [Products](https://hestia.earth/schema/Cycle#products) data are.
This model gap-fills the data completeness according to specific rules per key.

For more details about each of the models, please see the models documentation [here](../data_completeness_models).

## Gap-fills

- [dataCompleteness](https://hestia.earth/schema/Cycle#dataCompleteness)

## Requirements

`N/A`

## Implementation Status

- `Implemented`
- [Tested](../../../tests/cycle_post_models/test_data_completeness.py)
