# Site

This model is run only if the [pre model](../cycle_pre_models/site.md) with the same name has been run before.
This model will restore the `cycle.site` as a "linked node" (i.e. it will be set with only `@type`, `@id` and `name` keys).

## Gap-fills

- [cycle.site](https://hestia.earth/schema/Cycle#site)

## Requirements

- [site.@id](https://hestia.earth/schema/Site#id) must be set (is linked to an existing Site)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/cycle_post_models/test_site.py)
