from functools import reduce
from hestia_earth.utils.tools import non_empty_list

from hestia_earth.gap_filling.log import logger
from hestia_earth.gap_filling.utils import _gap_filled_version
from hestia_earth.gap_filling.data_completeness_models import crop_residue

GAP_FILLING_KEY = 'dataCompleteness'
MODELS = [
    crop_residue
]


def _need_gap_fill_model(model, cycle: dict):
    is_complete = cycle.get(GAP_FILLING_KEY, {}).get(model.GAP_FILLING_KEY)
    gap_fill = is_complete is False
    logger.info('key=%s, gap_fill=%s', '.'.join([GAP_FILLING_KEY, model.GAP_FILLING_KEY]), gap_fill)
    return gap_fill


def _gap_fill_model(model, cycle: dict):
    return {model.GAP_FILLING_KEY: model.gap_fill(cycle)} if _need_gap_fill_model(model, cycle) else None


def _gap_fill_cycle(cycle: dict):
    gap_fills = non_empty_list([_gap_fill_model(model, cycle) for model in MODELS])
    value = reduce(lambda prev, gap_fill: {**prev, **gap_fill}, gap_fills, cycle.get(GAP_FILLING_KEY, {}))
    gap_filled_keys = ','.join([next(iter(gap_fill)) for gap_fill in gap_fills])
    logger.info('key=%s, gap_fill_keys=%s', GAP_FILLING_KEY, gap_filled_keys)
    return _gap_filled_version(value, gap_filled_keys) if len(gap_fills) > 0 else None


def gap_fill(cycle: dict): return [(GAP_FILLING_KEY, _gap_fill_cycle(cycle))]
