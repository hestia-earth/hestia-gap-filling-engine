# this is not a gap-filling model
# we need to remove the complete site object after all have been gap-filled
from hestia_earth.utils.model import linked_node

from hestia_earth.gap_filling.log import logger

GAP_FILLING_KEY = 'site'


def _gap_fill_cycle(cycle: dict): return linked_node(cycle.get('site'))


def _need_gap_fill(cycle: dict):
    site_id = cycle.get('site', {}).get('@id')
    gap_fill = site_id is not None
    logger.info('key=%s, gap_fill=%s', GAP_FILLING_KEY, gap_fill)
    return gap_fill


def gap_fill(cycle: dict): return [(GAP_FILLING_KEY, _gap_fill_cycle(cycle) if _need_gap_fill(cycle) else [])]
