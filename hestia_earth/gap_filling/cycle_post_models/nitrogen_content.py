from hestia_earth.utils.model import find_primary_product
from hestia_earth.utils.lookup import get_table_value, download_lookup, column_name
from hestia_earth.utils.tools import safe_parse_float

from hestia_earth.gap_filling.log import logger
from hestia_earth.gap_filling.utils import should_gap_fill, _gap_filled_version, _get_or_create_property
from hestia_earth.gap_filling.cycle_pre_models import ag_crop_residue_total
from hestia_earth.gap_filling.cycle_models import bg_crop_residue

GAP_FILLING_KEY = 'properties'
TERM_ID = 'nitrogenContent'
PRODUCT_COLUMN_MAPPING = {
    ag_crop_residue_total.TERM_ID: 'N_Content_AG_Residue',
    bg_crop_residue.TERM_ID: 'N_Content_BG_Residue'
}


def _gap_fill_property(properties: list, value: float, product_term_id: str):
    logger.info('term=%s, product=%s, value=%s', TERM_ID, product_term_id, value)
    prop = _get_or_create_property(properties, TERM_ID)
    prop['value'] = value
    return _gap_filled_version(prop)


def _get_value(crop_id: str, term: dict):
    lookup_table = download_lookup('crop.csv', True)
    term_id = term.get('@id', '')

    logger.debug('lookup data for Term: %s', term_id)
    if crop_id in list(lookup_table.termid):
        column = column_name(PRODUCT_COLUMN_MAPPING[term_id])
        value = safe_parse_float(get_table_value(lookup_table, 'termid', crop_id, column), 0)
        return value if value > 0 else None
    return None


def _need_gap_fill_product(product: dict):
    product_term_id = product.get('term', {}).get('@id')
    gap_fill = product_term_id in PRODUCT_COLUMN_MAPPING \
        and should_gap_fill(product.get(GAP_FILLING_KEY, []), TERM_ID)
    logger.info('term=%s, product=%s, gap_fill=%s', TERM_ID, product_term_id, gap_fill)
    return gap_fill


def _gap_fill_cycle(cycle: dict, primary_product: dict):
    product_id = primary_product.get('term').get('@id')

    def gap_fill_product(values):
        index = values[0]
        product = values[1]
        properties = product.get(GAP_FILLING_KEY, [])
        product_term = product.get('term', {})
        value = _get_value(product_id, product_term)
        property = _gap_fill_property(properties, value, product_term.get('@id')) if value is not None else None
        return (f"products.{index}.{GAP_FILLING_KEY}" if property else None, [property])

    products = list(filter(lambda x: _need_gap_fill_product(x[1]), enumerate(cycle.get('products', []))))
    properties = list(map(gap_fill_product, products))
    return list(filter(lambda prop: prop[0] is not None, properties))


def _need_gap_fill(cycle: dict):
    product = find_primary_product(cycle)
    gap_fill = product is not None
    logger.info('key=%s, term=%s, gap_fill=%s', GAP_FILLING_KEY, TERM_ID, gap_fill)
    return gap_fill, product


def gap_fill(cycle: dict):
    need_gap_fill, product = _need_gap_fill(cycle)
    return _gap_fill_cycle(cycle, product) if need_gap_fill else []
