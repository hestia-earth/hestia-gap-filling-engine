from hestia_earth.gap_filling.log import logger

GAP_FILLING_KEY = 'irrigated'


def _gap_fill_impact(impact: dict):
    practices = impact.get('cycle', {}).get('practices', [])
    value = next((p for p in practices if p.get('term').get('@id') == 'irrigated'), None) is not None
    logger.info('key=%s, value=%s', GAP_FILLING_KEY, value)
    return value


def _need_gap_fill(impact: dict):
    practices = impact.get('cycle', {}).get('practices', [])
    gap_fill = len(practices) > 0
    logger.info('key=%s, gap_fill=%s', GAP_FILLING_KEY, gap_fill)
    return gap_fill


def gap_fill(impact: dict): return [(GAP_FILLING_KEY, _gap_fill_impact(impact) if _need_gap_fill(impact) else None)]
