from hestia_earth.utils.model import find_primary_product

from hestia_earth.gap_filling.log import logger

GAP_FILLING_KEY = 'product'


def _gap_fill_impact(impact: dict):
    primary_product = find_primary_product(impact.get('cycle', {})).get('term')
    logger.info('key=%s, value=%s', GAP_FILLING_KEY, primary_product.get('@id'))
    return primary_product


def _need_gap_fill(impact: dict):
    gap_fill = find_primary_product(impact.get('cycle', {})) is not None
    logger.info('key=%s, gap_fill=%s', GAP_FILLING_KEY, gap_fill)
    return gap_fill


def gap_fill(impact: dict): return [(GAP_FILLING_KEY, _gap_fill_impact(impact) if _need_gap_fill(impact) else None)]
