# Organic

Detects if the Cycle has an organic label.

## Gap-fills

- [impactAssessment.organic](https://www.hestia.earth/schema/ImpactAssessment#organic)

## Requirements

- [impactAssessment.cycle](https://www.hestia.earth/schema/ImpactAssessment#cycle) with at least one [practice](https://www.hestia.earth/schema/Cycle#practices)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/impact_assessment_models/test_organic.py)
