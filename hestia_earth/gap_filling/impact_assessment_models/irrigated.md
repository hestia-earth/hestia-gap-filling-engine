# Irrigated

Detects if the Cycle was irrigated.

## Gap-fills

- [impactAssessment.irrigated](https://www.hestia.earth/schema/ImpactAssessment#irrigated)

## Requirements

- [impactAssessment.cycle](https://www.hestia.earth/schema/ImpactAssessment#cycle) with at least one [practice](https://www.hestia.earth/schema/Cycle#practices)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/impact_assessment_models/test_irrigated.py)
