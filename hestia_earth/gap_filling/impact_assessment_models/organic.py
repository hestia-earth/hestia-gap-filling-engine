from hestia_earth.utils.lookup import get_table_value, download_lookup, column_name

from hestia_earth.gap_filling.log import logger

GAP_FILLING_KEY = 'organic'


def _is_organic(lookup, term_id: str):
    return get_table_value(lookup, 'termid', term_id, column_name('isOrganic')) == GAP_FILLING_KEY


def _gap_fill_impact(impact: dict):
    lookup = download_lookup('standardsLabels.csv')
    practices = impact.get('cycle', {}).get('practices', [])
    value = any([_is_organic(lookup, p.get('term').get('@id')) for p in practices])
    logger.info('key=%s, value=%s', GAP_FILLING_KEY, value)
    return value


def _need_gap_fill(impact: dict):
    practices = impact.get('cycle', {}).get('practices', [])
    gap_fill = len(practices) > 0
    logger.info('key=%s, gap_fill=%s', GAP_FILLING_KEY, gap_fill)
    return gap_fill


def gap_fill(impact: dict): return [(GAP_FILLING_KEY, _gap_fill_impact(impact) if _need_gap_fill(impact) else None)]
