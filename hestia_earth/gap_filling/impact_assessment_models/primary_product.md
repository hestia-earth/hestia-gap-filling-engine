# Primary Product

The `product` of an [ImpactAssessment](https://www.hestia.earth/schema/ImpactAssessment) is the [primary](https://www.hestia.earth/schema/Product#primary) product of the `Cycle`.

## Gap-fills

- [impactAssessment.product](https://www.hestia.earth/schema/ImpactAssessment#product)

## Requirements

- [impactAssessment.cycle](https://www.hestia.earth/schema/ImpactAssessment#cycle)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/impact_assessment_models/test_primary_product.py)
