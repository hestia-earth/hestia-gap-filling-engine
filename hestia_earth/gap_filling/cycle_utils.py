from hestia_earth.schema import InputJSONLD, ProductJSONLD
from hestia_earth.utils.api import download_hestia
from hestia_earth.utils.model import find_term_match, linked_node

from .utils import _term_id, _gap_filled_version


def _should_gap_fill_term_type(cycle: dict, term_id: str):
    term = download_hestia(term_id)
    term_type = term.get('termType')
    data_completeness = cycle.get('dataCompleteness', {})
    return term_type not in data_completeness or data_completeness[term_type] is False


def _get_or_create_input(inputs: list, term):
    term_id = _term_id(term)
    input = find_term_match(inputs, term_id, None)
    if input is None:
        input = InputJSONLD().to_dict()
        input['term'] = linked_node(term if isinstance(term, dict) else download_hestia(term_id))
        return _gap_filled_version(input, 'term')
    return input


def _new_product(term):
    product = ProductJSONLD().to_dict()
    product['term'] = linked_node(term if isinstance(term, dict) else download_hestia(_term_id(term)))
    return _gap_filled_version(product, 'term')


def _get_or_create_product(products: list, term):
    product = find_term_match(products, _term_id(term), None)
    return _new_product(term) if product is None else product
