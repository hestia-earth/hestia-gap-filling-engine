from pkgutil import extend_path
from hestia_earth.schema import NodeType
from hestia_earth.utils.tools import current_time_ms

from .log import logger
from .cycle import gap_fill as gap_fill_cycle
from .site import gap_fill as gap_fill_site
from .impact_assessment import gap_fill as gap_fill_impact_assessment

__path__ = extend_path(__path__, __name__)

GAP_FILL_NODE_TYPE = {
    NodeType.CYCLE.value: lambda v: gap_fill_cycle(v),
    NodeType.IMPACTASSESSMENT.value: lambda v: gap_fill_impact_assessment(v),
    NodeType.SITE.value: lambda v: gap_fill_site(v)
}


def gap_fill(node: dict):
    """
    Gap-fill an Hestia Node.

    Parameters
    ----------
    node : dict
        The node to gap-fill. Can be a `Cycle`, `Site` or `ImpactAssessment`.

    Returns
    -------
    dict
        The node with gap-filled content
    """
    now = current_time_ms()
    node_type = node.get('@type')
    data = GAP_FILL_NODE_TYPE[node_type](node) if node_type in GAP_FILL_NODE_TYPE else {}
    logger.info('time=%s, unit=ms', current_time_ms() - now)
    return data
