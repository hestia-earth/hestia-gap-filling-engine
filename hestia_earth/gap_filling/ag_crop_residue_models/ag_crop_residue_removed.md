# Above Ground Crop Residue - Removed From Field

This model estimates the amount of above ground crop residue using the [IPCC (2019)](https://www.ipcc-nggip.iges.or.jp/public/2019rf/pdf/4_Volume4/19R_V4_Ch11_Soils_N2O_CO2.pdf) linear regression based methodology, based on crop type and crop yield.
It then estimates how much of that crop residue is removed from the field using country and crop specific factors from [Köble (2014)](https://gnoc.jrc.ec.europa.eu/documentation/The_Global_Nitrous_Oxide_Calculator_User_Manual_version_1_2_4.pdf).

## Gap-fills

- [product.term](https://hestia.earth/schema/Product#term) [aboveGroundCropResidueRemoved](https://hestia.earth/term/aboveGroundCropResidueRemoved)
- [product.value](https://hestia.earth/schema/Product#value)

OR

- [practice.term](https://hestia.earth/schema/Practice#term) with [residueRemoved](https://hestia.earth/term/residueRemoved)
- [practice.value](https://hestia.earth/schema/Practice#value)

## Requirements

- [Product](https://hestia.earth/schema/Product) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?page=1&termType=crop) which is a [primary](https://hestia.earth/schema/Product#primary) product and reports the [yield](https://hestia.earth/schema/Product#value);
- Country: [site.country](https://hestia.earth/schema/Site#country).
- Data completeness assessment for crop residue: [completeness.cropResidue](https://www.hestia.earth/schema/Completeness#cropResidue) must be `False`.

## Implementation Status

- `Implemented`
- [Tested](../../../tests/ag_crop_residue_models/test_ag_crop_residue_removed.py)
