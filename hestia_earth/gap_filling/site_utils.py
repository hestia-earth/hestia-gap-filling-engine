from hestia_earth.schema import SchemaType, MeasurementJSONLD
from hestia_earth.utils.api import download_hestia, find_node_exact, find_related
from hestia_earth.utils.model import find_term_match, linked_node
from hestia_earth.utils.tools import list_average

from .utils import _term_id, _gap_filled_version


def _new_measurement(term, biblio_title=None):
    measurement = MeasurementJSONLD().to_dict()
    measurement['term'] = linked_node(term if isinstance(term, dict) else download_hestia(_term_id(term)))
    measurement = _gap_filled_version(measurement, 'term')
    if biblio_title:
        source = find_node_exact(SchemaType.SOURCE, {'bibliography.title': biblio_title})
        if source:
            measurement['source'] = linked_node({'@type': SchemaType.SOURCE.value, **source})
            measurement = _gap_filled_version(measurement, 'source')
    return measurement


def _get_or_create_measurement(measurements: list, term, biblio_title=None):
    measurement = find_term_match(measurements, _term_id(term), None)
    return _new_measurement(term, biblio_title) if measurement is None else measurement


def measurement_value_average(measurement: dict): return list_average(measurement.get('value', [0]))


def _related_cycles(type: SchemaType, id: str):
    nodes = find_related(type, id, SchemaType.CYCLE)
    return [] if nodes is None else list(map(lambda node: download_hestia(node.get('@id'), SchemaType.CYCLE), nodes))
