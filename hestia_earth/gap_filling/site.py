from .gap_filling import run_parallel, run_serie
from .site_models import MODELS
from .site_post_models import measurement_value

PRE_MODELS = []
POST_MODELS = [
    measurement_value.gap_fill
]


def gap_fill(site: dict):
    """
    Gap-fill an Hestia `Site` with all the models in:
        - site_models
        - site_post_models

    Parameters
    ----------
    site : dict
        The `Site` to gap-fill.

    Returns
    -------
    dict
        The `Site` with gap-filled content
    """
    data = run_serie(site, PRE_MODELS)
    data = run_parallel(data, MODELS)
    return run_serie(data, POST_MODELS)
