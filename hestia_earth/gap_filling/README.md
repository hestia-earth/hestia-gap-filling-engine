# Hestia Gap Filling Engine

The gap-filling engine is designed to gap-fill a Node running a 3-steps process:
1. It gap-fills `pre-models`: these models are run in sequence one after another to ensute the order is preserved.
1. It gap-fills a list of `models` in parallel: all the models run during this step do not need to run in order.
1. It gap-fills `post-models`: these models are run in sequence the same way as for the "pre-models". They typically contain models that required previous model to run.

**Note**: `pre-models` and `post-models` might not exist in some cases as they are not required to run the `models` in parallel.

You can find documentations for every model in their own `.md` files:
* [Cycle Pre-Models](./cycle_pre_models)
* [Cycle Models](./cycle_models)
* [Cycle Post-Models](./cycle_post_models)
* [ImpactAssessment Models](./impact_assessment_models)
* [Site Models](./site_models)
* [Site Post-Models](./site_post_models)

## Usage Examples

1. To run all the models on a `Cycle`:
```python
from hestia_earth.gap_filling import gap_fill

result = gap_fill(cycle)
```
2. To run a single model on a `Cycle`:
```python
from hestia_earth.gap_filling.gap_filling import run_serie
from hestia_earth.gap_filling.cycle_models import ag_crop_residue_burnt

result = run_serie(cycle, [ag_crop_residue_burnt.gap_fill])
```
3. To run a list of models in sequence:
```python
from hestia_earth.gap_filling.gap_filling import run_serie
from hestia_earth.gap_filling.cycle_pre_models import site
from hestia_earth.gap_filling.cycle_models import machinery_usage

# machinery_usage model requires the cycle to contain the full site, so if the Cycle is in Hestia you can
# 1. Gap-fill the site
# 2. Gap-fill the machineryInfrastructureDepreciatedAmountPerCycle term
# If the Cycle already contains the full site information, you can skip the `site.gap_fill` model entirely.
result = run_serie(cycle, [site.gap_fill, machinery_usage.gap_fill])
```
4. To run a list of models in parallel (does not conserve order):
```python
from hestia_earth.gap_filling.gap_filling import run_parallel
from hestia_earth.gap_filling.site_models import climate_zone
from hestia_earth.gap_filling.site_models import drainage_class

result = run_parallel(cycle, [climate_zone.gap_fill, drainage_class.gap_fill])
```
