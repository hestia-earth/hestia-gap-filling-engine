import re
from hestia_earth.schema import PropertyJSONLD, PracticeJSONLD
from hestia_earth.utils.api import download_hestia
from hestia_earth.utils.tools import non_empty_list
from hestia_earth.utils.model import find_term_match, linked_node


from hestia_earth.gap_filling.version import VERSION


def _new_major_version(version: str): return version.split('.')[0] != VERSION.split('.')[0]


def should_gap_fill(values: list, term_id: str, key='value'):
    """
    Determines whether the Term should trigger a gap-fill or not. We are checking for:
    1. If the Term is already present in the list
    2. If it is present, how reliable is the value?
    3. If not reliable enough, check if we gap-filled before or not and if we did, is it an old version?
    4. If all checks above pass, then gap-filling should occur.
    Note: this is a general pre-check only, each model can have 1 or more other checks.

    Parameters
    ----------
    values : list
        List of Blank Nodes containing Terms.
    term_id : str
        ID of the Term to check for.
    key : str
        The gap-filling key to check for. If we are gap-filling the value, this will equal to `value`.

    Returns
    -------
    bool
        If we should run gap-filling on this model or not.
    """
    term = find_term_match(values, term_id, None)
    return term is None or term.get('reliability', 1) >= 3 or (
        key in term.get('gapFilled', [])
        and _new_major_version(term.get('gapFilledVersion', [])[term.get('gapFilled', []).index(key)])
    )


def _gap_filled_version(node: dict, keys: str = None):
    node['gapFilled'] = node.get('gapFilled', [])
    node['gapFilledVersion'] = node.get('gapFilledVersion', [])
    all_keys = ['value'] if keys is None else keys.split(',')
    for key in list(filter(lambda key: node.get(key) is not None, all_keys)):
        if key in node['gapFilled']:
            node.get('gapFilledVersion')[node['gapFilled'].index(key)] = VERSION
        else:
            node['gapFilled'].append(key)
            node['gapFilledVersion'].append(VERSION)
    return node


def _match_el(source: dict, dest: dict):
    id1 = source.get('term', {}).get('@id')
    id2 = dest.get('term', {}).get('@id')
    # TODO: do different matchings from `@type`?
    start_date = dest.get('startDate')
    end_date = dest.get('endDate')
    return id1 == id2 \
        and (not start_date or start_date == source.get('startDate')) \
        and (not end_date or end_date == source.get('endDate'))


def _find_match_el_index(values: list, el: str, default_val=None):
    return next((i for i in range(len(values)) if _match_el(values[i], el)), default_val)


def _merge_lists_term(source: list, dest: list):
    for el in non_empty_list(dest):
        source_index = _find_match_el_index(source, el)
        if source_index is None:
            source.append(el)
        else:
            source[source_index] = el
    return source


def _term_id(term): return term.get('@id') if isinstance(term, dict) else term


def _new_property(term):
    node = PropertyJSONLD().to_dict()
    node['term'] = linked_node(term if isinstance(term, dict) else download_hestia(_term_id(term)))
    return _gap_filled_version(node, 'term')


def _get_or_create_property(properties: list, term):
    node = find_term_match(properties, _term_id(term), None)
    return _new_property(term) if node is None else node


def _new_practice(term):
    node = PracticeJSONLD().to_dict()
    node['term'] = linked_node(term if isinstance(term, dict) else download_hestia(_term_id(term)))
    return _gap_filled_version(node, 'term')


def _get_or_create_practice(practices: list, term):
    node = find_term_match(practices, _term_id(term), None)
    return _new_practice(term) if node is None else node


def _find_term_property(term_id: str, property: str):
    term = download_hestia(term_id)
    return find_term_match(term.get('defaultProperties', []), property, None)


def _get_property_value(value: dict, property: str):
    prop = find_term_match(value.get('properties', []), property, None)
    return _find_term_property(value.get('term', {}).get('@id'), property) if prop is None else prop


def _is_in_days(date: str):
    return date is not None and re.compile(r'^[\d]{4}\-[\d]{2}\-[\d]{2}').match(date) is not None
