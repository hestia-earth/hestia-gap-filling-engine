# this is not a gap-filling model
# we need to have the complete cycle object to gap-fill some models
from hestia_earth.schema import SchemaType
from hestia_earth.utils.api import download_hestia

from hestia_earth.gap_filling.log import logger

GAP_FILLING_KEY = 'cycle'


def _gap_fill_impact(impact: dict):
    try:
        return download_hestia(impact.get('cycle', {}).get('@id'), SchemaType.CYCLE, 'gapFilled')
    except Exception:
        return download_hestia(impact.get('cycle', {}).get('@id'), SchemaType.CYCLE)


def _need_gap_fill(impact: dict):
    cycle_id = impact.get('cycle', {}).get('@id')
    gap_fill = cycle_id is not None
    logger.info('model=%s, gap_fill=%s', GAP_FILLING_KEY, gap_fill)
    return gap_fill


def gap_fill(impact: dict): return [(GAP_FILLING_KEY, _gap_fill_impact(impact) if _need_gap_fill(impact) else [])]
