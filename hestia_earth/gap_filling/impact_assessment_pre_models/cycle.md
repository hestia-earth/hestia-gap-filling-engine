# Cycle

Some ImpactAssessment models need a full version of the linked [Cycle](https://hestia.earth/schema/Cycle) to run.
This model will fetch the complete version of the `gapFilled` [Cycle](https://hestia.earth/schema/Cycle) and include it.

## Gap-fills

- [impactAssessment.cycle](https://hestia.earth/schema/ImpactAssessment#cycle)

## Requirements

- [cycle.@id](https://hestia.earth/schema/Cycle#id) must be set (is linked to an existing Cycle)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/impact_assessment_pre_models/test_cycle_.py)
