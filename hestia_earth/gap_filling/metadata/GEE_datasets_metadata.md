# Summary: Spatial datasets used in HESTIA

## VECTOR DATA
*Title*: WWF Terrestrial Ecoregions of the World
*File on GEE*: Terrestrial_Ecoregions_World
*Description*: A shapefile containing polygons for each of the 867 terrestrial ecoregions as defined by WWF. Ecoregions represent the original distribution of distinct assemblages of species and communities
*External link*: https://www.worldwildlife.org/publications/terrestrial-ecoregions-of-the-world

*Title*: GADM database of Global Administrative Areas 3.6
*File on GEE*: gadm36
*Entry in the Glossary*: https://www.hestia.earth/glossary?termType=region
*Description*: A shapefile containing polygons for 386,735 administrative areas. These data cover all countries and up to five sub-divisions within countries
*External link*: https://gadm.org/

*Title*: Eco Climate Zone
*File on GEE*:
*Entry in the Glossary*: https://www.hestia.earth/term/eco-ClimateZone
*Description*: A shapefile grouping areas based on their ecology and climate, defined by the JRC
*External link*: https://ec.europa.eu/jrc/en/publication/eur-scientific-and-technical-research-reports/biofuels-new-methodology-estimate-ghg-emissions-due-global-land-use-change-methodology

## RASTER DATA
*Title*: pH (H2O)
*File on GEE*:
*Entry in the Glossary*: https://www.hestia.earth/term/soilPh
*Description*: Topsoil pH. Extracted from the Harmonized World Soil Database (HWSD)
*External link*: https://esdac.jrc.ec.europa.eu/ESDB_Archive/Soil_Data/Docs_GlobalData/Harmonized_World_Soi_Database_v1.2.pdf

*Title*: Clay Content (% weight, 0-30cm)
*File on GEE*: T_CLAY
*Entry in the Glossary*: https://www.hestia.earth/term/clayContent
*Description*: Topsoil clay fraction. Extracted from the Harmonized World Soil Database (HWSD)
*External link*: https://esdac.jrc.ec.europa.eu/ESDB_Archive/Soil_Data/Docs_GlobalData/Harmonized_World_Soi_Database_v1.2.pdf

*Title*: Sand Content (% weight, 0-30cm)
*File on GEE*:
*Entry in the Glossary*: https://www.hestia.earth/term/sandContent
*Description*: Topsoil sand fraction. Extracted from the Harmonized World Soil Database (HWSD)
*External link*: https://esdac.jrc.ec.europa.eu/ESDB_Archive/Soil_Data/Docs_GlobalData/Harmonized_World_Soi_Database_v1.2.pdf

*Title*: Organic Carbon Content (% weight, 0-30cm)
*File on GEE*: https://www.hestia.earth/term/soilOrganicCarbonContent
*Entry in the Glossary*:
*Description*: Topsoil organic carbon content. Extracted from the Harmonized World Soil Database (HWSD)
*External link*: https://esdac.jrc.ec.europa.eu/ESDB_Archive/Soil_Data/Docs_GlobalData/Harmonized_World_Soi_Database_v1.2.pdf

*Title*: Phosphorus Content (kg P t–1, 0-50cm)
*File on GEE*: soil_phosphorus_concentration
*Entry in the Glossary*: https://www.hestia.earth/term/soilPhosphorusContent
*Description*: Soil Phosphorus concentration derived from the Phosphorus area density to a depth of 50cm
*External link*: https://link.springer.com/article/10.1007%2Fs11367-015-0880-0

*Title*: Total Nitrogen Content (kg N t–1, 0-50cm)
*File on GEE*:
*Entry in the Glossary*: https://www.hestia.earth/term/soilNitrogenContent
*Description*: Soil Total Nigrogen concentration to a depth of 50cm
*External link*: https://www.researchgate.net/publication/282185041_World_soil_property_estimates_for_broad-_scale_modelling_WISE30sec

*Title*: Erodibility (t h MJ–1 mm–1)
*File on GEE*: erodibility
*Entry in the Glossary*: https://www.hestia.earth/term/erodibility
*Description*: The erodibility is a quantitative estimate of the vulnerability of the soil to erosion
*External link*: https://link.springer.com/article/10.1007%2Fs11367-015-0880-0

*Title*: Drainage
*File on GEE*: drainage-xy1
*Entry in the Glossary*: https://www.hestia.earth/term/drainageClass
*Description*: A six level factor describing drainage class based on soil type, texture, soil phase, and terrain slope
*External link*: https://esdac.jrc.ec.europa.eu/ESDB_Archive/Soil_Data/Docs_GlobalData/Harmonized_World_Soi_Database_v1.2.pdf

*Title*: Precipitation (mm year–1)
*File on GEE*: annual_precipitation
*Entry in the Glossary*: https://www.hestia.earth/term/annualRainfallLong-TermMean
*Entry in the Glossary (alternative)*: https://www.hestia.earth/term/rainfallAnnual
*Description*: Mean annual precipitation. Extracted from WorldClim
*External link*: https://www.worldclim.org/

*Title*: Winter-type Precipitation Correction
*File on GEE*: correction_winter-type_precipitation
*Entry in the Glossary*: https://www.hestia.earth/term/heavyWinterPrecipitaton
*Description*: At least one winter month where precipitation exceeds 15 % of the annual average
*External link*: https://www.worldclim.org/

*Title*: Slope (%)
*File on GEE*: GMTEDSLOPE
*Entry in the Glossary*: https://www.hestia.earth/term/slope
*Description*: Terrain slope measured in percent
*External link*: https://www.researchgate.net/profile/Dean_Gesch/publication/254213144_AN_ENHANCED_GLOBAL_ELEVATION_MODEL_GENERALIZED_FROM_MULTIPLE_HIGHER_RESOLUTION_SOURCE_DATASETS/links/53d03bf20cf25dc05cfe392f/AN-ENHANCED-GLOBAL-ELEVATION-MODEL-GENERALIZED-FROM-MULTIPLE-HIGHER-RESOLUTION-SOURCE-DATASETS.pdf

*Title*: Slope length (dimensionless)
*File on GEE*: slope_length_factor
*Entry in the Glossary*: https://www.hestia.earth/term/slopeLength
*Description*: Slope length
*External link*: https://www.researchgate.net/profile/Dean_Gesch/publication/254213144_AN_ENHANCED_GLOBAL_ELEVATION_MODEL_GENERALIZED_FROM_MULTIPLE_HIGHER_RESOLUTION_SOURCE_DATASETS/links/53d03bf20cf25dc05cfe392f/AN-ENHANCED-GLOBAL-ELEVATION-MODEL-GENERALIZED-FROM-MULTIPLE-HIGHER-RESOLUTION-SOURCE-DATASETS.pdf

*Title*: Phosphorus Reaching Aquatic Environment
*File on GEE*: TODO
*Entry in the Glossary*: TODO
*Description*: The amount of phosphorus reaching the aquatic environment.
*External link*: https://link.springer.com/article/10.1007/s11367-015-0880-0
