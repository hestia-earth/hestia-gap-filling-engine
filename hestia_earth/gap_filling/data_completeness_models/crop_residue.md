# Crop Residue

This model checks if we successfully gap filled all the [crop residue terms](https://hestia.earth/glossary?termType=cropResidue) and updates the [Data Completeness](https://hestia.earth/schema/Completeness#cropResidue) value.

## Gap-fills

- [dataCompleteness.cropResidue](https://hestia.earth/schema/Completeness#cropResidue)

## Requirements

- [product.term](https://hestia.earth/schema/Product#term) with all of: [belowGroundCropResidue](https://hestia.earth/term/belowGroundCropResidue), [aboveGroundCropResidueTotal](https://hestia.earth/term/aboveGroundCropResidueTotal) and at least one of: [aboveGroundCropResidueRemoved](https://hestia.earth/term/aboveGroundCropResidueRemoved), [aboveGroundCropResidueIncorporated](https://hestia.earth/term/aboveGroundCropResidueIncorporated), [aboveGroundCropResidueBurnt](https://hestia.earth/term/aboveGroundCropResidueBurnt) or [aboveGroundCropResidueLeftOnField](https://hestia.earth/term/aboveGroundCropResidueLeftOnField)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/data_completeness_models/test_crop_residue.py)
