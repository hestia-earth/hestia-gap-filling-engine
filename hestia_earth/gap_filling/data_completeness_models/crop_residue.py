from hestia_earth.schema import SchemaType, TermTermType
from hestia_earth.utils.api import find_node
from hestia_earth.utils.model import find_term_match

from hestia_earth.gap_filling.log import logger
from hestia_earth.gap_filling.cycle_models import bg_crop_residue
from hestia_earth.gap_filling.cycle_pre_models import ag_crop_residue_total

GAP_FILLING_KEY = 'cropResidue'
REQUIRED_TERM_IDS = [
    bg_crop_residue.TERM_ID,
    ag_crop_residue_total.TERM_ID
]


def _optional_term_ids():
    terms = find_node(SchemaType.TERM, {'termType': TermTermType.CROPRESIDUE.value})
    return [term.get('@id') for term in terms if term.get('@id') not in REQUIRED_TERM_IDS]


def gap_fill(cycle: dict):
    products = cycle.get('products', [])
    # all required terms + at least one of the optional terms must be present to be gap-filled
    required_valid = all([find_term_match(products, term_id, False) for term_id in REQUIRED_TERM_IDS])
    optional_valid = any([find_term_match(products, term_id, False) for term_id in _optional_term_ids()])
    is_complete = required_valid and optional_valid
    logger.info('key=%s, value=%s', GAP_FILLING_KEY, is_complete)
    return is_complete
