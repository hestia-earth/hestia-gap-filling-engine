from functools import reduce
from hestia_earth.utils.model import find_primary_product, find_term_match
from hestia_earth.utils.tools import safe_parse_float

from hestia_earth.gap_filling.log import logger
from hestia_earth.gap_filling.utils import _gap_filled_version, should_gap_fill, _get_or_create_practice
from hestia_earth.gap_filling.ag_crop_residue_models import ag_crop_residue_burnt
from hestia_earth.gap_filling.ag_crop_residue_models import ag_crop_residue_incorporated
from hestia_earth.gap_filling.ag_crop_residue_models import ag_crop_residue_leftonfield
from hestia_earth.gap_filling.ag_crop_residue_models import ag_crop_residue_removed

GAP_FILLING_KEY = 'practices'
TERM_ID = 'residueRemoved,residueIncorporated,residueBurnt,residueLeftOnField'
MODELS = [
    ag_crop_residue_removed,
    ag_crop_residue_incorporated,
    ag_crop_residue_burnt
]
REMAINING_TERM_ID = ag_crop_residue_leftonfield.PRACTICE_TERM_ID


def _gap_fill_practice(practices: list, term_id: str, value: float):
    practice = _get_or_create_practice(practices, term_id)
    logger.info('key=%s, term=%s, value=%s', GAP_FILLING_KEY, term_id, value)
    practice['value'] = value
    return _gap_filled_version(practice)


def _need_gap_fill_model(model, cycle: dict):
    gap_fill = should_gap_fill(cycle.get(GAP_FILLING_KEY, []), model.PRACTICE_TERM_ID)
    logger.info('key=%s, term=%s, gap_fill=%s', GAP_FILLING_KEY, model.PRACTICE_TERM_ID, gap_fill)
    return gap_fill


def _gap_fill_model(model, cycle: dict, primary_product: dict, remaining_value: float):
    need_gap_fill = _need_gap_fill_model(model, cycle)
    value = model.gap_fill_practice_value(cycle, primary_product) if need_gap_fill else None
    return None if value is None else value * remaining_value / 100


def _model_value(model, products: list):
    value = find_term_match(products, model.PRACTICE_TERM_ID).get('value', 0)
    return safe_parse_float(value, 0)


def _gap_fill_cycle(cycle: dict):
    practices = cycle.get(GAP_FILLING_KEY, [])
    primary_product = find_primary_product(cycle)
    # first, calculate the remaining value available after applying all user-uploaded data
    remaining_value = reduce(
        lambda prev, model: prev - _model_value(model, practices),
        MODELS + [ag_crop_residue_leftonfield],
        100
    )

    values = []
    # then gap-fill every model in order up to the remaining value
    for model in MODELS:
        value = _gap_fill_model(model, cycle, primary_product, remaining_value)
        logger.debug('key=%s, term=%s, value=%s', GAP_FILLING_KEY, model.PRACTICE_TERM_ID, value)
        if remaining_value > 0 and value is not None and value > 0:
            value = value if value < remaining_value else remaining_value
            values.extend([
                _gap_fill_practice(practices, model.PRACTICE_TERM_ID, value)
            ])
            remaining_value = remaining_value - value
            if remaining_value == 0:
                logger.debug('no more residue, stopping')
                break

    return values + [
        # whatever remains is "left on field"
        _gap_fill_practice(practices, REMAINING_TERM_ID, remaining_value)
    ] if remaining_value > 0 else values


def gap_fill(cycle: dict): return [(GAP_FILLING_KEY, _gap_fill_cycle(cycle))]
