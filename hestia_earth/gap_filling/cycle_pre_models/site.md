# Site

Some Cycle models need a full version of the linked [Site](https://hestia.earth/schema/Site) to run.
This model will fetch the complete version of the `gapFilled` [Site](https://hestia.earth/schema/Site) and include it.

## Gap-fills

- [cycle.site](https://hestia.earth/schema/Cycle#site)

## Requirements

- [site.@id](https://hestia.earth/schema/Site#id) must be set (is linked to an existing Site)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/cycle_pre_models/test_site.py)
