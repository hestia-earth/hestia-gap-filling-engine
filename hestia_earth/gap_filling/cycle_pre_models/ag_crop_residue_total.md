# Above Ground Crop Residue - Total

This model estimates the total amount of above ground crop residue using the [IPCC (2019)](https://www.ipcc-nggip.iges.or.jp/public/2019rf/pdf/4_Volume4/19R_V4_Ch11_Soils_N2O_CO2.pdf) linear regression based methodology, based on crop type and crop yield.
For the following broad crop groups (Bananas, Berries, Nuts, Olives, Oil palm, Apples, Citrus, Orchard Fruit, Grapes, Coffee, Cocoa), above ground crop residue is gap-filled using standardised values for each crop grouping using data from Poore & Nemecek (2018), which is based on a range of sources.

## Gap-fills

- [product.term](https://hestia.earth/schema/Product#term) with [aboveGroundCropResidueTotal](https://hestia.earth/term/aboveGroundCropResidueTotal)
- [product.value](https://hestia.earth/schema/Product#value)

## Requirements

- [product.term](https://hestia.earth/schema/Product#term) with [aboveGroundCropResidueRemoved](https://hestia.earth/term/aboveGroundCropResidueRemoved) or [Product](https://hestia.earth/schema/Product) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop) which is a [primary](https://hestia.earth/schema/Product#primary) product.

## Implementation Status

- `Implemented`
- [Tested](../../../tests/cycle_pre_models/test_ag_crop_residue_total.py)
