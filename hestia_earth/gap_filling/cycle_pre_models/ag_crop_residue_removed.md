# Above Ground Crop Residue - Removed

TODO

## Gap-fills

- [product.term](https://hestia.earth/schema/Product#term) with [aboveGroundCropResidueRemoved](https://hestia.earth/term/aboveGroundCropResidueRemoved)
- [product.value](https://hestia.earth/schema/Product#value)

## Requirements

- One or more [Product](https://hestia.earth/schema/Product) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop) which is equivalent to a crop residue removed;

## Implementation Status

- `Implemented`
- [Tested](../../../tests/cycle_pre_models/test_ag_crop_residue_removed.py)
