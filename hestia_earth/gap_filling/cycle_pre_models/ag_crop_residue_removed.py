from hestia_earth.utils.lookup import get_table_value, download_lookup, column_name
from hestia_earth.utils.tools import non_empty_list, safe_parse_float

from hestia_earth.gap_filling.log import logger
from hestia_earth.gap_filling.utils import should_gap_fill, _gap_filled_version, _get_property_value
from hestia_earth.gap_filling.cycle_utils import _should_gap_fill_term_type, _get_or_create_product

GAP_FILLING_KEY = 'products'
TERM_ID = 'aboveGroundCropResidueRemoved'
PROPERTY_KEY = 'dryMatter'


def _gap_fill_product(products: list, value: float):
    product = _get_or_create_product(products, TERM_ID)
    logger.info('key=%s, term=%s, value=%s', GAP_FILLING_KEY, TERM_ID, value)
    product['value'] = [value]
    return _gap_filled_version(product)


def _get_value(product: dict, product_dm_property: dict):
    value = product.get('value', [0])[0]
    dm_percent = safe_parse_float(product_dm_property.get('value'))
    logger.debug('term=%s, value: %s, dm_percent=%s', product.get('term', {}).get('@id'), value, dm_percent)
    return value * dm_percent / 100


def _gap_fill_cycle(cycle: dict, products: list):
    value = sum([_get_value(product, dm_prop) for product, dm_prop in products])
    return [_gap_fill_product(cycle.get(GAP_FILLING_KEY, []), value)] if value is not None else []


def _need_gap_fill_product(product: dict):
    lookup = download_lookup('crop.csv', True)
    term_id = product.get('term', {}).get('@id')
    product_match = get_table_value(lookup, 'termid', term_id, column_name('isAboveGroundCropResidueRemoved'))
    logger.debug('term=%s, match: %s', term_id, product_match)
    return [product, _get_property_value(product, PROPERTY_KEY)] if product_match else []


def _need_gap_fill(cycle: dict):
    products = non_empty_list(map(_need_gap_fill_product, cycle.get('products', [])))
    gap_fill = len(products) > 0 \
        and should_gap_fill(cycle.get(GAP_FILLING_KEY, []), TERM_ID) \
        and _should_gap_fill_term_type(cycle, TERM_ID)
    logger.info('key=%s, term=%s, gap_fill=%s', GAP_FILLING_KEY, TERM_ID, gap_fill)
    return gap_fill, products


def gap_fill(cycle: dict):
    need_gap_fill, products = _need_gap_fill(cycle)
    return [(GAP_FILLING_KEY, _gap_fill_cycle(cycle, products) if need_gap_fill else [])]
