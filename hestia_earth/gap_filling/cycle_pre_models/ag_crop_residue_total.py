from hestia_earth.utils.model import find_primary_product, find_term_match
from hestia_earth.utils.lookup import get_table_value, download_lookup
from hestia_earth.utils.tools import safe_parse_float

from hestia_earth.gap_filling.log import logger
from hestia_earth.gap_filling.utils import should_gap_fill, _gap_filled_version, _get_property_value
from hestia_earth.gap_filling.cycle_utils import _should_gap_fill_term_type, _get_or_create_product
from hestia_earth.gap_filling.ag_crop_residue_models.ag_crop_residue_removed import PRACTICE_TERM_ID
from .ag_crop_residue_removed import TERM_ID as REMOVED_TERM_ID

GAP_FILLING_KEY = 'products'
TERM_ID = 'aboveGroundCropResidueTotal'
PROPERTY_KEY = 'dryMatter'


def _get_removed_practice_value(cycle: dict) -> float:
    value = find_term_match(cycle.get('practices', []), PRACTICE_TERM_ID).get('value')
    return safe_parse_float(value) / 100 if value is not None else None


def _get_value_dm(primary_product: dict, dm_percent: float):
    crop_res_dry_matter = download_lookup('crop.csv', True)

    term_id = primary_product.get('term', {}).get('@id', '')
    product_yield = primary_product.get('value', [0])[0]

    logger.debug('lookup data for Term: %s', term_id)
    if term_id in list(crop_res_dry_matter.termid):
        # Multiply yield by dryMatter proportion
        yield_dm = product_yield * (dm_percent / 100)

        # estimate the AG DM calculation
        ag_slope = safe_parse_float(
            get_table_value(crop_res_dry_matter, 'termid', term_id, 'crop_residue_slope'), None
        )
        ag_intercept = safe_parse_float(
            get_table_value(crop_res_dry_matter, 'termid', term_id, 'crop_residue_intercept'), None
        )
        logger.debug('term=%s, yield=%s, dry_matter_percent=%s, slope=%s, intercept=%s',
                     term_id, product_yield, dm_percent, ag_slope, ag_intercept)

        # estimate abv. gro. residue as dry_yield * slope + intercept * 1000.  IPCC 2006 (Poore & Nemecek 2018)
        return None if ag_slope is None or ag_intercept is None else (yield_dm * ag_slope + ag_intercept * 1000)

    return None


def _get_value_default(primary_product: dict):
    crop_res = download_lookup('crop.csv', True)

    term_id = primary_product.get('term', {}).get('@id', '')

    logger.debug('lookup data for Term: %s', term_id)
    if term_id in list(crop_res.termid):
        # take the default ag crop residue for the crop in question
        value = safe_parse_float(
            get_table_value(crop_res, 'termid', term_id, 'default_ag_dm_crop_residue'), None
        )
        return value if value is not None else []

    return None


def _gap_fill_product(products: list, value: float):
    product = _get_or_create_product(products, TERM_ID)
    logger.info('key=%s, term=%s, value=%s', GAP_FILLING_KEY, TERM_ID, value)
    product['value'] = [value]
    return _gap_filled_version(product)


def _gap_fill_cycle(cycle: dict, primary_product: dict):
    practice_value = _get_removed_practice_value(cycle)
    dm_property = _get_property_value(primary_product, PROPERTY_KEY) if primary_product is not None else None
    removed_value = _get_removed_value(cycle)

    #  1) gap-fill using removed amount and practice value, or 2) use dm regression, or 3) use default for orchard crops
    value = removed_value / practice_value if removed_value is not None and practice_value is not None else \
        _get_value_dm(primary_product, safe_parse_float(dm_property.get('value'))) if dm_property is not None else \
        _get_value_default(primary_product)

    return [_gap_fill_product(cycle.get(GAP_FILLING_KEY, []), value)] if value is not None else []


def _get_removed_value(cycle: dict):
    # if we find the removed value, we can infer the total
    value = find_term_match(cycle.get(GAP_FILLING_KEY, []), REMOVED_TERM_ID, {'value': []}).get('value')
    return value[0] if len(value) > 0 else None


def _need_gap_fill(cycle: dict):
    product = find_primary_product(cycle)
    gap_fill = should_gap_fill(cycle.get(GAP_FILLING_KEY, []), TERM_ID) \
        and _should_gap_fill_term_type(cycle, TERM_ID) and product is not None
    logger.info('key=%s, term=%s, gap_fill=%s', GAP_FILLING_KEY, TERM_ID, gap_fill)
    return gap_fill, product


def gap_fill(cycle: dict):
    need_gap_fill, product = _need_gap_fill(cycle)
    return [(GAP_FILLING_KEY, _gap_fill_cycle(cycle, product) if need_gap_fill else [])]
