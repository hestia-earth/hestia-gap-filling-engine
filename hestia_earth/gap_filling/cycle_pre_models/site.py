# this is not a gap-filling model
# we need to have the complete site object to gap-fill some models
from hestia_earth.schema import SchemaType
from hestia_earth.utils.api import download_hestia

from hestia_earth.gap_filling.log import logger

GAP_FILLING_KEY = 'site'


def _gap_fill_cycle(cycle: dict):
    try:
        return download_hestia(cycle.get('site', {}).get('@id'), SchemaType.SITE, 'gapFilled')
    except Exception:
        return download_hestia(cycle.get('site', {}).get('@id'), SchemaType.SITE)


def _need_gap_fill(cycle: dict):
    site_id = cycle.get('site', {}).get('@id')
    gap_fill = site_id is not None
    logger.info('model=%s, gap_fill=%s', GAP_FILLING_KEY, gap_fill)
    return gap_fill


def gap_fill(cycle: dict): return [(GAP_FILLING_KEY, _gap_fill_cycle(cycle) if _need_gap_fill(cycle) else [])]
