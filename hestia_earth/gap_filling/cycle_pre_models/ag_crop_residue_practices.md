# Above Ground Crop Residue

This model gap-fills the [Practices](https://www.hestia.earth/glossary?termType=cropResidueManagement) which describe how above ground crop residue is managed, by assigning default percentages to each Practice using country and crop averages.
If any data are provided by the user, they are used first.

For more details about each of the models, please see the models documentation [here](../ag_crop_residue_models).

## Gap-fills

- [practice.term](https://hestia.earth/schema/Practice#term) with any of: [residueRemoved](https://hestia.earth/term/residueRemoved), [residueIncorporated](https://hestia.earth/term/residueIncorporated), [residueBurnt](https://hestia.earth/term/residueBurnt) or [residueLeftOnField](https://hestia.earth/term/residueLeftOnField)
- [practice.value](https://hestia.earth/schema/Practice#value)

## Requirements

`N/A`

## Implementation Status

- `Implemented`
- [Tested](../../../tests/cycle_pre_models/test_ag_crop_residue_practices.py)
