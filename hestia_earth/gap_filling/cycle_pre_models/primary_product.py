from functools import reduce
from hestia_earth.utils.lookup import get_table_value, column_name, download_lookup
from hestia_earth.utils.tools import safe_parse_float, non_empty_list

from hestia_earth.gap_filling.log import logger
from hestia_earth.gap_filling.utils import _gap_filled_version

GAP_FILLING_KEY = 'products'
MODEL_KEYS = 'primary,price,revenue,economicValueShare'


def _need_gap_fill_price(product: dict):
    gap_fill = 'price' not in product.keys() and len(product.get('value', [])) > 0
    logger.info('model=price, gap_fill=%s, term=%s', gap_fill, product.get('term', {}).get('@id'))
    return gap_fill


def _gap_fill_product_price(product: dict, value: float):
    # divide by 1000 to convert price per tonne to kg
    value = value/1000
    logger.info('model=price, value=%s, term=%s', value, product.get('term', {}).get('@id'))
    product['price'] = value
    return _gap_filled_version(product, 'price')


def _extract_grouped_data(data: str, key: str):
    # example data: Average_price_per_tonne:106950.5556;1991:-;1992:-
    return reduce(lambda prev, curr: {
        **prev,
        **{curr.split(':')[0]: curr.split(':')[1]}
    }, data.split(';'), {})[key] if data is not None and len(data) > 1 else None


def _gap_fill_price(cycle: dict, products: list):
    crop_lookup = download_lookup('crop.csv', True)

    # TODO: add description saying this is gap-filled based on annual value averaged between 1991-2018, source: FAOSTAT
    lookup = download_lookup('region-crop-cropGroupingFAOSTAT-price.csv')

    products_to_gap_fill = list(filter(_need_gap_fill_price, products))
    # gap-fill price only using country level average (FAO data)
    country_id = cycle.get('site').get('country').get('@id')

    def gap_fill_product(product: dict):
        term_id = product.get('term', {}).get('@id', '')
        grouping = get_table_value(crop_lookup, 'termid', term_id, column_name('cropGroupingFAOSTAT'))
        price_data = get_table_value(lookup, 'termid', country_id, column_name(grouping)) if grouping else None
        avg_price = _extract_grouped_data(price_data, 'Average_price_per_tonne')
        value = safe_parse_float(avg_price, None)
        return None if value is None else _gap_fill_product_price(product, value)

    return non_empty_list(list(map(gap_fill_product, products_to_gap_fill)))


def _need_gap_fill_revenue(product: dict):
    gap_fill = 'revenue' not in product.keys() and len(product.get('value', [])) > 0 and product.get('price', 0) > 0
    logger.info('model=revenue, gap_fill=%s, term=%s', gap_fill, product.get('term', {}).get('@id'))
    return gap_fill


def _gap_fill_product_revenue(product: dict):
    value = product.get('value', [1])[0] * product.get('price', 0)
    logger.info('model=revenue, value=%s, term=%s', value, product.get('term', {}).get('@id'))
    product['revenue'] = value
    return _gap_filled_version(product, 'revenue')


def _gap_fill_revenue(products: list):
    return list(map(_gap_fill_product_revenue, list(filter(_need_gap_fill_revenue, products))))


def _need_gap_fill_economicValueShare(product: dict):
    gap_fill = 'economicValueShare' not in product.keys()
    logger.info('model=economicValueShare, gap_fill=%s, term=%s', gap_fill, product.get('term', {}).get('@id'))
    return gap_fill


def _gap_fill_product_economicValueShare(product: dict, value: float):
    logger.info('model=economicValueShare, value=%s, term=%s', value, product.get('term', {}).get('@id'))
    product['economicValueShare'] = value
    return _gap_filled_version(product, 'economicValueShare')


def _gap_fill_economicValueShare(products: list):
    # do not gap-fill if we already have a total economic value over 100%
    total_value = sum([p.get('economicValueShare', 0) for p in products])
    if total_value >= 100:
        logger.debug('skip gap-fill economicValueShare over 100')
        return []

    lookup = download_lookup('crop.csv', True)

    # If revenue available for all products (or above 50% for any product), econValueShare = revenue/sum
    # (revenue all products in the cycle)*100
    # TODO: add this calculation

    # If no revenue provided = use country level averages for the given product (for example, wheat 80%; straw 20%)
    def gap_fill_product(product: dict):
        term = product.get('term', {}).get('@id', '')
        # TODO: need to ensure that when combining uploaded data on EVS with gap-filled EVS, the sum not > 100.
        value = safe_parse_float(get_table_value(lookup, 'termid', term, 'global_economic_value_share'), None)
        return None if value is None else _gap_fill_product_economicValueShare(product, value)

    return non_empty_list(list(map(gap_fill_product, list(filter(_need_gap_fill_economicValueShare, products)))))


def _need_gap_fill_primary(products: list):
    primary = next((p for p in products if p.get('primary', False) is True), None)
    gap_fill = len(products) > 0 and primary is None
    logger.info('model=primary, gap_fill=%s', gap_fill)
    return gap_fill


def _find_primary_product(products: list):
    # If only one product, primary = True
    if len(products) == 1:
        return products[0]

    # else primary product = the product with the largest economic value share
    else:
        max_products = sorted(
            list(filter(lambda p: 'economicValueShare' in p.keys(), products)),  # take only products with value
            key=lambda k: k.get('economicValueShare'),  # sort by value
            reverse=True  # take the first as top value
        )
        if len(max_products) > 0:
            return max_products[0]

    return None


def _gap_fill_primary(products: list):
    def gap_fill_product(product: dict):
        logger.info('model=primary, value=%s', product.get('term', {}).get('@id'))
        product['primary'] = True
        return _gap_filled_version(product, 'primary')

    primary = _find_primary_product(products)
    return [gap_fill_product(primary)] if primary else []


def gap_fill(cycle: dict):
    products = cycle.get(GAP_FILLING_KEY, [])

    _gap_fill_price(cycle, products)
    _gap_fill_revenue(products)
    _gap_fill_economicValueShare(products)
    _gap_fill_primary(products) if _need_gap_fill_primary(products) else []

    return [(GAP_FILLING_KEY, products)]
