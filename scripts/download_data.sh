#!/bin/bash

# Script to download all the data necessary data prior to running the engine.

API_URL=$1

DEST="${2:-"./"}hestia_earth/gap_filling/data/properties"
mkdir -p $DEST

echo "Downloading Terms..."

curl -s -X POST $API_URL/search \
  -H "Content-Type: application/json" \
  -d @scripts/find-term-water.json | python -mjson.tool | grep '@id' | sed 's/ //g' | sed 's/"@id"://g' | sed 's/"//g' | sed 's/,//g' \
  > $DEST/irrigations.txt

curl -s -X POST $API_URL/search \
  -H "Content-Type: application/json" \
  -d @scripts/find-term-liquid-fuel.json | python -mjson.tool | grep '@id' | sed 's/ //g' | sed 's/"@id"://g' | sed 's/"//g' | sed 's/,//g' \
  > $DEST/liquid_fuel.txt
