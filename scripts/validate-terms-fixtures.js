require('dotenv').config();
const { readFileSync, readdirSync, lstatSync } = require('fs');
const { join } = require('path');
const axios = require('axios');

const API_URL = process.env.API_URL;

const FIXTURES_DIR = join(__dirname, '../', 'tests', 'fixtures');

const recursiveFixtures = directory =>
  readdirSync(directory).flatMap(entry =>
    lstatSync(join(directory, entry)).isDirectory() ?
      recursiveFixtures(join(directory, entry)) : (
        join(directory, entry).endsWith('.jsonld') ? [join(directory, entry)] : []
      )
  );

const queryTerms = async terms => {
  const query = {
    bool: {
      must: [{
        match: { '@type': 'Term' }
      }],
      should: terms.filter(Boolean).map(term => ({ match: { '@id.keyword': term['@id'] } })),
      minimum_should_match: 1
    }
  };
  const { data: { results } } = await axios.post(`${API_URL}/search`, {
    limit: 1000,
    fields: ['@id'],
    query
  });
  return terms.filter(Boolean).map(term => {
    const exists = results.find(res => res['@id'] === term['@id']);
    return exists ? true : term['@id'];
  }).filter(val => val !== true);
};

const validateNodeType = {
  Cycle: data => [
    data.products ? queryTerms(data.products.map(({ term }) => term)) : null,
    data.emissions ? queryTerms(data.emissions.map(({ term }) => term)) : null,
    data.inputs ? queryTerms(data.inputs.map(({ term }) => term)) : null,
    data.site ? validateData(data.site) : null
  ],
  ImpactAssessment: data => [
    data.product ? validateData(data.product) : null
  ],
  Site: data => [
    data.measurements ? queryTerms(data.measurements.map(({ term }) => term)) : null,
    data.country ? queryTerms([data.country]) : null,
    data.region ? queryTerms([data.region]) : null
  ]
};

const validateData = async data => (await Promise.all(
  Array.isArray(data) ?
    data.flatMap(validateData) :
    data['@type'] in validateNodeType ? validateNodeType[data['@type']](data) : [queryTerms([data.term])]
)).flat().filter(Boolean);

const validateFixture = async filepath => {
  const missing = await validateData(JSON.parse(readFileSync(filepath)));
  return missing.length ? { filepath, missing } : null;
};

const run = async () => {
  const files = (await Promise.all(recursiveFixtures(FIXTURES_DIR).map(validateFixture))).filter(Boolean);
  if (files.length) {
    throw new Error(`Terms not found:
    \t${files.map(({ filepath, missing }) => `- ${filepath}: ${missing.join(', ')}`).join('\n\t')}
    `);
  }
};

run().then(() => process.exit(0)).catch(err => {
  console.error(err);
  process.exit(1);
});
