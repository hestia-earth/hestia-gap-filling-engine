const { readFileSync, writeFileSync, readdirSync, existsSync, lstatSync } = require('fs');
const { resolve, join } = require('path');

const ROOT = resolve(join(__dirname, '../'));
const encoding = 'UTF-8';
const version = require(join(ROOT, 'package.json')).version;

const VERSION_PATH = resolve(join(ROOT, 'hestia_earth', 'gap_filling', 'version.py'));
let content = readFileSync(VERSION_PATH, encoding);
content = content.replace(/VERSION\s=\s\'[\d\-a-z\.]+\'/, `VERSION = '${version}'`);
writeFileSync(VERSION_PATH, content, encoding);

const LAYER_REQUIREMENTS_PATH = resolve(join(ROOT, 'layer', 'requirements.txt'));
content = readFileSync(LAYER_REQUIREMENTS_PATH, encoding);
content = content.replace(/hestia_earth.gap_filling>=[\d\-a-z\.]+/, `hestia_earth.gap_filling>=${version}`);
writeFileSync(LAYER_REQUIREMENTS_PATH, content, encoding);

// Update the fixtures files to make sure we test on the new version
const FIXTURES_DIR = resolve(join(__dirname, '../', 'tests', 'fixtures'));

const recursiveFixtures = directory =>
  readdirSync(directory).flatMap(entry =>
    lstatSync(join(directory, entry)).isDirectory() ?
      recursiveFixtures(join(directory, entry)) :
      [
        join(directory, 'gap-filled.jsonld'),
        join(directory, 'complete.jsonld'),
        join(directory, 'same-version.jsonld')
      ]
  );

[
  ...recursiveFixtures(join(FIXTURES_DIR, 'cycle')),
  ...recursiveFixtures(join(FIXTURES_DIR, 'site'))
]
  .filter(filePath => existsSync(filePath))
  .map(filePath => {
    try {
      const value = JSON.parse(readFileSync(filePath, encoding));
      const nodes = Array.isArray(value) ? value : [
        ...(value.infrastructure || []),
        ...(value.inputs || []),
        ...(value.measurements || []),
        ...(value.products || []),
        ...('site' in value && value.site.measurements || [])
      ];
      [
        ...nodes,
        ...nodes
          .filter(node => 'properties' in node)
          .flatMap(node => node.properties)
      ]
        .filter(node => 'gapFilledVersion' in node && !node.gapFilledVersion.includes('-1.0.0'))
        .forEach(node => {
          node.gapFilledVersion = node.gapFilledVersion.map(() => version);
        });

      writeFileSync(filePath, JSON.stringify(value, null, 2), encoding);
    }
    catch (err) {
      console.error(err);
    }
  });
