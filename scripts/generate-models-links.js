const { readFileSync, writeFileSync, readdirSync, existsSync, lstatSync, read } = require('fs');
const { resolve, join } = require('path');
const { NodeType } = require('@hestia-earth/schema');

const encoding = 'UTF-8';
const ROOT = resolve(join(__dirname, '../'));
const SRC_DIR = join(ROOT, 'hestia_earth', 'gap_filling');

const modelsDir = readdirSync(SRC_DIR).filter(dir => dir.endsWith('_models'));

const parseNodeType = path => {
  const type = path.replace(/_pre_models|_post_models|_models/g, '').replace(/_/g, '').toLowerCase();
  return Object.keys(NodeType).find(nodeType => nodeType.toLowerCase() == type);
};

const findKey = content => {
  const val = content.match(/GAP_FILLING_KEY(\s?)=(\s?)\'([a-zA-Z-_]+)\'/g);
  return val && val.length ? val[0].replace(/GAP_FILLING_KEY|'|=/g, '').trim() : null;
};

const findModelKeys = content => {
  const val = content.match(/MODEL_KEYS(\s?)=(\s?)\'([a-zA-Z-_,]+)\'/g);
  return val && val.length ? val[0].replace(/MODEL_KEYS|'|=/g, '').trim().split(',') : ['value'];
};

const findTerms = content => {
  const val = content.match(/TERM_ID(\s?)=(\s?)\'([a-zA-Z-_,]+)\'/g);
  return val && val.length ? val[0].replace(/TERM_ID|'|=/g, '').trim().split(',').filter(Boolean) : [];
};

const processModel = dir => file => {
  const path = join('hestia_earth', 'gap_filling', dir, file);
  const docPath = path.replace('.py', '.md');
  const abspath = join(SRC_DIR, dir, file);
  const content = readFileSync(abspath, encoding);
  const type = parseNodeType(dir);
  const key = findKey(content);
  const terms = findTerms(content);
  const modelKeys = findModelKeys(content);
  return key ? [
    ...terms.flatMap(term => [
      {
        path,
        docPath,
        type,
        dataPath: `${key}.term`,
        term
      },
      ...modelKeys.map(modelkey => ({
        path,
        docPath,
        type,
        dataPath: `${key}.${modelkey}`,
        term
      }))
    ]),
    ...(terms.length ? [] : modelKeys.map(modelkey => ({
      path,
      docPath,
      type,
      dataPath: `${key}.${modelkey}`
    })))
  ].filter(Boolean) : [];
};

const processModels = (dir => {
  const path = join(SRC_DIR, dir);
  const files = readdirSync(path).filter(v => v.endsWith('.py') && !v.startsWith('__'));
  return files.flatMap(processModel(dir));
});

const links = modelsDir.flatMap(processModels);
writeFileSync(join(ROOT, 'model-links.json'), JSON.stringify({ links }, null, 2), encoding);
