# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.3.1](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/compare/v0.3.0...v0.3.1) (2021-03-25)


### Features

* **cycle machinery usage:** restrict `site.siteType` to gap-fill ([373d25a](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/373d25a45c97b3e87f245755f761466542470094))
* **cycle models:** skip models when data is already complete ([f8e0dc6](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/f8e0dc64532dffd720338a224b1d7fb2d9c31c52))
* **cycle seed:** gap-fill `sd` ([0991fe9](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/0991fe911c597ec017719e71b38b94fb734407cf))


### Bug Fixes

* **cycle crop-residue burnt:** account for removed amount prior to estimating burnt amount ([8b90a19](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/8b90a1940ca94d891b23bd98642eb43248d86e54)), closes [#57](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/57)
* **site region:** handle removed region from Glossary ([10d04ba](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/10d04bac73432f02c7c635526b22e57ff6957ea5))

## [0.3.0](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/compare/v0.2.5...v0.3.0) (2021-03-16)


### ⚠ BREAKING CHANGES

* upgrade to schema `2.10.0` and change gap-filled keys

### Features

* **cycle:** gap-fill `site` before running other models ([7d1894f](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/7d1894f167eff22ebf56b42a8047592dab38bb35))
* **cycle models:** gap-fill `startDate` ([9d13096](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/9d13096fa9736fb5367f795633cd4199832eb97a))
* **site:** add `awareWaterBasinId` model ([0539f84](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/0539f8475b9e840023912f215812df7cb24ea459))
* **site:** set `ecoregion` as string ([6304b3b](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/6304b3b7a84f078b211594b6b2f4f7e307a9e188))
* **site models:** add `eco-ClimateZone` ([f4e4de0](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/f4e4de0231bced7c14fbb325bb6b79eec640076a))
* **site models:** handle gee with `boundary` or `region` or `country` ([4efa5a8](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/4efa5a85c41f8440921863971366625b91b549c7))
* **site region:** try to fetch smallest region first ([3b50518](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/3b505189d8d2015d5cd23906cdf55dc38ccea5c6))
* **utils:** keep limited number of keys for linked node ([f8fb450](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/f8fb450e6fe45ee174d9f3e09ecbbd8081da66fb))


### Bug Fixes

* **cycle primary product:** fetch site if not nested ([7943335](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/7943335aa560fb6df20f84cab6603dfc26888fde))
* **cycle primary product:** fix error multiple gap-filled products ([4fddde2](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/4fddde2cc0d2dc2de46d483cead09eb988181bc6)), closes [#51](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/51)
* **utils:** handle old and new GEE api url ([110f86f](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/110f86fdf36ebac303bbdebd9b64e86765aa7587))


* migrate to `gapFilled` and `gapFilledVersion` ([4b38a7d](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/4b38a7dc6b9c022f5a40d382f8aadbb821bcfb3a))

### [0.2.5](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/compare/v0.2.4...v0.2.5) (2021-02-10)


### Features

* handle schema 1.2.0 ([6a2b36c](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/6a2b36cbe97a1199df2d5e764f22e545752d515e))


### Bug Fixes

* **site models:** update matching biblio title ([412c691](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/412c6916fdc88b10b84aa842e5f695a801afcc4c))

### [0.2.4](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/compare/v0.2.3...v0.2.4) (2021-02-03)


### Features

* **requirements:** update utils to 0.2.1 ([04cf87a](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/04cf87a7b15015214e3920696caa2b2e0b7565ea))

### [0.2.3](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/compare/v0.2.2...v0.2.3) (2021-01-31)


### Features

* **site:** gap-fill value with average min/max ([bb8b386](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/bb8b386e44ef7570872cbf8300564c986da7f0af)), closes [#40](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/40)


### Bug Fixes

* **cycle models:** fix `machineryInfrastructureDepreciatedAmountPerCycle` calculation ([600b6fc](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/600b6fcf19aa7d8c557b1156c8a84c5e1bc4c829)), closes [#5](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/5)
* **site measurement:** fix condition gap_fill ([e870208](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/e8702088d5c0cb95d5e25fac17550e55a66c54d1)), closes [#94](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/94)
* **site rainfallYearOfCycle:** fix unit issue with rainfall data extraction ([a8b3e47](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/a8b3e47f1b65ecf1c825a7f37aa2bab769e29f7e)), closes [#14](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/14)

### [0.2.2](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/compare/v0.2.1...v0.2.2) (2021-01-22)


### Features

* **log:** add function to add a filehandler ([e7d374b](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/e7d374b8d8606a3cea90b1dd673998aada78b6dc))

### [0.2.1](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/compare/v0.2.0...v0.2.1) (2021-01-21)

## [0.2.0](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/compare/v0.1.8...v0.2.0) (2021-01-21)


### ⚠ BREAKING CHANGES

* **requirements:** gap-filled `measurement.value` is now an array of number
* gap-filling `site` is done through another function

### Features

* add gap_fill method to run on both Cycle and Site ([b27abfa](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/b27abfa8135ae050bfbd04f238e476d27e7d2ce1))
* log parsable info on gap-fills ([53b42d7](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/53b42d70cf11ce1904c6e84e696d7adf47a613c7))
* **cycle:** add `aboveGroundCropResidueBurntDryMatter` model ([5336f30](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/5336f30b67adf292b20df0ad98228af11791729f))
* **cycle:** add `aboveGroundCropResidueIncorporatedDryMatter` model ([c7b0b99](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/c7b0b9957cd14d1ea0d67653c4de26f9969f50ae))
* **cycle:** add `aboveGroundCropResidueLeftOnFieldDryMatter` model ([73d59fb](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/73d59fbbe204539c193e33ef56137f33ea62824a))
* **cycle:** add `aboveGroundCropResidueRemovedDryMatter` model ([a9ac01b](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/a9ac01b6c7738d386aeef9bebbc8a4d18da7224a))
* **cycle:** add `aboveGroundCropResidueTotalDryMatter` model ([c252123](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/c2521236389d563178d302a3c56585ded83d9605))
* **cycle:** add primary product model ([855b4ae](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/855b4ae46733ad9421b7174518f8dcf8cae01789))
* **cycle models:** log gap-filling information ([5c9643c](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/5c9643ca1ebca13e238fe37d808ca61a1ad94b2f))
* **cycle primary:** skip gap-fill economic value if over 100 ([db1e728](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/db1e7289b40fc068d34daf99db07854817952739))
* **gap filling:** log time to gap-fill ([9a54f95](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/9a54f9586a06c9d931bb9c21dbeebaf39a9e6a76))
* **gap filling:** only gap-fill with `reliability` >= 3 ([c3162f6](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/c3162f6f90734663a65af07cf1ff57b1e934258c))
* **gap-filling:** gap-fill `ImpactAssessment.product` field ([0314e50](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/0314e5031dd1d1d22313e2614c71444a64be0b9f))
* **impact models:** log gap-filling information ([9783495](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/97834953b12d6a5fb4c9750411fc6a172a32dc28))
* **site models:** log gap-fill information ([a09f75c](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/a09f75c3fc04c90d370c9134de4f0d20301e31a8))


### Bug Fixes

* **cycle:** gap-fill primary product before other gap-fills ([38b627b](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/38b627b19fa3417f7295191deb1bd5102fd99a0d)), closes [#27](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/27)
* **cycle models:** handle lookup table value not found ([87f93ed](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/87f93eda76c0ac61da9b7574ff711e24aa9148a9))
* **log:** log INFO only to file log ([4e08471](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/4e08471f72cdbee50b224b42862e8eb4c75b8f1e))
* **lookup:** fix dryMatter prop for `almond` ([1565f94](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/1565f94d6e65bc0da5067c0484c9bfd3f21175d4))
* **seed model:** handle product without values ([3486abd](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/3486abd57de25b5ed190510cdddca7418174c674))
* **site:** move measurement gap-fills from cycle ([b91ea46](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/b91ea464525f5088103b3f65235391b0cd2bad87))
* **site models:** do not gap-fill cycle `endDate` multiple times ([3351549](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/335154977622af49f69563ce5e0202355d303a85))
* **site models:** handle no measurement found ([de12c59](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/de12c593eb226f9145519a2df02aa45c1e86eaa3))
* **site region:** handle region no `id` ([711dbdd](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/711dbdd46b8312cfccee250fe40e06e21a4e181c))
* **utils:** fix replace measurements ([73b85ac](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/73b85acb34a8977f305acdeed5785d127c9f954a))


* split gap_fills for cycle and site ([7327bfb](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/7327bfb2b22c6138dccedfb6412b45febb0e5286))
* **requirements:** update schema to 0.6.0 ([ec45b95](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/ec45b95d9b31cc11d802b225dca09ffdc78478a2))

### [0.1.8](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/compare/v0.1.7...v0.1.8) (2020-12-09)

### [0.1.7](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/compare/v0.1.6...v0.1.7) (2020-12-09)


### Features

* **models:** export model with name/module dict ([8c24808](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/8c248089e3d1e240968ba80a9e5c25da2226ffba))


### Bug Fixes

* **utils:** gap-fill list if major version changes only ([9d28cde](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/9d28cde06d71a2860dab21a5bd1255e90b31f68e))

### [0.1.6](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/compare/v0.1.5...v0.1.6) (2020-12-09)

### [0.1.5](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/compare/v0.1.4...v0.1.5) (2020-12-09)


### Features

* **gap filling:** Add N composition models ([4ea1abb](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/4ea1abb1b919844ff40e2d4b91eb8e9438c116f3)), closes [#5](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/5) [#6](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/6)
* **gap filling:** execute models in parralel ([4a8e314](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/4a8e3142c2f77d95287c0ffd5bc1707bd978d0e0))
* **gapfills:** add annualRainfallYearOfCycle ([525d1dc](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/525d1dc268a13d485ce4dbf25420a3a21afed8db))
* **gapfills:** add drainageClass ([0685f27](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/0685f270501f3ee27591faf0578fbcba4e7108f3))
* **gapfills:** add erodibility ([392fd14](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/392fd14dd1c515a774ce1b7bba364ce8f9fe9851))
* **gapfills:** add slope ([415a645](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/415a6452a16baadc1a8f83383926551f2901601c))
* **gapfills:** add slopeLength ([aec5efb](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/aec5efbcbf8e49ed4b39d80f2768a1fdc87b1185))
* **look up table:** Add n content look up table ([5580edf](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/5580edfcd117a3d5b7b939d78e56a6318ff270d3)), closes [#5](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/5) [#6](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/6)
* **model:** add dephtUpper and depthLower for soil phosphorus content ([84fb8d5](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/84fb8d5857405fd78cede0b5dd3e7e195cb632f9))
* **models:** add more sources ([3e4b745](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/3e4b745dd171fdf4ab5bd1d9e8d60449832d0b39))
* **models:** add soil nitrogen, soil ph and nutrient loss aquatic ([03ea0a0](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/03ea0a099767f8aa531bc61c6cbe916c28da5169))
* **models:** add startDate and endDate for annual measurements ([f417cc8](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/f417cc8df30714dbe275781577498761251373c6))
* **rainfall:** gap-fill source ([8686890](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/8686890ce7f1e0a02650e2f3ad729f8aaceabb5f))
* **soil texture:** gap fill sandContent ([af6e967](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/af6e967c83d6cc4c6f8801817c01371c879f2c2d))
* **soil texture:** gap-fill clayContent ([f65e2ec](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/f65e2ecae433e7c1ab4db66fbb8b79e946f75744))
* **soil texture:** gap-fill last model after all others are gap-filled ([e836944](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/e8369449c7cde81d3065ad2576a28071dd9f1362))


### Bug Fixes

* handle download hestia errors ([ad7633a](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/ad7633a62f7b1624415b4d6b9cf785509055e72a))
* **gee annual-mean-temp:** handle date availability ([f33c4cc](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/f33c4cc1f4b8e810539faab94caa89f24de95969))
* **models:** handle exiting model only update value and version ([462f13c](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/462f13ceaab9b3312a51fbeffd471fd243420643))
* **models:** update after term id changes ([d346cd2](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/d346cd2e7c8a3b76f34fd5cc22123c1e978026ce))

### [0.1.4](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/compare/v0.1.3...v0.1.4) (2020-11-05)

### [0.1.3](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/compare/v0.1.2...v0.1.3) (2020-11-02)

### [0.1.2](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/compare/v0.1.1...v0.1.2) (2020-11-02)


### Features

* **gapfills:** gap fill `site.region` based on coordinates ([ef38a33](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/ef38a33f1bc8741c0548e10ca4e23e47c306f6d7)), closes [#30](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/30)
* **models:** add new models from GEE ([2c9184c](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/2c9184c44fcf152164a431cf8aaa438fe2b3d497))


### Bug Fixes

* **models:** gap-fill only if necessary ([31dcbdf](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/31dcbdf55cb005fb345f0993e8d18625adbebf79)), closes [#16](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/16)

### [0.1.1](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/compare/v0.1.0...v0.1.1) (2020-10-05)


### Features

* **lookups:** add dry matter look up table ([325b443](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/325b443894e6485c09221428ac5e30ceaa6ec98e)), closes [#6](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/6) [#14](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/14)
* **lookups:** update crop name to match glossary ([733d73b](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/733d73bfd8100bb93792478a9bc20455f8001a50)), closes [#6](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/6) [#14](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/14)
* **root depth model:** add irrigation level ([3da376a](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/3da376af2660bffed637beac0b72c8ca9b955f73)), closes [#6](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/6) [#14](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/14) [#11](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/11)


### Bug Fixes

* **gap filling:** convert value to array to match validation ([3fee475](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/3fee475c2ab2aa7f931067ef04139c23dff26440)), closes [#6](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/6) [#14](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/14)
* **gap filling:** correct below ground crop residue model ([8cccbae](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/8cccbae06d2776a94b99845050b2f74affd19827)), closes [#6](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/6) [#14](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/14)
* **lookups:** update wheat in Seed look up to match glossary ([fdffcdf](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/fdffcdfc7c679e487c12eab8d935427d32d09e0d)), closes [#6](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/6) [#14](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/14)

## [0.1.0](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/compare/v0.0.4...v0.1.0) (2020-09-02)


### Features

* update files to match new schema 0.1.2 ([8c71d48](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/8c71d48424a9453acf6479948e1923849ec59861))


### Bug Fixes

* **models:** fix properties in products ([b2510e8](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/b2510e80160b5a8344e050a96088fa08c28b94b5)), closes [#21](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/21)

### [0.0.4](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/compare/v0.0.3...v0.0.4) (2020-08-27)


### Bug Fixes

* **gap filling:** handle cycle without properties ([fe65075](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/fe65075589fadf7e81264edb9aebbb9446d4d693))
* **models:** fix input values as array ([af00d50](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/af00d50d15ca57a539ce3b2ab79ff7e8f582d371))

### [0.0.3](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/compare/v0.0.2...v0.0.3) (2020-08-21)


### Bug Fixes

* **data:** add missing data files ([44a95b5](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/44a95b5fbf6449a0e2f6a8f7cede085c4188a7d0))

### [0.0.2](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/compare/v0.0.1...v0.0.2) (2020-08-21)


### Bug Fixes

* **gap filling:** fix relative imports ([99973f7](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/99973f74286b7b830fa13c69e3d71fb7b64cdd4a))

### 0.0.1 (2020-08-21)


### Features

* **below ground crop residue:** add below ground crop residue model ([efdb52a](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/efdb52aeaef37f62b6ab99716c174bd432e07d30))
* **bg_crop_residue_dry_matter:** Add hierarchy bg_crop_residue_dry_matter ([94f3e5a](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/94f3e5ab6b9798a7c03d7304fc2348d32bf6429f)), closes [#6](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/6)
* **cropSeed:** Add crop seed model and associated hierarchy ([d391944](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/d3919440901ca90a1c5d0d63d2297e48c15baf26)), closes [#6](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/6)
* **Engine:** add gap filling base class ([ae4c531](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/ae4c531b66142a29e534364c9ba2b024dfebcff9))
* **gap_filling_hierarchy:** add abstract gap_filling_hierarchy class ([b8809f6](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/b8809f6d8b100fe347d7414ca9a2f0ea383277c4))
* **machinery usage:** Add machinery usage gap filling ([71ba791](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/71ba79105274ecbb2ec377ce2f7704ac71a5aba8)), closes [#6](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/6)
* **models:** add dataState and dataVersions ([fde9a87](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/fde9a87226ac8ef8fb6d6deaec999ef430f20a12))
* **root_depth_model:** add root_depth_model ([41c5b8c](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/41c5b8c7ebef2f77ff660cdaf90089ddb10e013e))
* **rootingdepth:** add rootingdepth hierarchy ([b279fa3](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/b279fa30dc783a05d2051f4c7101eb3af286be55))


### Bug Fixes

* **expected json:** re-orderd and added missing data ([29f2121](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/29f21211bfb8a6a76a0474df9ca55e07543f1d7a)), closes [#6](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/6)
* **gap filling:** use base_context param ([9f649d1](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/9f649d1f5a65de17c30fd21f6201a54ef96db90a))
* **gap_filling:** fix bug in import data ([d9374e1](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/d9374e1cf5463ec171ffa6343076296d33050dc4))
* **gap_filling_hierarchy:** empty list now treated correctly (ignored) ([367e350](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/367e350999459db2d632842a31fcd35cac7c8db0)), closes [#6](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/6)
* **metadata:** Fix the formatting of the GEE_datasets_metadata.md ([cd9ebc8](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/cd9ebc8f7a8e4390f487b8e7a3fc29c63a95fc42))
* **old country name:** Change "Aus" ro "Australia" to match GoT ([2192634](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/21926340b864dd24ad384b4e86e22a1e40704c49)), closes [#6](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/6)
* **root_depth_model:** update only nodes without root depth where there is data ([5fec538](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/5fec53841f9526bb28667e3910cf2b76e6592615))
* **rootingDepth:** change rootDepth to rootingDepth to match the schema ([18e154b](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/18e154be7adca0172ef2b868aae42c8fc20513f9)), closes [#6](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/6)
* **test:** fix encoding and json root depth data ([4e47cd9](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/4e47cd942cd1a8c8d63ee48c12be7056b1145379)), closes [#6](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/6)
* **test cycle:** Changed "Aus" to "Australia" ([4ab7724](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commit/4ab772453658a4d079eb3787bd87b1e9ff46ed9d)), closes [#6](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/issues/6)
