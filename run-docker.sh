#!/bin/sh
docker build --progress=plain \
  -t hestia-gap-filling-engine:latest \
  --build-arg API_URL=$API_URL \
  .
docker run --rm \
  --name hestia-gap-filling-engine \
  -v ${PWD}:/app \
  hestia-gap-filling-engine:latest python run.py "$@"
