FROM python:3.7

WORKDIR /app

COPY requirements.txt /app/requirements.txt

RUN pip install -r requirements.txt
RUN pip install python-dotenv

ARG API_URL
COPY scripts /app/scripts
RUN chmod +x scripts/download_data.sh \
  && ./scripts/download_data.sh $API_URL

COPY . /app

CMD run.py
