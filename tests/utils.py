import os
import copy
from hestia_earth.schema import SchemaType

fixtures_path = os.path.abspath('tests/fixtures')
OLD_VERSION = '-1.0.0'
TERM = {
    "@type": "Term",
    "@id": "test"
}
MEASUREMENT = {
    "@type": "Measurement",
    "term": {
        "@type": "Term",
        "@id": "test"
    }
}
INPUT = {
    "@type": "Input",
    "term": {
        "@type": "Term",
        "@id": "test"
    }
}
PRODUCT = {
    "@type": "Product",
    "term": {
        "@type": "Term",
        "@id": "test"
    }
}
PROPERTY = {
    "@type": "Property",
    "term": {
        "@type": "Term",
        "@id": "test"
    }
}
PRACTICE = {
    "@type": "Practice",
    "term": {
        "@type": "Term",
        "@id": "test"
    }
}


def order_lists(obj: dict, props: list):
    for prop in props:
        if prop in obj:
            obj[prop] = sorted(obj[prop], key=lambda x: x.get('term').get('@id'))


def fake_download(node_id, node_type=SchemaType.TERM):
    return {"@type": node_type.value, "@id": node_id}


def fake_download_term(node_id, node_type=SchemaType.TERM):
    return {"@type": node_type.value, "@id": node_id, "name": node_id, "termType": "other"}


def fake_new_measurement(term, *args):
    node = copy.deepcopy(MEASUREMENT)
    if isinstance(term, str):
        node['term']['@id'] = term
    else:
        node['term'] = term
    return node


def fake_measurement(values, term, *args):
    node = fake_new_measurement(term)
    if values:
        values.append(node)
    return node


def fake_new_input(term):
    node = copy.deepcopy(INPUT)
    if isinstance(term, str):
        node['term']['@id'] = term
    else:
        node['term'] = term
    return node


def fake_input(values, term):
    node = fake_new_input(term)
    if values:
        values.append(node)
    return node


def fake_new_product(term):
    node = copy.deepcopy(PRODUCT)
    if isinstance(term, str):
        node['term']['@id'] = term
    else:
        node['term'] = term
    return node


def fake_product(values, term):
    node = fake_new_product(term)
    if values:
        values.append(node)
    return node


def fake_new_property(term):
    node = copy.deepcopy(PROPERTY)
    if isinstance(term, str):
        node['term']['@id'] = term
    else:
        node['term'] = term
    return node


def fake_property(values, term):
    node = fake_new_property(term)
    if values:
        values.append(node)
    return node


def fake_new_practice(term):
    node = copy.deepcopy(PRACTICE)
    if isinstance(term, str):
        node['term']['@id'] = term
    else:
        node['term'] = term
    return node


def fake_practice(values, term):
    node = fake_new_practice(term)
    if values:
        values.append(node)
    return node
