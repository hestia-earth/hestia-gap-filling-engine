import unittest
import json
from tests.utils import fixtures_path

from hestia_earth.gap_filling.impact_assessment_post_models.cycle import gap_fill, _need_gap_fill


class TestPostCycle(unittest.TestCase):
    def test_need_gap_fill(self):
        cycle = {}
        impact = {'cycle': cycle}

        # cycle has no @id => no gap-fill
        self.assertEqual(_need_gap_fill(impact), False)
        cycle['@id'] = 'id'

        # cycle has an id => gap-fill
        self.assertEqual(_need_gap_fill(impact), True)

    def test_gap_fill(self):
        # contains a full cycle
        with open(f"{fixtures_path}/impact-assessment/complete.jsonld", encoding='utf-8') as f:
            impact = json.load(f)

        cycle = impact.get('cycle')
        key, value = gap_fill(impact)[0]
        self.assertEqual(key, 'cycle')
        self.assertEqual(value, {'@type': cycle['@type'], '@id': cycle['@id']})
