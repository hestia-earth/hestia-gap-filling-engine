import unittest
import json
from .utils import fixtures_path

from hestia_earth.gap_filling.impact_assessment import gap_fill


class TestImpactAssessment(unittest.TestCase):
    #  this impact is already complete and is used to test it is not changed by gap-filling engine
    def test_gap_fill_data_complete(self):
        with open(f"{fixtures_path}/impact-assessment/complete.jsonld", encoding='utf-8') as f:
            impact = json.load(f)

        # remove cycle @id to prevent gap-filling since already included
        impact['cycle']['@id'] = None
        result = gap_fill(impact)
        self.assertEqual(result, impact)
