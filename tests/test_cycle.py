import unittest
import json
import os
from .utils import fixtures_path, order_lists

from hestia_earth.gap_filling.cycle import gap_fill

cycle_path = os.path.join(fixtures_path, 'cycle')
site_path = os.path.join(fixtures_path, 'site')


class TestCycle(unittest.TestCase):
    #  this cycle is already complete and is used to test it is not changed by gap-filling engine
    def test_gap_fill_data_complete(self):
        with open(os.path.join(cycle_path, 'complete.jsonld'), encoding='utf-8') as f:
            cycle = json.load(f)

        # remove site @id to prevent gap-filling since already included
        cycle['site']['@id'] = None
        result = gap_fill(cycle)
        self.assertEqual(result, cycle)

    # run a series of cycles that we have verified the values for
    def test_gap_fill_reconciliation(self):
        src_dir = os.path.join(cycle_path, 'reconciliation')
        src_site_dir = os.path.join(site_path, 'reconciliation')
        ids = os.listdir(src_dir)

        for id in ids:
            with open(os.path.join(src_dir, id, 'cycle.jsonld'), encoding='utf-8') as f:
                cycle = json.load(f)
            with open(os.path.join(src_dir, id, 'gap-filled.jsonld'), encoding='utf-8') as f:
                expected = json.load(f)
            # load the corresponding gap-filled site
            with open(os.path.join(src_site_dir, cycle['site']['@id'], 'gap-filled.jsonld'), encoding='utf-8') as f:
                site = json.load(f)

            original_site = cycle['site']
            cycle['site'] = site
            # remove site @id to prevent gap-filling since already included
            cycle['site']['@id'] = None
            result = gap_fill(cycle)
            # restore site to avoid having diffs
            cycle['site'] = original_site
            result['site'] = original_site
            # sort all lists to avoid errors on order
            order_lists(cycle, ['inputs', 'products', 'emissions'])
            order_lists(result, ['inputs', 'products', 'emissions'])
            self.assertEqual(result, expected)
