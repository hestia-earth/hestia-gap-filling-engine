import unittest

from hestia_earth.gap_filling.gee import should_gap_fill, _site_gadm_id


class TestGEE(unittest.TestCase):
    def test_should_gap_fill(self):
        # empty => no gap-fill
        self.assertEqual(should_gap_fill({}), False)

        # with latitude / longitude => gap-fill
        self.assertEqual(should_gap_fill({'longitude': 0, 'latitude': 0}), True)

        # with a region => gap-fill
        self.assertEqual(should_gap_fill({'region': {'@id': 'GADM-GBR'}}), True)

        # with a country => gap-fill
        self.assertEqual(should_gap_fill({'country': {'@id': 'GADM-GBR'}}), True)

        # with a boundary => gap-fill
        self.assertEqual(should_gap_fill({'boundary': {'type': 'Features'}}), True)

    def test_site_gadm_id(self):
        site = {}
        self.assertEqual(_site_gadm_id(site), None)

        site['country'] = {'@id': 'country'}
        self.assertEqual(_site_gadm_id(site), 'country')

        site['region'] = {'@id': 'region'}
        self.assertEqual(_site_gadm_id(site), 'region')
