import unittest
from unittest.mock import patch
import json
from tests.utils import fixtures_path, fake_practice

from hestia_earth.gap_filling.cycle_models.irrigated import TERM_ID, GAP_FILLING_KEY, _need_gap_fill, gap_fill

class_path = 'hestia_earth.gap_filling.cycle_models.irrigated'
fixtures_folder = f"{fixtures_path}/cycle/{TERM_ID}"


class TestIrrigated(unittest.TestCase):
    def test_need_gap_fill(self):
        # practice with model and gap-filled with different version => gap-fill
        with open(f"{fixtures_folder}/old-version.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)
        need_gap_fill = _need_gap_fill(cycle)
        self.assertEqual(need_gap_fill, True)

        # practice with model and gap-filled with same version => no gap-fill
        with open(f"{fixtures_folder}/same-version.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)
        need_gap_fill = _need_gap_fill(cycle)
        self.assertEqual(need_gap_fill, False)

        # with irrigation practice => no gap-fill
        with open(f"{fixtures_folder}/with-irrigation-practices/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)
        need_gap_fill = _need_gap_fill(cycle)
        self.assertEqual(need_gap_fill, False)

        # with irrigation input but value too low => no gap-fill
        with open(f"{fixtures_folder}/with-irrigation-inputs-low-value/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)
        need_gap_fill = _need_gap_fill(cycle)
        self.assertEqual(need_gap_fill, False)

    @patch(f"{class_path}._get_or_create_practice", side_effect=fake_practice)
    def test_gap_fill(self, _m):
        # data on total crop residue, product and burnt practice amount => gap-fill
        with open(f"{fixtures_folder}/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)

        with open(f"{fixtures_folder}/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(cycle)[0]
        self.assertEqual(key, GAP_FILLING_KEY)
        self.assertEqual(value, expected)
