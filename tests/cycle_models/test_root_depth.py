import unittest
from unittest.mock import patch
import json
from tests.utils import OLD_VERSION, fixtures_path, fake_property, fake_download

from hestia_earth.gap_filling.version import VERSION
from hestia_earth.gap_filling.cycle_models.root_depth import TERM_ID, _need_gap_fill_product, gap_fill

class_path = 'hestia_earth.gap_filling.cycle_models.root_depth'
fixtures_folder = f"{fixtures_path}/cycle/{TERM_ID}"


class TestRootDepth(unittest.TestCase):
    def test_need_gap_fill_product(self):
        product = {}
        # no properties => gap-fill
        self.assertEqual(_need_gap_fill_product(product), True)

        # product with model and gap-filled with different version => gap-fill
        prop = {
            'term': {
                '@id': TERM_ID
            },
            'gapFilled': ['value'],
            'gapFilledVersion': [OLD_VERSION]
        }
        product['properties'] = [prop]
        self.assertEqual(_need_gap_fill_product(product), True)

        # product with model and gap-filled with same version => NO gap-fill
        prop['gapFilledVersion'] = [VERSION]
        self.assertEqual(_need_gap_fill_product(product), False)

    @patch(f"{class_path}.download_hestia", side_effect=fake_download)
    @patch(f"{class_path}._get_or_create_property", side_effect=fake_property)
    def test_gap_fill(self, _m1, _m2):
        with open(f"{fixtures_folder}/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)

        with open(f"{fixtures_folder}/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(cycle)[0]
        self.assertEqual(key, 'products.0.properties')
        self.assertEqual(value, expected)

    @patch(f"{class_path}.download_hestia", side_effect=fake_download)
    @patch(f"{class_path}._get_or_create_property", side_effect=fake_property)
    def test_gap_fill_with_irrigation(self, _m1, _m2):
        with open(f"{fixtures_folder}/with-irrigation/cycle.jsonld", encoding='utf-8') as f:
            cycle_irrigation = json.load(f)

        with open(f"{fixtures_folder}/with-irrigation/gap-filled.jsonld", encoding='utf-8') as f:
            expected_irrigation = json.load(f)

        key, value = gap_fill(cycle_irrigation)[0]
        self.assertEqual(key, 'products.0.properties')
        self.assertEqual(value, expected_irrigation)
