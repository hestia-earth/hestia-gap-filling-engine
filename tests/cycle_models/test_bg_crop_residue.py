import unittest
from unittest.mock import patch
import json
from tests.utils import fixtures_path, fake_product

from hestia_earth.gap_filling.cycle_models.bg_crop_residue import TERM_ID, _need_gap_fill, gap_fill

class_path = 'hestia_earth.gap_filling.cycle_models.bg_crop_residue'
fixtures_folder = f"{fixtures_path}/cycle/{TERM_ID}"


class TestBelowGroundCropResidue(unittest.TestCase):
    @patch(f"{class_path}._should_gap_fill_term_type", return_value=True)
    def test_need_gap_fill(self, _m):
        cycle = {'products': []}

        # no products => gap-fill
        need_gap_fill, _p = _need_gap_fill(cycle)
        self.assertEqual(need_gap_fill, False)

        # product with model and gap-filled with different version => gap-fill
        with open(f"{fixtures_folder}/old-version.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)
        need_gap_fill, _p = _need_gap_fill(cycle)
        self.assertEqual(need_gap_fill, True)

        # product with model and gap-filled with same version => no gap-fill
        with open(f"{fixtures_folder}/same-version.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)
        need_gap_fill, _p = _need_gap_fill(cycle)
        self.assertEqual(need_gap_fill, False)

    @patch(f"{class_path}._get_or_create_product", side_effect=fake_product)
    def test_gap_fill(self, _m):
        with open(f"{fixtures_folder}/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)

        with open(f"{fixtures_folder}/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(cycle)[0]
        self.assertEqual(key, 'products')
        self.assertEqual(value, expected)

    @patch(f"{class_path}._get_or_create_product", side_effect=fake_product)
    def test_gap_fill_koga(self, _m):
        with open(f"{fixtures_folder}/koga/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)

        with open(f"{fixtures_folder}/koga/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        sb_key, sb_value = gap_fill(cycle)[0]
        self.assertEqual(sb_key, 'products')
        self.assertEqual(sb_value, expected)

    @patch(f"{class_path}._get_or_create_product", side_effect=fake_product)
    def test_gap_fill_default(self, _m):
        with open(f"{fixtures_folder}/default-no-dm-property/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)

        with open(f"{fixtures_folder}/default-no-dm-property/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        sb_key, sb_value = gap_fill(cycle)[0]
        self.assertEqual(sb_key, 'products')
        self.assertEqual(sb_value, expected)
