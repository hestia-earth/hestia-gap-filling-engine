import unittest
from unittest.mock import patch
import json
from tests.utils import OLD_VERSION, fixtures_path, fake_input
from hestia_earth.schema import SiteSiteType

from hestia_earth.gap_filling.version import VERSION
from hestia_earth.gap_filling.cycle_models.machinery_usage import TERM_ID, _need_gap_fill, gap_fill

class_path = 'hestia_earth.gap_filling.cycle_models.machinery_usage'
fixtures_folder = f"{fixtures_path}/cycle/{TERM_ID}"


class TestMachineryUsage(unittest.TestCase):
    @patch(f"{class_path}._should_gap_fill_term_type", return_value=True)
    def test_need_gap_fill(self, _m):
        cycle = {'site': {}, 'inputs': []}
        # no site.siteType => no gap-fill
        self.assertEqual(_need_gap_fill(cycle), False)
        cycle['site']['siteType'] = SiteSiteType.CROPLAND.value

        # no inputs => gap-fill
        self.assertEqual(_need_gap_fill(cycle), True)

        # product with model and gap-filled with different version => gap-fill
        hinput = {
            'term': {
                '@id': TERM_ID
            },
            'gapFilled': ['value'],
            'gapFilledVersion': [OLD_VERSION]
        }
        cycle['inputs'].append(hinput)
        self.assertEqual(_need_gap_fill(cycle), True)

        # product with model and gap-filled with same version => NO gap-fill
        hinput['gapFilledVersion'] = [VERSION]
        self.assertEqual(_need_gap_fill(cycle), False)

    @patch(f"{class_path}._get_or_create_input", side_effect=fake_input)
    def test_gap_fill(self, _m):
        with open(f"{fixtures_folder}/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)

        with open(f"{fixtures_folder}/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(cycle)[0]
        self.assertEqual(key, 'inputs')
        self.assertEqual(value, expected)
