import unittest
import json
from tests.utils import fixtures_path

from hestia_earth.gap_filling.cycle_models.start_date import GAP_FILLING_KEY, _need_gap_fill, gap_fill

fixtures_folder = f"{fixtures_path}/cycle/{GAP_FILLING_KEY}"


class TestCropSeed(unittest.TestCase):
    def test_need_gap_fill(self):
        cycle = {}

        # no endDate => no gap-fill
        cycle['endDate'] = None
        self.assertEqual(_need_gap_fill(cycle), False)

        cycle['endDate'] = '2010'
        # no cycleDuration => no gap-fill
        cycle['cycleDuration'] = None
        self.assertEqual(_need_gap_fill(cycle), False)

        cycle['cycleDuration'] = 120
        # with a startDate => no gap-fill
        cycle['startDate'] = '2010'
        self.assertEqual(_need_gap_fill(cycle), False)

        cycle['startDate'] = None
        # endDate not precise enough => no gap-fill
        cycle['endDate'] = '2020-01'
        self.assertEqual(_need_gap_fill(cycle), False)

        # endDate is precise enough => gap-fill
        cycle['endDate'] = '2020-01-01'
        self.assertEqual(_need_gap_fill(cycle), True)

    def test_gap_fill(self):
        with open(f"{fixtures_folder}/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)

        key, value = gap_fill(cycle)[0]
        self.assertEqual(key, GAP_FILLING_KEY)
        self.assertEqual(value, '2008-08-04')
