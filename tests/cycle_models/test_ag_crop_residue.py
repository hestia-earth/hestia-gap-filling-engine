import unittest
from unittest.mock import patch
import json
from tests.utils import fixtures_path, fake_product

from hestia_earth.gap_filling.cycle_models.ag_crop_residue import GAP_FILLING_KEY, _need_gap_fill, gap_fill, \
    _need_gap_fill_model

class_path = 'hestia_earth.gap_filling.cycle_models.ag_crop_residue'
fixtures_folder = f"{fixtures_path}/cycle/aboveGroundCropResidue"


class FakeModel():
    def __init__(self, TERM_ID, PRACTICE_TERM_ID):
        self.TERM_ID = TERM_ID
        self.PRACTICE_TERM_ID = PRACTICE_TERM_ID

    def gap_fill_practice_value(*args): return None
    def need_gap_fill(*args): return True
    def gap_fill(*args): return 0


class TestAboveGroundCropResidue(unittest.TestCase):
    @patch(f"{class_path}._should_gap_fill_term_type", return_value=True)
    def test_need_gap_fill(self, _m):
        cycle = {GAP_FILLING_KEY: []}

        # no products => no gap-fill
        need_gap_fill, _p = _need_gap_fill(cycle)
        self.assertEqual(need_gap_fill, False)

        # product with total crop residue => gap-fill
        with open(f"{fixtures_folder}/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)
        need_gap_fill, _p = _need_gap_fill(cycle)
        self.assertEqual(need_gap_fill, True)

    def test_need_gap_fill_model(self):
        model = FakeModel('aboveGroundCropResidueBurnt', 'residueBurnt')

        # product with model and gap-filled with different version => gap-fill
        with open(f"{fixtures_folder}/old-version.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)
        need_gap_fill, _p = _need_gap_fill_model(model, cycle, {})
        self.assertEqual(need_gap_fill, True)

        # product with model and gap-filled with same version => no gap-fill
        with open(f"{fixtures_folder}/same-version.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)
        need_gap_fill, _p = _need_gap_fill_model(model, cycle, {})
        self.assertEqual(need_gap_fill, False)

    @patch(f"{class_path}._get_or_create_product", side_effect=fake_product)
    def test_gap_fill(self, _m):
        with open(f"{fixtures_folder}/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)

        with open(f"{fixtures_folder}/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(cycle)[0]
        self.assertEqual(key, GAP_FILLING_KEY)
        self.assertEqual(value, expected)
