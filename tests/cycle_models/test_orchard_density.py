import unittest
from unittest.mock import patch
import json
from tests.utils import OLD_VERSION, fixtures_path, fake_practice

from hestia_earth.gap_filling.version import VERSION
from hestia_earth.gap_filling.cycle_models.orchard_density import TERM_ID, _need_gap_fill, gap_fill

class_path = 'hestia_earth.gap_filling.cycle_models.orchard_density'
fixtures_folder = f"{fixtures_path}/cycle/{TERM_ID}"


class TestOrchardDensity(unittest.TestCase):
    @patch(f"{class_path}._should_gap_fill_term_type", return_value=True)
    def test_need_gap_fill(self, _m):
        cycle = {'practices': []}

        # no inputs => gap-fill
        self.assertEqual(_need_gap_fill(cycle), True)

        # product with model and gap-filled with different version => gap-fill
        hpractice = {
            'term': {
                '@id': TERM_ID
            },
            'gapFilled': ['value'],
            'gapFilledVersion': [OLD_VERSION]
        }
        cycle['practices'].append(hpractice)
        self.assertEqual(_need_gap_fill(cycle), True)

        # product with model and gap-filled with same version => NO gap-fill
        hpractice['gapFilledVersion'] = [VERSION]
        self.assertEqual(_need_gap_fill(cycle), False)

    @patch(f"{class_path}._get_or_create_practice", side_effect=fake_practice)
    def test_gap_fill(self, _m):
        with open(f"{fixtures_folder}/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)

        with open(f"{fixtures_folder}/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(cycle)[0]
        self.assertEqual(key, 'practices')
        self.assertEqual(value, expected)

    @patch(f"{class_path}._get_or_create_practice", side_effect=fake_practice)
    def test_gap_fill_no_orchard_crop(self, _m):

        with open(f"{fixtures_folder}/no-orchard-crop/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)

        key, value = gap_fill(cycle)[0]
        self.assertEqual(key, 'practices')
        self.assertEqual(value, [])
