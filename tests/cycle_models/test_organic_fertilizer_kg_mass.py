import unittest
from unittest.mock import patch
import json
from tests.utils import OLD_VERSION, fixtures_path, fake_input

from hestia_earth.gap_filling.version import VERSION
from hestia_earth.gap_filling.cycle_models.organic_fertilizer_kg_mass import _need_gap_fill, gap_fill

class_path = 'hestia_earth.gap_filling.cycle_models.organic_fertilizer_kg_mass'
fixtures_folder = f"{fixtures_path}/cycle/organic-fertilizer-kg-mass"


class TestOrganicFertilizerKgMass(unittest.TestCase):
    def test_need_gap_fill(self):
        # cycle with no organic fertiliser inputs
        inputs = []
        self.assertEqual(_need_gap_fill(inputs), [])

        term_id = 'manureFresh'
        # cycle with an organic fertiliser + missing as Mass data
        inputs.append({
            'term': {
                '@id': term_id + 'AsN'
            },
            'value': 0.00208
        })
        self.assertEqual(_need_gap_fill(inputs), [term_id])

        # cycle with an organic fertiliser + as mass data gap-filled old version
        inputs.append({
            'term': {
                '@id': term_id + 'KgMass'
            },
            'value': 0.4,
            'gapFilled': ['value'],
            'gapFilledVersion': [OLD_VERSION]
        })
        self.assertEqual(_need_gap_fill(inputs), [term_id])

        # cycle with an organic fertiliser gap-filled current version + as mass gap-filled data current version
        inputs[0]['gapFilled'] = ['value']
        inputs[0]['gapFilledVersion'] = [VERSION]
        inputs[1]['gapFilledVersion'] = [VERSION]
        self.assertEqual(_need_gap_fill(inputs), [])

    @patch(f"{class_path}._get_or_create_input", side_effect=fake_input)
    def test_gap_fill(self, _m):
        # cycle with an organic fertiliser => Iranian Hazelnut example
        with open(f"{fixtures_folder}/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)

        with open(f"{fixtures_folder}/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(cycle)[0]
        self.assertEqual(key, 'inputs')
        self.assertCountEqual(value, expected)
