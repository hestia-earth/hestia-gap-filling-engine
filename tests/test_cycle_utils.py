import unittest
from unittest.mock import patch
import json

from hestia_earth.schema import TermTermType
from .utils import fake_download, fixtures_path

from hestia_earth.gap_filling.version import VERSION
from hestia_earth.gap_filling.cycle_utils import _should_gap_fill_term_type, _get_or_create_input, \
    _get_or_create_product


class TestCycleUtils(unittest.TestCase):
    @patch('hestia_earth.gap_filling.cycle_utils.download_hestia')
    def test_should_gap_fill_term_type(self, mock_download):
        with open(f"{fixtures_path}/cycle/complete.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)

        cycle['dataCompleteness'][TermTermType.CROPRESIDUE.value] = True
        mock_download.return_value = {
            'termType': TermTermType.CROPRESIDUE.value
        }
        self.assertEqual(_should_gap_fill_term_type(cycle, 'termid'), False)

        cycle['dataCompleteness'][TermTermType.CROPRESIDUE.value] = False
        mock_download.return_value = {
            'termType': TermTermType.CROPRESIDUE.value
        }
        self.assertEqual(_should_gap_fill_term_type(cycle, 'termid'), True)

        # termType not in dataCompleteness
        mock_download.return_value = {
            'termType': TermTermType.CROPRESIDUEMANAGEMENT.value
        }
        self.assertEqual(_should_gap_fill_term_type(cycle, 'termid'), True)

    @patch('hestia_earth.gap_filling.cycle_utils.download_hestia', side_effect=fake_download)
    def test_get_or_create_input(self, _m):
        term_id = 'term'

        # no input match term, create a new one
        self.assertEqual(_get_or_create_input([], term_id), {
            "@type": "Input",
            "term": {
                "@type": "Term",
                "@id": term_id
            },
            "gapFilled": ['term'],
            "gapFilledVersion": [VERSION]
        })

        # with existing input, return it
        input = {
            "@type": "Input",
            "term": {
                "@type": "Term",
                "@id": term_id
            }
        }
        self.assertEqual(_get_or_create_input([input], term_id), input)

    @patch('hestia_earth.gap_filling.cycle_utils.download_hestia', side_effect=fake_download)
    def test_get_or_create_product(self, _m):
        term_id = 'term'

        # no product match term, create a new one
        self.assertEqual(_get_or_create_product([], term_id), {
            "@type": "Product",
            "term": {
                "@type": "Term",
                "@id": term_id
            },
            "primary": False,
            "gapFilled": ['term'],
            "gapFilledVersion": [VERSION]
        })

        # with existing product, return it
        product = {
            "@type": "Product",
            "term": {
                "@type": "Term",
                "@id": term_id
            }
        }
        self.assertEqual(_get_or_create_product([product], term_id), product)
