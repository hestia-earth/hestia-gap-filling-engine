import unittest
import json
from tests.utils import fixtures_path

from hestia_earth.gap_filling.site_post_models.measurement_value import _need_gap_fill, gap_fill


fixtures_folder = f"{fixtures_path}/site/value"


class TestMeasurementValue(unittest.TestCase):
    def test_need_gap_fill(self):
        site = {}

        # no measurements => NO gap-fill
        self.assertEqual(_need_gap_fill(site), (False, []))

        # measurements without value/min/max => NO gap-fill
        site['measurements'] = [{}]
        self.assertEqual(_need_gap_fill(site), (False, []))

        # measurements without min/max => NO gap-fill
        site['measurements'] = [{
            'value': []
        }]
        self.assertEqual(_need_gap_fill(site), (False, []))

        # with min and max and value => NO gap-fill
        site['measurements'] = [{
            'min': [5],
            'max': [50],
            'value': [25]
        }]
        self.assertEqual(_need_gap_fill(site), (False, []))

        # with min and max but not value => gap-fill
        site['measurements'] = [{
            'min': [5],
            'max': [10],
            'value': []
        }]
        (gap_fill, measurements) = _need_gap_fill(site)
        self.assertEqual(gap_fill, True)
        self.assertEqual(measurements, site['measurements'])

    def test_gap_fill(self):
        with open(f"{fixtures_folder}/site.jsonld", encoding='utf-8') as f:
            site = json.load(f)

        with open(f"{fixtures_folder}/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(site)[0]
        self.assertEqual(key, 'measurements')
        self.assertEqual(value, expected)
