import unittest
from unittest.mock import patch
from hestia_earth.schema import NodeType

from hestia_earth.gap_filling import gap_fill
from hestia_earth.gap_filling.gap_filling import _merge_gap_fills


class TestGapFilling(unittest.TestCase):
    @patch('hestia_earth.gap_filling.gap_fill_cycle')
    @patch('hestia_earth.gap_filling.gap_fill_site')
    def test_gap_fill_node_type(self, mock_gap_fill_site, mock_gap_fill_cycle):
        node = {'@type': NodeType.SITE.value}
        gap_fill(node)
        mock_gap_fill_site.assert_called_once_with(node)

        node = {'@type': NodeType.CYCLE.value}
        gap_fill(node)
        mock_gap_fill_cycle.assert_called_once_with(node)

    def test_merge_gap_fills(self):
        source = {
            'list': [{
                'value': {
                    'items': []
                }
            }, {}]
        }
        new_item1 = {'id': 'new-item-1', 'term': {'@id': 'term-1'}}
        new_item2 = {'id': 'new-item-2', 'term': {'@id': 'term-2'}}
        new_measurement = {'id': 'new-measurement', 'term': {'@id': 'term-3'}}
        new_value = {'@type': 'Term'}
        new_list_item = {'term': {'@id': 'term-4'}}
        gap_fills = [
            ('list.0.value.items', [new_item1]),
            ('list.0.value.measurements', [new_measurement]),
            ('list.0.value.items', [new_item2]),
            ('list.1.value', new_value),
            ('list', [new_list_item])
        ]

        self.assertEqual(_merge_gap_fills(source, gap_fills), {
            'list': [{
                'value': {
                    'items': [new_item1, new_item2],
                    'measurements': [new_measurement]
                }
            }, {
                'value': new_value
            }, new_list_item]
        })
