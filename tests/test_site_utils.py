import unittest
from unittest.mock import patch
from .utils import fake_download

from hestia_earth.gap_filling.version import VERSION
from hestia_earth.gap_filling.site_utils import _get_or_create_measurement


class TestSiteUtils(unittest.TestCase):
    @patch('hestia_earth.gap_filling.site_utils.find_node_exact', return_value={"@type": "Source", "@id": "source"})
    @patch('hestia_earth.gap_filling.site_utils.download_hestia', side_effect=fake_download)
    def test_get_or_create_measurement(self, _m1, _m2):
        term_id = 'term'

        # no measurement match term, create a new one
        self.assertEqual(_get_or_create_measurement([], term_id), {
            "@type": "Measurement",
            "term": {
                "@type": "Term",
                "@id": term_id
            },
            "gapFilled": ['term'],
            "gapFilledVersion": [VERSION]
        })

        # with a title
        title = 'title'
        self.assertEqual(_get_or_create_measurement([], term_id, title), {
            "@type": "Measurement",
            "term": {
                "@type": "Term",
                "@id": term_id
            },
            "source": {
                "@type": "Source",
                "@id": "source"
            },
            "gapFilled": ['term', 'source'],
            "gapFilledVersion": [VERSION, VERSION]
        })

        # with existing measurement, return it
        measurement = {
            "@type": "Measurement",
            "term": {
                "@type": "Term",
                "@id": term_id
            }
        }
        self.assertEqual(_get_or_create_measurement([measurement], term_id), measurement)
