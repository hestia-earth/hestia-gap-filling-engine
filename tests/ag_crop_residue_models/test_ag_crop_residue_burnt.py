import unittest
import json
from tests.utils import fixtures_path

from hestia_earth.gap_filling.ag_crop_residue_models.ag_crop_residue_burnt import TERM_ID, \
    need_gap_fill, gap_fill_practice_value

fixtures_folder = f"{fixtures_path}/cycle/{TERM_ID}"


class TestAboveGroundCropResidueBurnt(unittest.TestCase):
    def test_need_gap_fill(self):
        cycle = {}

        # no primary product => no gap-fill
        self.assertEqual(need_gap_fill(cycle, None), False)

        # with primary product => gap-fill
        self.assertEqual(need_gap_fill(cycle, {}), True)

    def test_gap_fill_practice_value(self):
        with open(f"{fixtures_folder}/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)

        primary_product = cycle.get('products')[0]
        self.assertEqual(gap_fill_practice_value(cycle, primary_product), 25.200000000000006)
