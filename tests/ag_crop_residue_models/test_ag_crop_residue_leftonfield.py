import unittest

from hestia_earth.gap_filling.ag_crop_residue_models.ag_crop_residue_leftonfield import gap_fill_practice_value


class TestAboveGroundCropResidueLeftOnField(unittest.TestCase):
    def test_gap_fill_practice_value(self):
        self.assertEqual(gap_fill_practice_value(), None)
