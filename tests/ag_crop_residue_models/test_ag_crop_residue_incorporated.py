import unittest

from hestia_earth.gap_filling.ag_crop_residue_models.ag_crop_residue_incorporated import need_gap_fill, \
    gap_fill_practice_value


class TestAboveGroundCropResidueIncorporated(unittest.TestCase):
    def test_need_gap_fill(self):
        self.assertEqual(need_gap_fill(), True)

    def test_gap_fill_practice_value(self):
        self.assertEqual(gap_fill_practice_value(), None)
