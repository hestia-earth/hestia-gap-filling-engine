import unittest
from unittest.mock import patch
from .utils import fake_download

from hestia_earth.gap_filling.version import VERSION
from hestia_earth.gap_filling.utils import should_gap_fill, _get_or_create_property, _merge_lists_term


class TestUtils(unittest.TestCase):
    def test_should_gap_fill(self):
        term_id = 'termId'
        values = [{
            'term': {
                '@id': term_id
            }
        }]

        # term not found => gap-fill
        self.assertEqual(should_gap_fill(values, 'random id'), True)

        # with low reliability level => gap-fill
        values[0]['reliability'] = 3
        self.assertEqual(should_gap_fill(values, term_id), True)

        # with high reliability level => no gap-fill
        values[0]['reliability'] = 1
        self.assertEqual(should_gap_fill(values, term_id), False)

        # key is gap-filled => no gap-fill
        values[0]['gapFilled'] = []
        self.assertEqual(should_gap_fill(values, term_id), False)

        # key is gap-filled => no gap-fill
        values[0]['gapFilled'] = ['value']
        values[0]['gapFilledVersion'] = ['0.0.0']
        self.assertEqual(should_gap_fill(values, term_id), False)

        # with a different major version => gap-fill
        [major, minor, patch] = VERSION.split('.')
        major_version_before = '.'.join([str(int(major) - 1), minor, patch])
        values[0]['gapFilledVersion'] = [major_version_before]
        self.assertEqual(should_gap_fill(values, term_id), True)

        # with the same major version => no gap-fill
        values[0]['gapFilledVersion'] = [VERSION]
        self.assertEqual(should_gap_fill(values, term_id), False)

    @patch('hestia_earth.gap_filling.utils.download_hestia', side_effect=fake_download)
    def test_get_or_create_property(self, _m):
        term_id = 'term'

        # no property match term, create a new one
        self.assertEqual(_get_or_create_property([], term_id), {
            "@type": "Property",
            "term": {
                "@type": "Term",
                "@id": term_id
            },
            "gapFilled": ['term'],
            "gapFilledVersion": [VERSION]
        })

        # with existing property, return it
        property = {
            "@type": "Property",
            "term": {
                "@type": "Term",
                "@id": term_id
            }
        }
        self.assertEqual(_get_or_create_property([property], term_id), property)

    def test_merge_lists_term(self):
        measurement1 = {
            "@type": "Measurement",
            "term": {
                "@id": "rainfallAnnual",
                "@type": "Term"
            },
            "value": [1]
        }
        measurement2 = {
            "@type": "Measurement",
            "term": {
                "@id": "rainfallAnnual",
                "@type": "Term"
            },
            "value": [2]
        }
        source = [measurement1]
        dest = [measurement2]
        result = _merge_lists_term(source, dest)
        self.assertEqual(result, [measurement2])

    def test_merge_lists_term_handle_measurement(self):
        # both measurements have the same term.@id, but with different dates
        # it should add both measurements to the result
        measurement1 = {
            "@type": "Measurement",
            "term": {
                "@id": "rainfallAnnual",
                "@type": "Term"
            },
            "value": [1],
            "startDate": "2009-01-01",
            "endDate": "2009-12-31"
        }
        measurement2 = {
            "@type": "Measurement",
            "term": {
                "@id": "rainfallAnnual",
                "@type": "Term"
            },
            "value": [2],
            "startDate": "2010-01-01",
            "endDate": "2010-12-31"
        }
        source = [measurement1]
        dest = [measurement2]
        result = _merge_lists_term(source, dest)
        self.assertEqual(result, [measurement1, measurement2])
