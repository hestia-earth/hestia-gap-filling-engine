import unittest
import json
from tests.utils import fixtures_path

from hestia_earth.gap_filling.site_models.water_basin import _need_gap_fill, GAP_FILLING_KEY, gap_fill

class_path = 'hestia_earth.gap_filling.site_models.water_basin'
fixtures_folder = f"{fixtures_path}/site/{GAP_FILLING_KEY}"


class TestAwareWaterBasinId(unittest.TestCase):
    def test_need_gap_fill(self):
        # no model => gap-fill
        site = {'longitude': 0, 'latitude': 0}
        self.assertEqual(_need_gap_fill(site), True)

        # with model => NO gap-fill
        site[GAP_FILLING_KEY] = {}
        self.assertEqual(_need_gap_fill(site), False)

    def test_gap_fill_boundary(self):
        with open(f"{fixtures_path}/site/boundary.jsonld", encoding='utf-8') as f:
            site = json.load(f)

        with open(f"{fixtures_folder}/id.txt", encoding='utf-8') as f:
            expected = f.read().strip()

        key, value = gap_fill(site)[0]
        self.assertEqual(key, GAP_FILLING_KEY)
        self.assertEqual(value, expected)

    def test_gap_fill_coordinates(self):
        with open(f"{fixtures_path}/site/coordinates.jsonld", encoding='utf-8') as f:
            site = json.load(f)

        with open(f"{fixtures_folder}/id.txt", encoding='utf-8') as f:
            expected = f.read().strip()

        key, value = gap_fill(site)[0]
        self.assertEqual(key, GAP_FILLING_KEY)
        self.assertEqual(value, expected)

    def test_gap_fill_gadm(self):
        with open(f"{fixtures_path}/site/gadm.jsonld", encoding='utf-8') as f:
            site = json.load(f)

        with open(f"{fixtures_folder}/gadm/id.txt", encoding='utf-8') as f:
            expected = f.read().strip()

        key, value = gap_fill(site)[0]
        self.assertEqual(key, GAP_FILLING_KEY)
        self.assertEqual(value, expected)
