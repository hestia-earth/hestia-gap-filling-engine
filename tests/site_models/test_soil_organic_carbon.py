import unittest
from unittest.mock import patch
import json
from tests.utils import OLD_VERSION, fixtures_path, fake_measurement

from hestia_earth.gap_filling.version import VERSION
from hestia_earth.gap_filling.site_models.soil_organic_carbon import _need_gap_fill, TERM_ID, gap_fill

class_path = 'hestia_earth.gap_filling.site_models.soil_organic_carbon'
fixtures_folder = f"{fixtures_path}/site/{TERM_ID}"


class TestsoilOrganicCarbonContent(unittest.TestCase):
    def test_need_gap_fill(self):
        # but not measurement => gap-fill
        site = {'longitude': 0, 'latitude': 0, 'measurements': []}
        self.assertEqual(_need_gap_fill(site), True)

        # with measurement with model and gap-filled with different version => gap-fill
        measurement = {
            'term': {
                '@id': TERM_ID
            },
            'gapFilled': ['value'],
            'gapFilledVersion': [OLD_VERSION]
        }
        site['measurements'].append(measurement)
        self.assertEqual(_need_gap_fill(site), True)

        # with measurement with model and gap-filled with same version => NO gap-fill
        measurement['gapFilledVersion'] = [VERSION]
        self.assertEqual(_need_gap_fill(site), False)

    @patch(f"{class_path}._get_or_create_measurement", side_effect=fake_measurement)
    def test_gap_fill_coordinates(self, _m):
        with open(f"{fixtures_path}/site/coordinates.jsonld", encoding='utf-8') as f:
            site = json.load(f)

        with open(f"{fixtures_folder}/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(site)[0]
        self.assertEqual(key, 'measurements')
        self.assertEqual(value, expected)

    @patch(f"{class_path}._get_or_create_measurement", side_effect=fake_measurement)
    def test_gap_fill_boundary(self, _m):
        with open(f"{fixtures_path}/site/boundary.jsonld", encoding='utf-8') as f:
            site = json.load(f)

        with open(f"{fixtures_folder}/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(site)[0]
        self.assertEqual(key, 'measurements')
        self.assertEqual(value, expected)

    @patch(f"{class_path}._get_or_create_measurement", side_effect=fake_measurement)
    def test_gap_fill_gadm(self, _m):
        with open(f"{fixtures_path}/site/gadm.jsonld", encoding='utf-8') as f:
            site = json.load(f)

        with open(f"{fixtures_folder}/gadm/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(site)[0]
        self.assertEqual(key, 'measurements')
        self.assertEqual(value, expected)
