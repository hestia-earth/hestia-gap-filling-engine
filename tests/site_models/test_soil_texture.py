import unittest
from unittest.mock import patch
import json
from tests.utils import OLD_VERSION, fixtures_path, fake_download

from hestia_earth.gap_filling.version import VERSION
from hestia_earth.gap_filling.site_models.soil_texture import TERM_IDS, _need_gap_fill, _gap_fill_single, gap_fill

class_path = 'hestia_earth.gap_filling.site_models.soil_texture'
fixtures_folder = f"{fixtures_path}/site/soil-texture"


class TestSoilTexture(unittest.TestCase):
    def test_need_gap_fill(self):
        # but not measurement => gap-fill
        site = {'longitude': 0, 'latitude': 0, 'measurements': []}
        self.assertEqual(_need_gap_fill(site), True)

        # with 1 measurement with model and gap-filled with different version => gap-fill
        measurement = {
            'term': {
                '@id': list(TERM_IDS.keys())[0]
            },
            'gapFilled': ['value'],
            'gapFilledVersion': [OLD_VERSION]
        }
        site['measurements'].append(measurement)
        self.assertEqual(_need_gap_fill(site), True)

        # with 3 measurements with model and gap-filled with same version => NO gap-fill
        measurement['gapFilledVersion'] = VERSION
        site['measurements'].append({
            'term': {
                '@id': list(TERM_IDS.keys())[1]
            },
            'gapFilled': ['value'],
            'gapFilledVersion': [VERSION]
        })
        site['measurements'].append({
            'term': {
                '@id': list(TERM_IDS.keys())[2]
            },
            'gapFilled': ['value'],
            'gapFilledVersion': [VERSION]
        })
        self.assertEqual(_need_gap_fill(site), False)

    @patch(f"{class_path}.find_node_exact", return_value={"@id": "source"})
    @patch(f"{class_path}.download_hestia", side_effect=fake_download)
    def test_gap_fill_single(self, _m1, _m2):
        model = list(TERM_IDS.keys())[0]
        measurements = [{
            'term': {
                '@id': list(TERM_IDS.keys())[1]
            },
            'value': [20]
        }, {
            'term': {
                '@id': list(TERM_IDS.keys())[2]
            },
            'value': [30]
        }]
        measurement = _gap_fill_single(measurements, model)[0]
        self.assertEqual(measurement.get('value'), [50])

    @patch(f"{class_path}.find_node_exact", return_value={"@id": "source"})
    @patch(f"{class_path}.download_hestia", side_effect=fake_download)
    def test_gap_fill_coordinates(self, _m1, _m2):
        with open(f"{fixtures_path}/site/coordinates.jsonld", encoding='utf-8') as f:
            site = json.load(f)

        with open(f"{fixtures_folder}/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(site)[0]
        self.assertEqual(key, 'measurements')
        self.assertEqual(value, expected)

    @patch(f"{class_path}.find_node_exact", return_value={"@id": "source"})
    @patch(f"{class_path}.download_hestia", side_effect=fake_download)
    def test_gap_fill_boundary(self, _m1, _m2):
        with open(f"{fixtures_path}/site/boundary.jsonld", encoding='utf-8') as f:
            site = json.load(f)

        with open(f"{fixtures_folder}/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(site)[0]
        self.assertEqual(key, 'measurements')
        self.assertEqual(value, expected)

    @patch(f"{class_path}.find_node_exact", return_value={"@id": "source"})
    @patch(f"{class_path}.download_hestia", side_effect=fake_download)
    def test_gap_fill_gadm(self, _m1, _m2):
        with open(f"{fixtures_path}/site/gadm.jsonld", encoding='utf-8') as f:
            site = json.load(f)

        with open(f"{fixtures_folder}/gadm/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(site)[0]
        self.assertEqual(key, 'measurements')
        self.assertEqual(value, expected)
