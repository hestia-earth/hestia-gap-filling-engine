import unittest
from unittest.mock import patch
import json
from tests.utils import OLD_VERSION, fixtures_path, fake_new_measurement

from hestia_earth.gap_filling.version import VERSION
from hestia_earth.gap_filling.site_models.rainfall_annual import _need_gap_fill, TERM_ID, gap_fill

class_path = 'hestia_earth.gap_filling.site_models.rainfall_annual'
fixtures_folder = f"{fixtures_path}/site/{TERM_ID}"


def fake_cycles(*args): return [{'@id': 'id', 'endDate': '2009'}]


class TestRainfallAnnual(unittest.TestCase):
    def test_need_gap_fill(self):
        site = {}
        end_year = 1978

        # end date too far => NO gap-fill
        self.assertEqual(_need_gap_fill(site, end_year), False)

        # end date not too far => NO gap-fill
        end_year = 1990
        self.assertEqual(_need_gap_fill(site, end_year), False)

        # with location but not measurement => gap-fill
        site = {'longitude': 0, 'latitude': 0, 'measurements': []}
        self.assertEqual(_need_gap_fill(site, end_year), True)

        # with endDate with measurement with model and gap-filled with different version => gap-fill
        measurement = {
            'term': {
                '@id': TERM_ID
            },
            'gapFilled': ['value'],
            'gapFilledVersion': [OLD_VERSION]
        }
        site['measurements'].append(measurement)
        self.assertEqual(_need_gap_fill(site, end_year), True)

        # with endDate with measurement with model and gap-filled with same version => NO gap-fill
        measurement['gapFilledVersion'] = [VERSION]
        self.assertEqual(_need_gap_fill(site, end_year), False)

    @patch(f"{class_path}._new_measurement", side_effect=fake_new_measurement)
    @patch(f"{class_path}._related_cycles", side_effect=fake_cycles)
    def test_gap_fill_coordinates(self, _m1, _m2):
        with open(f"{fixtures_path}/site/coordinates.jsonld", encoding='utf-8') as f:
            site = json.load(f)

        with open(f"{fixtures_folder}/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(site)[0]
        self.assertEqual(key, 'measurements')
        self.assertEqual(value, expected)

    @patch(f"{class_path}._new_measurement", side_effect=fake_new_measurement)
    @patch(f"{class_path}._related_cycles", side_effect=fake_cycles)
    def test_gap_fill_boundary(self, _m1, _m2):
        with open(f"{fixtures_path}/site/boundary.jsonld", encoding='utf-8') as f:
            site = json.load(f)

        with open(f"{fixtures_folder}/boundary/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(site)[0]
        self.assertEqual(key, 'measurements')
        self.assertEqual(value, expected)

    @patch(f"{class_path}._new_measurement", side_effect=fake_new_measurement)
    @patch(f"{class_path}._related_cycles", side_effect=fake_cycles)
    def test_gap_fill_gadm(self, _m1, _m2):
        with open(f"{fixtures_path}/site/gadm.jsonld", encoding='utf-8') as f:
            site = json.load(f)

        with open(f"{fixtures_folder}/gadm/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(site)[0]
        self.assertEqual(key, 'measurements')
        self.assertEqual(value, expected)
