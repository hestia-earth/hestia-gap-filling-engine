import unittest
from unittest.mock import patch
import json
from tests.utils import fixtures_path, fake_download_term

from hestia_earth.gap_filling.site_models.region import _need_gap_fill, GAP_FILLING_KEY, gap_fill

class_path = 'hestia_earth.gap_filling.site_models.region'
fixtures_folder = f"{fixtures_path}/site/{GAP_FILLING_KEY}"


class TestRegion(unittest.TestCase):
    def test_need_gap_fill(self):
        # site but not model => gap-fill
        site = {'longitude': 0, 'latitude': 0}
        site['site'] = site
        self.assertEqual(_need_gap_fill(site), True)

        # site with model => NO gap-fill
        site[GAP_FILLING_KEY] = {}
        self.assertEqual(_need_gap_fill(site), False)

    @patch(f"{class_path}.download_hestia", side_effect=fake_download_term)
    def test_gap_fill(self, _m):
        with open(f"{fixtures_path}/site/coordinates.jsonld", encoding='utf-8') as f:
            site = json.load(f)

        with open(f"{fixtures_folder}/term.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(site)[0]
        self.assertEqual(key, GAP_FILLING_KEY)
        self.assertEqual(value, expected)
