import unittest
from unittest.mock import patch
import json
from tests.utils import fixtures_path

from hestia_earth.gap_filling.data_completeness_models.crop_residue import gap_fill

class_path = 'hestia_earth.gap_filling.data_completeness_models.crop_residue'
fixtures_folder = f"{fixtures_path}/cycle/dataCompleteness"


class TestCropResidue(unittest.TestCase):
    @patch(f"{class_path}.find_node")
    def test_gap_fill(self, mock_find_node):
        # data on total crop residue, product and burnt practice amount => gap-fill
        with open(f"{fixtures_folder}/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)

        mock_find_node.return_value = [{'@id': 'aboveGroundCropResidueRemoved'}]
        self.assertEqual(gap_fill(cycle), True)

        mock_find_node.return_value = [{'@id': 'unknown-term'}]
        self.assertEqual(gap_fill(cycle), False)
