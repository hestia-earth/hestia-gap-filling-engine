import unittest
from unittest.mock import patch
import json
from tests.utils import fixtures_path

from hestia_earth.gap_filling.impact_assessment_pre_models.cycle import gap_fill, _need_gap_fill

class_path = 'hestia_earth.gap_filling.impact_assessment_pre_models.cycle'

with open(f"{fixtures_path}/cycle/complete.jsonld", encoding='utf-8') as f:
    cycle = json.load(f)


class TestPreCycle(unittest.TestCase):
    def test_need_gap_fill(self):
        cycle = {}
        impact = {'cycle': cycle}

        # cycle has no @id => no gap-fill
        self.assertEqual(_need_gap_fill(impact), False)
        cycle['@id'] = 'id'

        # cycle has an @id => gap-fill
        self.assertEqual(_need_gap_fill(impact), True)

    @patch(f"{class_path}.download_hestia", return_value=cycle)
    def test_gap_fill(self, _m):
        impact = {'cycle': {'@id': cycle['@id']}}

        key, value = gap_fill(impact)[0]
        self.assertEqual(key, 'cycle')
        self.assertEqual(value, cycle)
