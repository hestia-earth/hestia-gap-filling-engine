import unittest
from unittest.mock import patch
import json
from tests.utils import fixtures_path, fake_practice

from hestia_earth.gap_filling.cycle_pre_models.ag_crop_residue_practices import GAP_FILLING_KEY, gap_fill, \
    _need_gap_fill_model

class_path = 'hestia_earth.gap_filling.cycle_pre_models.ag_crop_residue_practices'
fixtures_folder = f"{fixtures_path}/cycle/aboveGroundCropResidue/practices"


class FakeModel():
    def __init__(self, PRACTICE_TERM_ID):
        self.PRACTICE_TERM_ID = PRACTICE_TERM_ID

    def gap_fill_practice_value(*args): return None


class TestAboveGroundCropResiduePractices(unittest.TestCase):
    def test_need_gap_fill_model(self):
        model = FakeModel('residueBurnt')

        # product with model and gap-filled with different version => gap-fill
        with open(f"{fixtures_folder}/old-version.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)
        self.assertEqual(_need_gap_fill_model(model, cycle), True)

        # product with model and gap-filled with same version => no gap-fill
        with open(f"{fixtures_folder}/same-version.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)
        self.assertEqual(_need_gap_fill_model(model, cycle), False)

    @patch(f"{class_path}._get_or_create_practice", side_effect=fake_practice)
    def test_gap_fill(self, _m):
        with open(f"{fixtures_folder}/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)

        with open(f"{fixtures_folder}/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(cycle)[0]
        self.assertEqual(key, GAP_FILLING_KEY)
        self.assertEqual(value, expected)
