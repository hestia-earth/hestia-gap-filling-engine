import unittest
from unittest.mock import patch
import json
from tests.utils import fixtures_path

from hestia_earth.gap_filling.cycle_pre_models.primary_product import gap_fill, _need_gap_fill_primary, \
    _gap_fill_primary, _gap_fill_economicValueShare

class_path = 'hestia_earth.gap_filling.cycle_pre_models.primary_product'
fixtures_folder = f"{fixtures_path}/cycle/primary"


class TestPrimaryProduct(unittest.TestCase):
    def test_need_gap_fill_primary(self):
        # no products => do not gap-fill
        products = []
        self.assertEqual(_need_gap_fill_primary(products), False)

        # with product but no primary info => gap-fill
        product = {
            '@type': 'Product'
        }
        products.append(product)
        self.assertEqual(_need_gap_fill_primary(products), True)

        # with product with primary => no gap-fill
        product['primary'] = True
        self.assertEqual(_need_gap_fill_primary(products), False)

    def test_gap_fill_primary(self):
        # only 1 product => primary
        products = [{
            '@type': 'Product'
        }]
        _gap_fill_primary(products)
        self.assertEqual(products[0]['primary'], True)

        # multiple products => primary with biggest economicValueShare
        products = [{
            '@type': 'Product',
            'economicValueShare': 100
        }, {
            '@type': 'Product',
            'economicValueShare': 0
        }, {
            '@type': 'Product',
            'economicValueShare': 456464564
        }, {
            '@type': 'Product'
        }]
        _gap_fill_primary(products)
        self.assertEqual(products[2]['primary'], True)

    @patch(f"{class_path}.get_table_value", return_value='10')
    def test_gap_fill_economicValueShare(self, _m):
        # if total value >= 100, do nothing
        products = [{
            '@type': 'Product',
            'economicValueShare': 20
        }, {
            '@type': 'Product',
            'economicValueShare': 80
        }, {
            '@type': 'Product'
        }]
        _gap_fill_economicValueShare(products)
        self.assertEqual(products[2].get('economicValueShare'), None)

        # total < 100 => gap-fill
        products[1]['economicValueShare'] = 70
        _gap_fill_economicValueShare(products)
        self.assertEqual(products[2].get('economicValueShare'), 10)

    def test_gap_fill(self):
        with open(f"{fixtures_folder}/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)

        with open(f"{fixtures_folder}/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(cycle)[0]
        self.assertEqual(key, 'products')
        self.assertEqual(value, expected)
