import unittest
from unittest.mock import patch
import json
from tests.utils import fixtures_path, fake_product

from hestia_earth.gap_filling.cycle_pre_models.ag_crop_residue_total import TERM_ID, _need_gap_fill, gap_fill

class_path = 'hestia_earth.gap_filling.cycle_pre_models.ag_crop_residue_total'
fixtures_folder = f"{fixtures_path}/cycle/{TERM_ID}"


class TestAboveGroundCropResidueTotal(unittest.TestCase):
    @patch(f"{class_path}._should_gap_fill_term_type", return_value=True)
    def test_need_gap_fill(self, _m):
        cycle = {'products': []}

        # no products => no gap-fill
        need_gap_fill, *args = _need_gap_fill(cycle)
        self.assertEqual(need_gap_fill, False)

        # product with model and gap-filled with different version => gap-fill
        with open(f"{fixtures_folder}/old-version.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)
        need_gap_fill, *args = _need_gap_fill(cycle)
        self.assertEqual(need_gap_fill, True)

        # product with model and gap-filled with same version => no gap-fill
        with open(f"{fixtures_folder}/same-version.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)
        need_gap_fill, *args = _need_gap_fill(cycle)
        self.assertEqual(need_gap_fill, False)

        # pass should gap fill but no changes as no dryMatter or default ag residue data
        with open(f"{fixtures_folder}/no-prop.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)
        need_gap_fill, *args = _need_gap_fill(cycle)
        self.assertEqual(need_gap_fill, True)

        # with a removed product => gap-fill
        with open(f"{fixtures_folder}/with-removed/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)
        need_gap_fill, *args = _need_gap_fill(cycle)
        self.assertEqual(need_gap_fill, True)

    @patch(f"{class_path}._get_or_create_product", side_effect=fake_product)
    def test_gap_fill(self, _m):
        with open(f"{fixtures_folder}/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)

        with open(f"{fixtures_folder}/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(cycle)[0]
        self.assertEqual(key, 'products')
        self.assertEqual(value, expected)

    @patch(f"{class_path}._get_or_create_product", side_effect=fake_product)
    def test_gap_fill_removed(self, _m):
        with open(f"{fixtures_folder}/with-removed/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)

        with open(f"{fixtures_folder}/with-removed/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(cycle)[0]
        self.assertEqual(key, 'products')
        self.assertEqual(value, expected)

    @patch(f"{class_path}._get_or_create_product", side_effect=fake_product)
    def test_gap_fill_koga(self, _m):
        with open(f"{fixtures_folder}/koga/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)

        with open(f"{fixtures_folder}/koga/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(cycle)[0]
        self.assertEqual(key, 'products')
        self.assertEqual(value, expected)

    @patch(f"{class_path}._get_or_create_product", side_effect=fake_product)
    def test_gap_fill_tree_nut(self, _m):
        with open(f"{fixtures_folder}/default-no-dm-property/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)

        with open(f"{fixtures_folder}/default-no-dm-property/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(cycle)[0]
        self.assertEqual(key, 'products')
        self.assertEqual(value, expected)
