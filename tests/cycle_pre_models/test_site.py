import unittest
from unittest.mock import patch
import json
from tests.utils import fixtures_path

from hestia_earth.gap_filling.cycle_pre_models.site import gap_fill, _need_gap_fill

class_path = 'hestia_earth.gap_filling.cycle_pre_models.site'

with open(f"{fixtures_path}/site/complete.jsonld", encoding='utf-8') as f:
    site = json.load(f)


class TestPreSite(unittest.TestCase):
    def test_need_gap_fill(self):
        site = {}
        cycle = {'site': site}

        # site has no @id => no gap-fill
        self.assertEqual(_need_gap_fill(cycle), False)
        site['@id'] = 'id'

        # site has @id => gap-fill
        self.assertEqual(_need_gap_fill(cycle), True)

    @patch(f"{class_path}.download_hestia", return_value=site)
    def test_gap_fill(self, _m):
        cycle = {'site': {'@id': site['@id']}}

        key, value = gap_fill(cycle)[0]
        self.assertEqual(key, 'site')
        self.assertEqual(value, site)
