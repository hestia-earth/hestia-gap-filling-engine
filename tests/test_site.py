import unittest
from unittest.mock import patch
import json
import os
from .utils import fixtures_path, order_lists

from hestia_earth.gap_filling.site import gap_fill

site_path = os.path.join(fixtures_path, 'site')


class TestSite(unittest.TestCase):
    @patch('hestia_earth.gap_filling.site_models.rainfall_annual._related_cycles', return_value=[])
    @patch('hestia_earth.gap_filling.site_models.temperature_annual._related_cycles', return_value=[])
    #  this site is already complete and is used to test it is not changed by gap-filling engine
    def test_gap_fill_data_complete(self, _m1, _m2):
        with open(os.path.join(site_path, 'complete.jsonld'), encoding='utf-8') as f:
            site = json.load(f)

        self.assertEqual(gap_fill(site), site)

    # run a series of cycles that we have verified the values for
    @patch('hestia_earth.gap_filling.site_utils.find_node_exact', return_value={"@id": "source"})
    @patch('hestia_earth.gap_filling.site_models.rainfall_annual._related_cycles', return_value=[])
    @patch('hestia_earth.gap_filling.site_models.temperature_annual._related_cycles', return_value=[])
    def test_gap_fill_reconciliation(self, mock_r1, mock_r2, _m1):
        src_dir = os.path.join(site_path, 'reconciliation')

        for id in os.listdir(src_dir):
            with open(os.path.join(src_dir, id, 'site.jsonld'), encoding='utf-8') as f:
                site = json.load(f)
            with open(os.path.join(src_dir, id, 'gap-filled.jsonld'), encoding='utf-8') as f:
                expected = json.load(f)
            with open(os.path.join(src_dir, id, 'cycles.jsonld'), encoding='utf-8') as f:
                cycles = json.load(f).get('cycles')

            mock_r1.return_value = cycles
            mock_r2.return_value = cycles

            result = gap_fill(site)
            # sort all lists to avoid errors on order
            order_lists(site, ['measurements'])
            order_lists(result, ['measurements'])
            self.assertEqual(result, expected)
