{
  "@id": "wwr0drl7r8mi",
  "@type": "Cycle",
  "description": "No tillage, Stubble retained, Nitrogen fertilization (cNTSRNF)",
  "functionalUnitMeasure": "1 ha",
  "defaultSource": {
    "@id": "v5ljiyw5wmfq",
    "@type": "Source"
  },
  "site": {
    "@id": "_elwpsuxka1_",
    "@type": "Site"
  },
  "dataCompleteness": {
    "fertilizer": true,
    "soilAmendments": true,
    "pesticidesAntibiotics": true,
    "water": true,
    "electricityFuel": true,
    "products": true,
    "coProducts": true,
    "cropResidue": false,
    "manureManagement": true,
    "other": true,
    "material": false,
    "@type": "Completeness"
  },
  "startDateDefinition": "harvest of previous crop",
  "startDate": "1968-12",
  "endDate": "2008-12",
  "inputs": [
    {
      "term": {
        "@type": "Term",
        "name": "Seed",
        "termType": "other",
        "@id": "seed",
        "units": "kg"
      },
      "value": [
        40
      ],
      "@type": "Input"
    },
    {
      "term": {
        "@type": "Term",
        "name": "Diesel",
        "termType": "fuel",
        "@id": "diesel",
        "units": "kg"
      },
      "value": [
        18.148
      ],
      "@type": "Input"
    },
    {
      "term": {
        "@type": "Term",
        "name": "Urea (as N)",
        "termType": "inorganicFertilizer",
        "@id": "ureaAsN",
        "units": "kg N"
      },
      "value": [
        70
      ],
      "@type": "Input"
    },
    {
      "term": {
        "@type": "Term",
        "name": "Herbicide unspecified (AI)",
        "termType": "pesticideAI",
        "@id": "herbicideUnspecifiedAi",
        "units": "kg active ingredient"
      },
      "value": [
        1.8
      ],
      "@type": "Input"
    }
  ],
  "emissions": [
    {
      "term": {
        "@type": "Term",
        "name": "CO2, to air, soil carbon stock change",
        "termType": "emission",
        "@id": "co2ToAirSoilCarbonStockChange",
        "units": "kg CO2"
      },
      "value": [
        254
      ],
      "methodDescription": "Annualised difference between the SOC stocks in 1968 and 2008. Soil organic carbon contents in the fine-ground samples were determined with a LECO CNS-2000 analyser. Corrected for soil bulk density.",
      "methodTier": "measured",
      "@type": "Emission"
    },
    {
      "term": {
        "@type": "Term",
        "name": "CH4, to air, soil",
        "termType": "emission",
        "@id": "ch4ToAirSoil",
        "units": "kg CH4"
      },
      "value": [
        0.16
      ],
      "methodDescription": "Manual sampling chambers. Samples taken about 2–3 times per week during the high emission periods. Sampling between July 2006 and June 2009.",
      "methodTier": "measured",
      "method": {
        "@type": "Term",
        "name": "Closed chamber, static",
        "termType": "methodEmissionResourceUse",
        "@id": "closedChamberStatic"
      },
      "@type": "Emission"
    },
    {
      "term": {
        "@type": "Term",
        "name": "N2O, to air, inorganic fertilizer, direct",
        "termType": "emission",
        "@id": "n2OToAirInorganicFertilizerDirect",
        "units": "kg N2O"
      },
      "value": [
        0.57383
      ],
      "methodDescription": "Manual sampling chambers. Samples taken about 2–3 times per week during the high emission periods. Sampling between July 2006 and June 2009",
      "methodTier": "measured",
      "method": {
        "@type": "Term",
        "name": "Closed chamber, static",
        "termType": "methodEmissionResourceUse",
        "@id": "closedChamberStatic"
      },
      "source": {
        "@id": "xulgu221pi7e",
        "@type": "Source"
      },
      "@type": "Emission"
    },
    {
      "term": {
        "@type": "Term",
        "name": "N2O, to air, inorganic fertilizer, indirect",
        "termType": "emission",
        "@id": "n2OToAirInorganicFertilizerIndirect",
        "units": "kg N2O"
      },
      "value": [
        0.11074
      ],
      "methodTier": "tier 1",
      "@type": "Emission"
    },
    {
      "term": {
        "@type": "Term",
        "name": "CO2, to air, urea hydrolysis",
        "termType": "emission",
        "@id": "co2ToAirUreaHydrolysis",
        "units": "kg CO2"
      },
      "value": [
        110
      ],
      "methodTier": "tier 1",
      "@type": "Emission"
    },
    {
      "term": {
        "@type": "Term",
        "name": "N2O, to air, crop residue decomposition, direct",
        "termType": "emission",
        "@id": "n2OToAirCropResidueDecompositionDirect",
        "units": "kg N2O"
      },
      "value": [
        0.02685
      ],
      "methodDescription": "Manual and automatic sampling chambers. Samples taken about 2–3 times per week during the high emission periods. Sampling between July 2006 and June 2009.",
      "methodTier": "measured",
      "@type": "Emission"
    },
    {
      "term": {
        "@type": "Term",
        "name": "CO2, to air, fuel combustion",
        "termType": "emission",
        "@id": "co2ToAirFuelCombustion",
        "units": "kg CO2"
      },
      "value": [
        49
      ],
      "methodTier": "tier 1",
      "@type": "Emission"
    },
    {
      "term": {
        "@type": "Term",
        "name": "CO2eq (GWP100, IPCC 2007)",
        "termType": "characterisedIndicator",
        "@id": "co2EqGwp100Ipcc2007",
        "units": "kg CO2eq"
      },
      "value": [
        16
      ],
      "methodTier": "background",
      "inputs": [
        {
          "@type": "Term",
          "name": "Machinery infrastructure, depreciated amount per Cycle",
          "termType": "material",
          "@id": "machineryInfrastructureDepreciatedAmountPerCycle",
          "units": "kg/functional unit/Cycle"
        }
      ],
      "@type": "Emission"
    },
    {
      "term": {
        "@type": "Term",
        "name": "CO2eq (GWP100, IPCC 2007)",
        "termType": "characterisedIndicator",
        "@id": "co2EqGwp100Ipcc2007",
        "units": "kg CO2eq"
      },
      "value": [
        7
      ],
      "methodTier": "background",
      "inputs": [
        {
          "@type": "Term",
          "name": "Diesel",
          "termType": "fuel",
          "@id": "diesel",
          "units": "kg"
        }
      ],
      "@type": "Emission"
    },
    {
      "term": {
        "@type": "Term",
        "name": "CO2eq (GWP100, IPCC 2007)",
        "termType": "characterisedIndicator",
        "@id": "co2EqGwp100Ipcc2007",
        "units": "kg CO2eq"
      },
      "value": [
        12
      ],
      "methodTier": "background",
      "inputs": [
        {
          "@type": "Term",
          "name": "Seed",
          "termType": "other",
          "@id": "seed",
          "units": "kg"
        }
      ],
      "@type": "Emission"
    },
    {
      "term": {
        "@type": "Term",
        "name": "CO2eq (GWP100, IPCC 2007)",
        "termType": "characterisedIndicator",
        "@id": "co2EqGwp100Ipcc2007",
        "units": "kg CO2eq"
      },
      "value": [
        134
      ],
      "methodTier": "background",
      "inputs": [
        {
          "@type": "Term",
          "name": "Urea (as N)",
          "termType": "inorganicFertilizer",
          "@id": "ureaAsN",
          "units": "kg N"
        }
      ],
      "@type": "Emission"
    },
    {
      "term": {
        "@type": "Term",
        "name": "CO2eq (GWP100, IPCC 2007)",
        "termType": "characterisedIndicator",
        "@id": "co2EqGwp100Ipcc2007",
        "units": "kg CO2eq"
      },
      "value": [
        31
      ],
      "methodTier": "background",
      "inputs": [
        {
          "@type": "Term",
          "name": "Herbicide unspecified (AI)",
          "termType": "pesticideAI",
          "@id": "herbicideUnspecifiedAi",
          "units": "kg active ingredient"
        }
      ],
      "@type": "Emission"
    }
  ],
  "products": [
    {
      "term": {
        "@type": "Term",
        "name": "Cereals, grain",
        "termType": "crop",
        "@id": "cerealsGrain",
        "units": "kg"
      },
      "description": "Wheat, grain; Barley, grain",
      "value": [
        2900
      ],
      "economicValueShare": 100,
      "properties": [
        {
          "term": {
            "@type": "Term",
            "name": "Dry matter",
            "termType": "property",
            "@id": "dryMatter",
            "units": "% (0-100)"
          },
          "value": 87.5,
          "@type": "Property"
        }
      ],
      "primary": true,
      "@type": "Product"
    },
    {
      "term": {
        "@type": "Term",
        "name": "Above ground crop residue, total",
        "termType": "cropResidue",
        "@id": "aboveGroundCropResidueTotal",
        "units": "kg dry matter"
      },
      "value": [
        3915
      ],
      "properties": [
        {
          "term": {
            "@type": "Term",
            "name": "Burning efficiency",
            "termType": "property",
            "@id": "burningEfficiency",
            "units": "% (0-100)"
          },
          "value": 90,
          "@type": "Property"
        },
        {
          "term": {
            "@type": "Term",
            "name": "Nitrogen content",
            "termType": "property",
            "@id": "nitrogenContent",
            "units": "%"
          },
          "value": 0.7,
          "@type": "Property"
        },
        {
          "term": {
            "@type": "Term",
            "name": "Carbon content",
            "termType": "property",
            "@id": "carbonContent",
            "units": "%"
          },
          "value": 40,
          "@type": "Property"
        }
      ],
      "@type": "Product"
    }
  ],
  "practices": [
    {
      "term": {
        "@type": "Term",
        "name": "Residue left on field",
        "termType": "cropResidueManagement",
        "@id": "residueLeftOnField",
        "units": "percent"
      },
      "value": 100,
      "@type": "Practice"
    }
  ],
  "cycleDuration": 14610,
  "name": "Cereals, grain - Queensland, Australia - 2008-12 - No tillage, Stubble retained, ...",
  "dataDescription": "Measured and modelled emissions. Partial data on inputs and products. Site measurements available.",
  "schemaVersion": "2.14.0",
  "originalId": "8",
  "dataPrivate": false
}
