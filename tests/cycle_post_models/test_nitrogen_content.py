import unittest
from unittest.mock import patch
import json
from tests.utils import OLD_VERSION, fixtures_path, fake_property

from hestia_earth.gap_filling.version import VERSION
from hestia_earth.gap_filling.cycle_post_models.nitrogen_content import TERM_ID, \
    _need_gap_fill, _need_gap_fill_product, gap_fill

class_path = 'hestia_earth.gap_filling.cycle_post_models.nitrogen_content'
fixtures_folder = f"{fixtures_path}/cycle/{TERM_ID}"


class TestNitrogenContent(unittest.TestCase):
    def test_need_gap_fill(self):
        cycle = {'products': []}

        # no products => no gap-fill
        need_gap_fill, *args = _need_gap_fill(cycle)
        self.assertEqual(need_gap_fill, False)

        # with primary product => gap-fill
        cycle['products'] = [{'primary': True}]
        need_gap_fill, *args = _need_gap_fill(cycle)
        self.assertEqual(need_gap_fill, True)

    def test_need_gap_fill_product(self):
        product = {}

        # not a gap-filled product => no gap-fill
        product['term'] = {'@id': 'random id'}
        self.assertEqual(_need_gap_fill_product(product), False)

        # with a gap-filled product => gap-fill
        product['term']['@id'] = 'aboveGroundCropResidueTotal'
        self.assertEqual(_need_gap_fill_product(product), True)

        # product with model and gap-filled with different version => gap-fill
        prop = {
            'term': {
                '@id': TERM_ID
            },
            'gapFilled': ['value'],
            'gapFilledVersion': [OLD_VERSION]
        }
        product['properties'] = [prop]
        self.assertEqual(_need_gap_fill_product(product), True)

        # product with model and gap-filled with same version => NO gap-fill
        prop['gapFilledVersion'] = [VERSION]
        self.assertEqual(_need_gap_fill_product(product), False)

    @patch(f"{class_path}._get_or_create_property", side_effect=fake_property)
    def test_gap_fill(self, _m):
        with open(f"{fixtures_folder}/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)

        with open(f"{fixtures_folder}/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(cycle)[0]
        self.assertEqual(key, 'products.1.properties')
        self.assertEqual(value, expected)
