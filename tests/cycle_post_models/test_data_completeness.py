import unittest
import json
from tests.utils import fixtures_path

from hestia_earth.gap_filling.cycle_post_models.data_completeness import GAP_FILLING_KEY, gap_fill, _need_gap_fill_model

fixtures_folder = f"{fixtures_path}/cycle/dataCompleteness"


class FakeModel():
    def __init__(self, GAP_FILLING_KEY):
        self.GAP_FILLING_KEY = GAP_FILLING_KEY

    def gap_fill(*args): return True


class TestDataCompleteness(unittest.TestCase):
    def test_need_gap_fill_model(self):
        key = 'cropResidue'
        model = FakeModel(key)
        cycle = {'dataCompleteness': {}}

        # already complete => no gap-fill
        cycle['dataCompleteness'][key] = True
        self.assertEqual(_need_gap_fill_model(model, cycle), False)

        # not complete => gap-fill
        cycle['dataCompleteness'][key] = False
        self.assertEqual(_need_gap_fill_model(model, cycle), True)

    def test_gap_fill(self):
        with open(f"{fixtures_folder}/cycle.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)

        with open(f"{fixtures_folder}/gap-filled.jsonld", encoding='utf-8') as f:
            expected = json.load(f)

        key, value = gap_fill(cycle)[0]
        self.assertEqual(key, GAP_FILLING_KEY)
        self.assertEqual(value, expected)
