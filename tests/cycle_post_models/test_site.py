import unittest
import json
from tests.utils import fixtures_path

from hestia_earth.gap_filling.cycle_post_models.site import gap_fill, _need_gap_fill


class TestPostSite(unittest.TestCase):
    def test_need_gap_fill(self):
        site = {}
        cycle = {'site': site}

        # site has no @id => no gap-fill
        self.assertEqual(_need_gap_fill(cycle), False)
        site['@id'] = 'id'

        # site has an id => gap-fill
        self.assertEqual(_need_gap_fill(cycle), True)

    def test_gap_fill(self):
        # contains a full site
        with open(f"{fixtures_path}/cycle/complete.jsonld", encoding='utf-8') as f:
            cycle = json.load(f)

        site = cycle.get('site')
        key, value = gap_fill(cycle)[0]
        self.assertEqual(key, 'site')
        self.assertEqual(value, {'@type': site['@type'], '@id': site['@id']})
