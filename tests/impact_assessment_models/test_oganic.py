import unittest
import json
from tests.utils import fixtures_path

from hestia_earth.gap_filling.impact_assessment_models.organic import _need_gap_fill, GAP_FILLING_KEY, gap_fill

fixtures_folder = f"{fixtures_path}/impact-assessment/{GAP_FILLING_KEY}"


class TestOrganic(unittest.TestCase):
    def test_need_gap_fill(self):
        # no cycle => no gap-fill
        impact = {}
        self.assertEqual(_need_gap_fill(impact), False)

        # with cycle no practices => no gap-fill
        cycle = {'practices': []}
        impact['cycle'] = cycle
        self.assertEqual(_need_gap_fill(impact), False)

        # with practices
        practice = {}
        cycle['practices'].append(practice)
        self.assertEqual(_need_gap_fill(impact), True)

    def test_gap_fill(self):
        with open(f"{fixtures_folder}/impact-assessment.jsonld", encoding='utf-8') as f:
            impact = json.load(f)

        key, value = gap_fill(impact)[0]
        self.assertEqual(key, GAP_FILLING_KEY)
        self.assertEqual(value, True)
