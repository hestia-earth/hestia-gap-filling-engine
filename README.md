# Hestia Gap Filling Engine

[![Pipeline Status](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/badges/master/pipeline.svg)](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commits/master)
[![Coverage Report](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/badges/master/coverage.svg)](https://gitlab.com/hestia-earth/hestia-gap-filling-engine/commits/master)
[![Documentation Status](https://readthedocs.org/projects/hestia-gap-filling-engine/badge/?version=latest)](https://hestia-gap-filling-engine.readthedocs.io/en/latest/?badge=latest)

A set of modules for filling gaps in the activity data using external datasets (e.g. populating soil properties with a geospatial dataset using provided coordinates) and internal lookups (e.g. populating machinery use from fuel use). Includes rules for when gaps should be filled versus not (e.g. never gap fill yield, gap fill crop residue if yield provided etc.).

The Gap Filling Engine sits before the 'Calculation Engine' in the process.

The Gap Filling Engine uses datasets that are non-distributable, and therefore public access to this package is restricted, however all the sources are documented.

## Documentation

Official documentation can be found on [Read the Docs](https://hestia-gap-filling-engine.readthedocs.io/en/latest/index.html).

Additional models documentation can be found in the [source folder](./hestia_earth/gap_filling).

## Install

1. Install the module:
```bash
pip install hestia_earth.gap_filling
```

2. Download the latest data:
```bash
curl https://gitlab.com/hestia-earth/hestia-gap-filling-engine/-/raw/master/scripts/download_data.sh?inline=false -o download_data.sh && chmod +x download_data.sh
# pip default install directory is /usr/local/lib/python<version>/site-packages
./download_data.sh <API_URL> <pip install directory>
```

To use the production-ready version of the data, please use `API_URL=https://api.hestia.earth`.
For development version, please contact us at <community@hestia.earth>.

### Usage

```python
# will work with either Cycle or Site
from hestia_earth.gap_filling import gap_fill

# cycle is a JSONLD node cycle
gap_filled_cycle = gap_fill(cycle)
```
