Welcome to Hestia Gap Filling Engine's documentation!
========================================

This package contains the Gap Filling engine developed by Hestia.

Installation
------------

Install from `PyPI <https://pypi.python.org/pypi>`_ using `pip <http://www.pip-installer.org/en/latest/>`_, a
package manager for Python.

.. code-block:: bash

    pip install hestia_earth.gap_filling


Requirements
============

- `hestia_earth.schema >= 2.13.0 <https://pypi.org/project/hestia-earth.schema/>`_
- `hestia_earth.utils >= 0.4.3 <https://pypi.org/project/hestia-earth.schema/>`_
- `python-dateutil >= 2.8.1 <https://pypi.org/project/python-dateutil/>`_
- `requests >= 2.24.0 <https://pypi.org/project/requests/>`_

Contents
--------

.. autosummary::
   :toctree: _autosummary
   :caption: API Reference
   :template: custom-module-template.rst
   :recursive:

   hestia_earth.gap_filling


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
