#!/bin/bash

STAGE=${1:-"dev"}

cd ./layer/

rm -rf layer.zip
zip -r layer.zip python

aws lambda publish-layer-version \
    --region us-east-1 \
    --layer-name "hestia-$STAGE-python37-gap-filling-engine" \
    --description "Gap Filling Engine running on python 3.7" \
    --zip-file "fileb://layer.zip" \
    --compatible-runtimes python3.7
