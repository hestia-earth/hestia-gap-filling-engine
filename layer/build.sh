#!/bin/bash

export PKG_DIR="python"

PKG_PATH=python/lib/python3.7/site-packages/

cd ./layer/

rm -rf ${PKG_DIR} && mkdir -p ${PKG_DIR}

docker run --rm \
  -v $(pwd):/var/task lambci/lambda:build-python3.7 \
    pip install -r requirements.txt -t $PKG_PATH

# cp -R ../hestia_earth $PKG_PATH/.
rsync -ax --exclude __pycache__ ../hestia_earth $PKG_PATH/.
# numpy from layer
rm -rf $PKG_PATH/numpy*

cd ../
./scripts/download_data.sh $1 ./layer/$PKG_PATH
